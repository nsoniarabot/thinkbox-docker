# Instructions

### Please be aware that you ONLY need the "docker-compose.yaml" file from this repository. While you can clone and inspect all the files, that would just take a LOT more time than you need to spend to simply run the application.


# Setup and Config (One time exercise)

You will need to configure following on your machine
* AWS CLI (to access privately stored images)
* Docker & Docker compose

### Install and configure AWS CLI
Please execute the following commands to install AWS CLI (Linux / Mac).
```
### Please use specifically version CLI version2 
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
Now that you have AWS-cli installed, you will need an "AWS IAM" user so that you can authenticate yourself to our AWS Account. Please have an IAM user created for you with access to ECR (Elastic Container Registery).

Now, execute the following command to authenticate your machine to AWS.
```
aws configure
```
It will ask you for your Access Key, Secret Key which you should have received at the time of your IAM user account creation. It will also ask you for a "Region" which should be *"us-west-2"* without the quotes.


### Install Docker and Docker-compose
Please install Docker and docker-compose in your machine. The installation procedure depends on your operating system and a simple Google search will get you the commands that you need to execute.




# Deploying the application

* Please ensure you have docker and docker-compose installed on your machine
* Get the docker-compose.yaml from this repository to your machine
* Execute the following command to authenticate yourself to AWS ECR specifically.
```
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 801671600627.dkr.ecr.us-west-2.amazonaws.com
```
* CD into the directory where you've saved docker-compose.yaml and execute the following command

```
docker-compose up -d
```
- Please note that the application will take about 50 seconds to start. We've setup a "wait time" of 30 seconds for the database before the application is started.

* Now that the containers are created, please excecute these two commands that that will bring the application up
```
docker exec -it app-dockerization_webapp_1 /bin/bash
sytsemctl start thinkbox
```

This will log you in the application container and start the service. You can then simply "exit" out of it.


Note - This will expose the web application on port 80. This can be changed by modifying the "ports" section of docker-compose.yaml file. Additionally, it may take a few seconds for the application to show up if you're running it for the first titme.
