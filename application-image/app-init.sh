## OS packages
## Please do not remove any of these or it'll break the build
apt-get update
apt-get install python3-dev libhunspell-dev libpq-dev -y

## create virtualenv for application and python libs
cd /opt/thinkbox
python3.7 -m venv venv
cd /opt/thinkbox
source venv/bin/activate

cd /opt/thinkbox
pip3.7 install pyyaml
pip3.7 install flask-restplus
pip3.7 install --no-deps --no-index --find-links=/opt/thinkbox/pre_pythoninstall -r requirements_INSTALL_ME_FIRST.txt
cd /opt/thinkbox/pythoninstall
for x in `ls /opt/thinkbox/pythoninstall`; do pip3.7 install $x; done
#pip3.7 install --no-deps --no-index --find-links=/home/thinkbox/thinkbox/pythoninstall -r requirements_offline.txt
pip3.7 install --no-deps --no-index --find-links=/opt/thinkbox/post_pythoninstall -r post_requirements.txt

## missing deps from offline downloads
pip3.7 install Werkzeug==0.16.1
pip3.7 install flask-script
pip3.7 install flask_sqlalchemy
pip3.7 install flask_debugtoolbar
pip3.7 install pandas
pip3.7 install psycopg2
chown -R arabot:arabot /opt/thinkbox


## install pre_pythoninstall

# tar -xzf pre_pythoninstall.tar.gz
# cd pre_pythoninstall
# pip3.7 install --upgrade pip
# pip3.7 install Cython-0.29.13-cp37-cp37m-manylinux1_x86_64.whl
# pip3.7 install Werkzeug==0.16.1
# pip3.7 install hunspell

# ## install "pythoninstall"
# cd /home/thinkbox/thinkbox/
# tar -xzf pythoninstall.tar.gz
# cd /home/thinkbox/thinkbox/pythoninstall
# for x in `ls /home/thinkbox/thinkbox/pythoninstall`; do pip3.7 install $x; done

# ## post install
# #pip3 install uwsgi==2.0.19.1
pip3.7 install uwsgi
