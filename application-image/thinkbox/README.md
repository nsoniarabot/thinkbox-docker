# README #
This repository is the heart of the arabot NLP systems. It will handle intent detection and interface with all NLP functionalities. 

### How do I get set up? ###

* Clone the repo
* Create new folder in the resources folder and name it "core_nlp" and add the coreNLP jar there.
* Create a directory called logs in the thinkbox directory
* Create these directories under the top level of the resource directory: models, test_dictionaries, pkl_files, test_models and py4j_dictionaries
* Create a clean virtual environment
```bash
virtualenv --no-site-packages -p python3 venv
```
* Install all necessary requirements:
```bash 
sudo apt install python3-dev libhunspell-dev
sudo apt-get install libpq-dev
sudo apt install uwsgi-plugin-python3
pip3 install -r requirements_INSTALL_ME_FIRST.txt 
pip3 install -r requirements.txt
```
* Copy config.yml to local.yml and edit it to use the correct paths on your machine
* Download the database and the test database from google drive
* Start the uwsgi flask server using
```bash
uwsgi_python3 --ini uwsgi.ini
```
* To install nltk data files:
```python
import nltk
nltk.download('stopwords')
nltk.download('punkt')
```

* To test without uwsgi:
```bash 
python3 api.py
```

* To call the API, use this command:
```bash
curl -d '{"user_text":"whatever", "profile_id":1}' -H "Content-Type: application/json" -X POST http://localhost:5000/parse
```
* Run all the tests using:
```bash
python3 -m pytest test.py -v
```

### Contribution guidelines ###

* Create a branch for your work and then create a pull request to the "develop" branch once your code is ready. Ask someone to review and merge your code.
* Code should only be merged to the master branch when it is ready to be released to production

#### Py4j setup and connection ###
* The py4j package was installed when you installed requirements.txt

* The Py4J Java library is located under */usr/local/share/py4j/py4j0.x.jar.* 

* Add this library to your classpath when using Py4J in a Java program.

* Create java interface and java class as entry point ***(client side)*** like below:


```java

// py4j/examples/IThinkBox.java 
package py4j.thinkbox;

public interface IThinkBox {
    public String userIntent(String text, int profile_id);
}

// py4j/examples/IThinkBox.java 
package py4j.thinkbox;
import py4j.ClientServer;

public class ThinkBoxClientApp {

    public static void main(String[] args) {
        ClientServer clientServer = new ClientServer(null);
        // We get an entry point from the Python side
        IThinkBox tb = (IThinkBox) clientServer.getPythonServerEntryPoint(new Class[] { IThinkBox.class });
        // Java calls Python without ever having been called from Python
        System.out.println(tb.userIntent("wharever",3333));
        // close clientServer
        clientServer.shutdown();
    }
}
```
* Run ```py4j_server.py``` form thinkbox repository as ***server side***

* For more explanation, see [py4j doc](https://www.py4j.org)

### Classify and Add training data endpoints
To add data use:
`curl -F 'file=@/home/nadine/Documents/training_data.csv' "localhost:5000/insert_data?profile_id=2"`

To train use:
`curl "localhost:5000/train_model?profile_id=233"`


### Who do I talk to? ###

* Data science team
