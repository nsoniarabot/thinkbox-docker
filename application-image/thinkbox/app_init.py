import logging
from config import thinkbox_config
from components.sentiment_analysis import SentimentAnalysis
import components.spellcheck_component.spellcheck_manager as spell_manager
from classifier.classifier_manager import ClassifierManager, ClassifierManagerMap
from jobs import JobUtils
from thinkbox_apis.models.classifier_models import ClassifierModel


def log_init(filename):
    logging.basicConfig(
        filename=filename,
        format=thinkbox_config['logging']['log_format'],
        datefmt=thinkbox_config['logging']['log_date_format'],
        level=logging.DEBUG
    )

def classifiers_init():
    path = thinkbox_config['classifier']['model_path']
    cm = ClassifierModel()
    profile_ids = cm.get_models()
    if path is None:
        return
    for profile_id in profile_ids:
        classifier_manager = ClassifierManager(profile_id[0])
        classifier_manager.load_model(profile_id[0], path)
        ClassifierManagerMap.add_classifier_manager(profile_id[0], classifier_manager)

def spellchecker_init(dictionary_path):
    spell_manager.reload_dictionaries(dictionary_path, thinkbox_config['dictionaries']['affixation_file_path'])

def jobs_init():
    JobUtils.initialize_jobs()

def initialize_all(log_file_path, dictionary_path):
    log_init(log_file_path)
    spellchecker_init(dictionary_path)
    classifiers_init()
    jobs_init()

