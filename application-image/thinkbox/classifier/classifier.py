from abc import ABC, abstractmethod
from sklearn.model_selection import GridSearchCV
from sklearn import svm

import joblib
import os
import logging
import numpy as np

class Classifier(ABC):
    def __init__(self, profile_id):
        self.profile_id = profile_id
        self.model = None
        self.min_cutoff = None
        pass

    @abstractmethod
    def train(self, X_train, y_train):
        return

    @abstractmethod
    def evaluate(self, X_test, y_test):
        return

    @abstractmethod
    def classify(self, vectorized_user_text):
        return

    @abstractmethod
    def save_model(self, path, version):
        return

    @abstractmethod
    def load_model(self, path, version):
        return


class BaseClassifier(Classifier):
    def __init__(self, profile_id):
        super().__init__(profile_id)
        self.model = None
        self.base_model = None

    def train(self, X_train, y_train, X_test ,y_test, cv=5, scoring='accuracy'):
        return

    def set_mean_cutoff(self, X_test):
        predictions = self.model.predict(X_test)
        predictions_proba = self.model.predict_proba(X_test)
        probas_list = []
        for row_index, (input, prediction, label, proba) in enumerate(
                zip(X_test, predictions, y_test, predictions_proba)):
            if prediction == label:
                probas_list.append(max(proba))
        quantile_list = []
        for i in list(np.arange(start=0.01, stop=0.11, step=0.01)):
            quantile_list.append(np.quantile(probas_list, i))
        self.min_cutoff = np.mean(quantile_list)

    def cutoff(self):
        return self.min_cutoff

    def evaluate(self, X_test, y_test):
        if self.model is not None:
            return self.model.score(X_test, y_test)

    def classify(self, vectorized_user_text, confidence_cutoff):
        if confidence_cutoff is None:
            confidence_cutoff = 0.0
        if self.model is not None:
            probabilities = self.model.predict_proba(vectorized_user_text)
            highest_probabilty_index = np.argmax(probabilities[0])
            if (probabilities[0][highest_probabilty_index] > confidence_cutoff):
                return self.model.classes_[highest_probabilty_index]
            else:
                return None 

    def save_model(self, path, version):
        full_path = os.path.join(path, str(self.profile_id) + '_' + str(version))
        logging.info('saving... ' + full_path)

        if self.model is not None:
            joblib.dump(self.model, full_path)

    def load_model(self, path, version):
        full_path = os.path.join(path, str(self.profile_id) + '_' + str(version))
        logging.info('loading... ' + full_path)
        try:
            self.model = joblib.load(full_path)
        except Exception as e:
            logging.error(e)
            self.model = None
            return False

        return True


class SVM(BaseClassifier):
    def __init__(self, profile_id):
        super().__init__(profile_id)
        self.parameters = {'kernel': ['linear', 'rbf'],
                           'C': [0.001, 0.01, 0.1, 1, 10, 100],
                           'gamma': [0.001, 0.01, 0.1, 1, 10]
                           }

    def train(self, X_train, y_train, X_test ,y_test, cv=5, scoring='accuracy'):
        self.base_model = svm.SVC(probability=True)
        self.model = GridSearchCV(self.base_model, self.parameters, cv=cv, scoring=scoring)
        self.model.fit(X_train, y_train)
        self.set_mean_cutoff(X_test)


class ThinkboxLogisticRegression(BaseClassifier):
    def __init__(self, profile_id):
        super().__init__(profile_id)
        self.parameters = {
            "C" : [0.1, 1, 5],
            "solver":['lbfgs', 'liblinear'],
            'class_weight' : ['balanced'],
            'multi_class' : ['auto']
        }

    def train(self, X_train, y_train, X_test ,y_test, cv=5, scoring='accuracy'):
        self.model_base  = LogisticRegression(class_weight='balanced')
        self.model = GridSearchCV(self.base_model, self.parameters, cv=cv, scoring=scoring)
        self.model.fit(X_train, y_train)
        self.set_mean_cutoff(X_test)
