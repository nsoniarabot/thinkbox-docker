from sklearn.feature_extraction.text import *
from classifier.intent_classifier_preprocessing.intent_classifier_preprocessing import *
from thinkbox_apis.models.label import Label
from classifier.classifier import SVM, ThinkboxLogisticRegression
from sklearn.pipeline import Pipeline
import pandas as pd
import logging
import sys


from thinkbox_apis.models.training_sample import TrainingSample
from thinkbox_apis.models.classifier_models import ClassifierModel



class ClassifierManager:
    def __init__(self, profile_id, preprocessor=None, classifier=None):
        cm = ClassifierModel()
        self.profile_id = profile_id
        self.data = None
        self.preprocessor = preprocessor
        self.classifier = classifier
        self.confidence_cutoff = cm.get_confidence_cutoff()
        return

    def insert_model_to_db(self, hyperparameters, metric, metric_value, vectorizer, preprocessor, model_type, active_on_test, active_on_live, confidence_cutoff):
        classifier_config = ClassifierModel()
        classifier_config.hyperparameters = hyperparameters
        classifier_config.metric = metric
        classifier_config.metric_value = metric_value
        classifier_config.vectorizer = vectorizer
        classifier_config.preprocessor = preprocessor
        classifier_config.model_type = model_type
        classifier_config.profile_id = self.profile_id
        classifier_config.active_on_test = active_on_test
        classifier_config.active_on_live = active_on_live
        classifier_config.confidence_cutoff = confidence_cutoff
        return classifier_config.insert_classifier_model_config(classifier_config)

    def train_model(self, vectorizer, evaluation_threshold, active_on_test=True, active_on_live = True, cv=5, scoring='accuracy'):
        logging.info('training model ' + str(self.profile_id))
        self.preprocessor.process_data()
        self.preprocessor.get_vectorized_training_data(vectorizer)
        X_train, X_test, y_train, y_test = self.preprocessor.split_data()
        self.classifier.train(X_train, y_train, X_test, y_test, cv, scoring)
        cutoff =  round(self.classifier.cutoff(),2)
        evaluation_score = self.classifier.evaluate(X_test, y_test)
        if evaluation_score > evaluation_threshold:
            return self.insert_model_to_db(str(self.classifier.model.best_params_), 'accuracy', float(evaluation_score),
                                           vectorizer.__class__.__name__, self.preprocessor.__class__.__name__,
                                           self.classifier.__class__.__name__, active_on_test, active_on_live, cutoff)
        else:
            return None

    @staticmethod
    def get_data(profile_id):
        ts = TrainingSample()
        results = ts.get_training_samples(profile_id)
        if results is None or len(results) == 0:
            return
        data = pd.DataFrame(results)
        data.columns = ['label_id', 'text']
        return data

    def classify(self, user_text):
        return self.classifier.classify(self.preprocessor.get_vectorized_query(user_text), self.confidence_cutoff[self.profile_id])

    def classify_get_name(self, user_text):
        classification = self.classify(user_text)
        if classification is None:
            return 'others'
        return Label.get_label_name(self.profile_id, classification)

    def save_model(self, path, version):
        self.classifier.save_model(path, version)
        self.preprocessor.save_vector(path, version, self.profile_id)

    def init_preprocessor(self, classifier_config):
        self.preprocessor = getattr(sys.modules[__name__], classifier_config.preprocessor)(ClassifierManager.get_data(self.profile_id))
        self.classifier = getattr(sys.modules[__name__], classifier_config.model_type)(self.profile_id)

    def load_model(self, profile_id, path):
        cm = ClassifierModel()
        result = cm.get_classifier_model_config(profile_id)
        if result is not None:
            self.init_preprocessor(result)

            if self.classifier.load_model(path, result.id) \
                    and self.preprocessor.load_vector(path, result.id, self.profile_id):
                return True
            else:
                logging.error('model not found ' + str(self.profile_id) + ', version = ' + str(result.id))


class ClassifierManagerMap:
    classifier_managers = {}

    @staticmethod
    def add_classifier_manager(profile_id, classifier_manager):
        ClassifierManagerMap.classifier_managers[profile_id] = classifier_manager

    @staticmethod
    def get_classifier_manager(profile_id):
        if profile_id in ClassifierManagerMap.classifier_managers:
            return ClassifierManagerMap.classifier_managers[profile_id]
        else:
            return None
