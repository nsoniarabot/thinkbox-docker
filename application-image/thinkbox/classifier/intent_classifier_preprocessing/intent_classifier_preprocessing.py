import pandas as pd
from sklearn.model_selection import train_test_split
from abc import ABC
import joblib
import os
from components.text_preprocessing_pipelines import TextPreprocessingPipelines
import logging


class IntentClassifierPreprocessing(ABC):
    def __init__(self, data: pd):
        self.data = data
        self.vectorizer = None
        self.vectorized_data = None
        self.processed_data = []
        self.training_ids = []
        pass

    @staticmethod
    def normalize(line):
        pass

    def split_data(self, test_size=0.2, random_state=42):
        pass

    def get_vectorized_query(self, query):
        pass

    def process_data(self):
        pass

    def train_vectorizer(self):
        pass

    def get_vectorized_training_data(self, vectorizer):
        pass

    def save_vector(self, path, version, profile_id):
        pass

    def load_vector(self, path, version, profile_id):
        pass

    @staticmethod
    def get_vectorizer_name(path, profile_id, version):
        pass


class IntentClassifierBasicPreprocessing(IntentClassifierPreprocessing):

    def __init__(self, data: pd):
        super().__init__(data)

    @staticmethod
    def normalize(line):
        return TextPreprocessingPipelines.process(line)

    def split_data(self, test_size=0.2, random_state=0):
        return train_test_split(self.vectorized_data, self.training_ids, test_size=test_size, random_state=random_state, stratify=self.training_ids)

    def get_vectorized_query(self, query):
        return self.vectorizer.transform([query])

    def process_data(self):
        processed_data = []
        training_ids = []
        for text, label_id in zip(self.data['text'], self.data['label_id']):
            processed_line = self.normalize(text)
            if processed_line not in processed_data:
                processed_data.append(processed_line)
                training_ids.append(label_id)
        self.processed_data = processed_data
        self.training_ids = training_ids
        return self.processed_data

    def train_vectorizer(self):
        self.vectorizer.fit_transform(self.processed_data, self.training_ids)

    def get_vectorized_training_data(self, vectorizer):
        self.vectorizer = vectorizer
        self.vectorized_data = self.vectorizer.fit_transform(self.processed_data, self.training_ids)
        return self.vectorized_data

    def save_vector(self, path, version, profile_id):
        if self.vectorizer is not None:
            joblib.dump(self.vectorizer, IntentClassifierBasicPreprocessing.get_vectorizer_name(path, profile_id, version))

    def load_vector(self, path, version, profile_id):
        try:
            self.vectorizer = joblib.load(IntentClassifierBasicPreprocessing.get_vectorizer_name(path, profile_id, version))
        except Exception as e:
            logging.error(e)
            self.vectorizer = None
            return False
        return True

    @staticmethod
    def get_vectorizer_name(path, profile_id, version):
        return os.path.join(path, str(profile_id) + '_' + str(version) + '.vec')
