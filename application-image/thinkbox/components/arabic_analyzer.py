from components.base_component import BaseComponent
from enum import Enum
from config import thinkbox_config
from trace import Trace
import sys
import logging
import enum


class AnalyzerProfile(enum.Enum):

    '''
    The "AnalyzerProfile" is an "Enum" structure for mapping the
        "arabic_analyzer" profile ID
    returning method: None
    '''

    NONE = 1
    TEXT_ARABOT_AR_REGULAR = 2
    TEXT_ARABOT_AR_ADVANCED = 3
    TEXT_ARABOT_AR_REGULAR_SP = 4
    TEXT_ARABOT_AR_ADVANCED_SP = 5
    TEXT_ARABOT_MIXED_REGULAR_SP = 6
    TEXT_ARABOT_MIXED_ADVANCED_SP = 7
    TEXT_ARABOT_AR_ADVANCED_K = 8
    TEXT_ARABOT_AR_REGULAR_MK = 9
    TEXT_ARABOT_AR_REGULAR_MLT = 10
    TEXT_ARABOT_MIXED_EXACT = 11
    TEXT_ARABOT_MIXED_LS = 12
    TEXT_ARABOT_MIXED_REGULAR_SOW = 13
    TEXT_ARABOT_FR = 14
    TEXT_ARABOT_FR_SP = 15
    TEXT_ARABOT_EN_FR = 16
    TEXT_ARABOT_EN_FR_SP = 17
    ARABOT_KEYWORD_ANALYZER = 20




class Analyzer(BaseComponent):
    analyzers = None
    '''
    Analyzer component main functionality is analyzing and normalizing
    arabic text, inherited from arabot-commons project which exists as
    dependency into arabot-core jar file

    returning method: process
    '''

    def process(self, trace: Trace, params: dict):

        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)

        if ('analyzer_pid' not in params or params['analyzer_pid'] is None):
            params['analyzer_pid'] = 'TEXT_ARABOT_AR_ADVANCED'

        user_text = trace.current_text
        analyzedText = self.analyze_text(user_text, AnalyzerProfile[params['analyzer_pid']].value)
        return analyzedText

    def analyze_text(self, text, analyzerProfile):
        if self.analyzers is None:
            self.build_analyzers()
        
        if analyzerProfile not in self.analyzers:
            analyzerProfile = AnalyzerProfile['TEXT_ARABOT_AR_ADVANCED'].value
        
        result = self.analyzers[analyzerProfile].analyzeString(text).toString()
        return result
    
    def build_analyzers(self):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        
        self.analyzers = {}
        
        arabot_ar = pyjnius_init.ArabotArAdvanced("thinkbox-ArabotArAdvanced")
        arabot_ar.buildAnalyzer()
        self.analyzers[AnalyzerProfile['TEXT_ARABOT_AR_ADVANCED'].value] = arabot_ar
        
        arabot_fr = pyjnius_init.ArabotFr('thinkbox-ArabotFr')
        arabot_fr.buildAnalyzer()
        self.analyzers[AnalyzerProfile['TEXT_ARABOT_FR'].value] = arabot_fr
        
        arabot_ar_reg = pyjnius_init.ArabotArRegular('thinkbox-ArabotArRegular')
        arabot_ar_reg.buildAnalyzer()
        self.analyzers[AnalyzerProfile['TEXT_ARABOT_AR_REGULAR'].value] = arabot_ar_reg
        
        arabot_mixed_reg = pyjnius_init.ArabotMixedRegularSP('thinkbox-ArabotMixedRegularSP')
        arabot_mixed_reg.buildAnalyzer()
        self.analyzers[AnalyzerProfile['TEXT_ARABOT_MIXED_REGULAR_SP'].value] = arabot_mixed_reg
        
        arabot_mixed_advanced = pyjnius_init.ArabotMixedAdvancedSP('thinkbox-ArabotMixedAdvancedSP')
        arabot_mixed_advanced.buildAnalyzer()
        self.analyzers[AnalyzerProfile['TEXT_ARABOT_MIXED_ADVANCED_SP'].value] = arabot_mixed_advanced
