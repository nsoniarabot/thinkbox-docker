from abc import ABC, abstractmethod
from trace import Trace


class BaseComponent(ABC):
    """
    All NLP components should extend this class and implement the process method. 
    This allows them to be used easily in the intent parsing pipeline
    """
    def __init__(self):
        super().__init__()
    
    @abstractmethod
    def process(self, trace: Trace, params):
        pass