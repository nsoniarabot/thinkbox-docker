from components.base_component import BaseComponent
from trace import Trace
from thinkbox_apis.models.custom_entity import CustomEntity
import re
import operator

class CustomEntites(BaseComponent):
    '''
       The "CustomEntites" class returns a dictionary
       with corrected detected entities values or synonyms
       returning method: process
       '''

    def __init__(self):
        super().__init__()

    def process(self, trace: Trace, params):
        '''The only method that would be called from this class

            :param params: Dictionary param that contains the
                user text and the profile ID
            :param trace: not used parameter in this class
            :type params: dict
            :type trace: dict
            :returns: detected entities from user text
            :rtype: pyhton list of dictionaries
        '''

        profile_id = params['profile_id']
        user_text = trace.current_text

        entities_obj = CustomEntity()
        entities_structure = entities_obj.get_entities_structure(profile_id)
        enable_parsing_dict = entities_obj.get_parsing_entites_names(profile_id)
        parsed_entites = self.entities_parser(user_text, entities_structure, enable_parsing_dict)
        return parsed_entites

    def entity_search(self, text, values_synonyms, enable_parsing):
        detected_entities = []
        if enable_parsing == False:
            for i in values_synonyms:
                if i is None:
                    continue
                if re.findall('\\b' + i + '\\b', text):
                    detected_entities.append(i)
        else:
            for j in values_synonyms:
                if j is None:
                    continue
                if re.findall('\\b' + j + '\\b', text):
                    if text.endswith(j) == False and text.split(j)[1].split()[0].isnumeric():
                        detected_entities.append(text.split(j)[1].split()[0])
        return detected_entities

    def entities_parser(self, user_text, entities_structure, enable_parsing_dict):
        final_result = {}
        for i in entities_structure:
            entity_result = []
            entity = list(i.keys())[0]
            enable_parsing = enable_parsing_dict[entity]
            entity_values = list(i[entity].keys())
            for v in entity_values:
                syns_list = i[entity][v]
                syns_list.append(v)
                founded_entities = self.entity_search(user_text, syns_list, enable_parsing)
                if founded_entities:
                    for f in founded_entities:
                        index_list = [m.start() for m in re.finditer(f, user_text)]
                        if len(index_list) > 1:
                            for idx in index_list:
                                start_index = idx
                                end_index = idx + len(f)
                                parsed_result = {"matchedValue": f, "replacedValue": v, "locationFrom": start_index,
                                                 "locationTo": end_index}
                                entity_result.append(parsed_result)
                        else:
                            start_index = index_list[0]
                            end_index = index_list[0] + len(f)
                            parsed_result = {"matchedValue": f, "replacedValue": v, "locationFrom": start_index,
                                             "locationTo": end_index}
                            entity_result.append(parsed_result)
            if len(entity_result):
                final_result[entity] = entity_result
        return final_result