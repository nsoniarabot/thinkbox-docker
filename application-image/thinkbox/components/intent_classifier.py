from components.base_component import BaseComponent
from trace import Trace
from classifier.classifier_manager import ClassifierManagerMap


class IntentClassifier(BaseComponent):
    def __init__(self):
        super().__init__()

    def process(self, trace: Trace, params):
        classifier_manager = ClassifierManagerMap.get_classifier_manager(params['profile_id'])
        if (classifier_manager is not None):
            classification = classifier_manager.classify(trace.current_text)
            if classification is None:
                return None
            else:
                return str(classification)
        else:
            return None
