from components.base_component import BaseComponent
from enum import Enum
from config import thinkbox_config
from trace import Trace
import sys
import logging


class ParserNames(Enum):
    AGE_PARSER = 'age'
    COLOR_PARSER = 'color'
    CREDIT_CARD_PARSER = 'credit_card'
    CURRENCY_PARSER = 'currency'
    DATE_PARSER = 'date'
    EMAIL_PARSER = 'email'
    GEO_PARSER = 'geo'
    IN_RANGE_PARSER = 'InRangeParser'
    LANGUAGE_PARSER = 'language'
    LAT_LONG_PARSER = 'lat_long_checker'
    NAME_PARSER = 'name'
    CONFIRMATION_PARSER = 'confirmation'
    NUMBERS_PARSER = 'number'
    PERCENTAGE_PARSER = 'percentage'
    PHONE_PARSER = 'phone'
    TEMPERATURE_PARSER = 'temperature'
    TIME_DURATION_PARSER = 'time_duration'
    UNITS_PARSER = 'unit'
    URLS_PARSER = 'url'
    TIME_PERIOD = 'timePeriod'


class PParserMatch():
    def __init__(self, parser_match):
        self.userUtterance = str(parser_match.getUserUtterance())
        self.matchedValue = str(parser_match.getMatchedValue())
        self.replacedValue = str(parser_match.getReplacedValue())
        self.locationFrom = int(parser_match.getLocationFrom())
        self.locationTo = int(parser_match.getLocationTo())
        self.variables = parser_match.getVariables()

    @staticmethod
    def get_dict(parser_match):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        jSet = pyjnius_init.jSet
        variables = {}
        j_keys = jSet()
        j_keys.addAll(parser_match.getVariables().keySet())
        for variable in j_keys:
            variables[variable] = parser_match.getVariables().get(variable)
        return {
            'userUtterance': str(parser_match.getUserUtterance()),
            'matchedValue': str(parser_match.getMatchedValue()),
            'replacedValue': str(parser_match.getReplacedValue()),
            'locationFrom': int(parser_match.getLocationFrom()),
            'locationTo': int(parser_match.getLocationTo()),
            'variables': variables
        }

    @staticmethod
    def convert_to_python_objects(matches):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)

        ParserMatch = pyjnius_init.ParserMatch
        p_matches = []
        if matches is not None:
            for a in range(matches.size()):
                p_matches.append(PParserMatch.get_dict(matches.get(a)))
        return p_matches

    @staticmethod
    def get_parser_map():
        return {
            1: ParserNames.AGE_PARSER,
            2: ParserNames.COLOR_PARSER,
            3: ParserNames.CREDIT_CARD_PARSER,
            4: ParserNames.CURRENCY_PARSER,
            5: ParserNames.DATE_PARSER,
            6: ParserNames.EMAIL_PARSER,
            7: ParserNames.GEO_PARSER,
            8: ParserNames.IN_RANGE_PARSER,
            9: ParserNames.LANGUAGE_PARSER,
            10: ParserNames.LAT_LONG_PARSER,
            11: ParserNames.NAME_PARSER,
            13: ParserNames.NUMBERS_PARSER,
            14: ParserNames.PERCENTAGE_PARSER,
            15: ParserNames.PHONE_PARSER,
            16: ParserNames.TEMPERATURE_PARSER,
            17: ParserNames.TIME_DURATION_PARSER,
            18: ParserNames.UNITS_PARSER,
            19: ParserNames.URLS_PARSER,
            20: ParserNames.CONFIRMATION_PARSER,
            21: ParserNames.TIME_PERIOD
        }

    @staticmethod
    def get_parser_map_string():
        return {
            # "IN_RANGE_PARSER": ParserNames.IN_RANGE_PARSER.value,
            "DATE": ParserNames.DATE_PARSER.value,
            "GEO": ParserNames.GEO_PARSER.value,
            "PHONE": ParserNames.PHONE_PARSER.value,
            "EMAIL": ParserNames.EMAIL_PARSER.value,
            "AGE": ParserNames.AGE_PARSER.value,
            "COLOR": ParserNames.COLOR_PARSER.value,
            "CREDIT_CARD": ParserNames.CREDIT_CARD_PARSER.value,
            "CURRENCY": ParserNames.CURRENCY_PARSER.value,
            "LANGUAGE": ParserNames.LANGUAGE_PARSER.value,
            "LAT_LONG": ParserNames.LAT_LONG_PARSER.value,
            "NAME": ParserNames.NAME_PARSER.value,
            "NUMBERS": ParserNames.NUMBERS_PARSER.value,
            "PERCENTAGE": ParserNames.PERCENTAGE_PARSER.value,
            "PHONE": ParserNames.PHONE_PARSER.value,
            "TEMPERATURE": ParserNames.TEMPERATURE_PARSER.value,
            "TIME_DURATION": ParserNames.TIME_DURATION_PARSER.value,
            "UNITS": ParserNames.UNITS_PARSER.value,
            "URLS": ParserNames.URLS_PARSER.value,
            "CONFIRMATION": ParserNames.CONFIRMATION_PARSER.value,
            "TIME_PERIOD": ParserNames.TIME_PERIOD.value
        }


class Parsers(BaseComponent):
    def process(self, trace: Trace, params):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        ParserExtraction = pyjnius_init.ParserExtraction
        jString = pyjnius_init.jString
        user_text = jString(trace.original_text)
        matches = []
        if not params['parsers_list'] or params['parsers_list'] == 'all':
            parsers_list =  list(PParserMatch.get_parser_map_string().values())
            params['parser_names'] = parsers_list
        else:
            request_names = params['parsers_list'].split(",")
            parsers_list = []
            for name in request_names:
                parsers_list.append(PParserMatch.get_parser_map_string()[name.strip().upper()])
            params['parser_names'] = parsers_list
        named_entities = trace.named_entities
        for parser_name in params['parser_names']:
            matches = ParserExtraction.extractOne(user_text, jString(parser_name))
            parser_result = PParserMatch.convert_to_python_objects(matches)
            if parser_result is not None and len(parser_result) > 0:
                if named_entities is not None:
                    named_entities[parser_name] = parser_result
                else:
                    named_entities = {parser_name: parser_result}
        return named_entities
