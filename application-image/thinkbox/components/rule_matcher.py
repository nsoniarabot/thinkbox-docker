import re
import components.parsers as parsers
from thinkbox_apis.models.rule import Rule, RulesRuleType
from components.base_component import BaseComponent
from trace import Trace
from enum import Enum
from components.parsers import PParserMatch

class RuleBasedIntentParser(BaseComponent):
    def __init__(self):
        super().__init__()

    def process(self, trace: Trace, params):
        rules = Rule()
        rules = rules.get_rules(params['profile_id'])
        matcher = RuleMatcher()
        label = None

        if rules is None:
            return None

        for rule in rules:
            params['rule'] = rule
            label = matcher.process(trace, params)
            if label is not None:
                break
        return label


class RuleMatcher(BaseComponent):
    def __init__(self):
        super().__init__()

    def process(self, trace: Trace, params):

        if params['rule'].rule_type == RulesRuleType.exact.value:
            return RuleMatcher.exact_matcher(trace, params)
        elif params['rule'].rule_type == RulesRuleType.keyword.value:
            return RuleMatcher.keyword_matcher(trace, params)
        elif params['rule'].rule_type == RulesRuleType.regex.value:
            return RuleMatcher.regex_matcher(trace, params)
        elif params['rule'].rule_type == RulesRuleType.parser.value:
            return RuleMatcher.parser_matcher(trace, params)
        else:
            return None

    @staticmethod
    def exact_matcher(trace: Trace, params):
        label = None
        if trace.current_text == params['rule'].keyword:
            label = params['rule'].label_id
        return label

    @staticmethod
    def keyword_matcher(trace: Trace, params):
        label = None
        if all(word in trace.current_text.split() for word in params['rule'].keyword.split()):
            label = params['rule'].label_id
        return label

    @staticmethod
    def regex_matcher(trace: Trace, params):
        result = re.match(params['rule'].regex, trace.current_text)
        label = None
        if result is not None:
            label = params['rule'].label_id
        return label

    @staticmethod
    def parser_matcher(trace: Trace, params):
        parser = parsers.Parsers()
        label = None
        named_entities = parser.process(trace,
                                        {"parser_names": [params['rule'].parser_name]})

        if named_entities is not None and len(named_entities) != 0:
            label = params['rule'].label_id
        return label
