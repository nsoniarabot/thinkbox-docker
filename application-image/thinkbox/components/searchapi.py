from collections import OrderedDict
import logging
import requests
from components.base_component import BaseComponent
from thinkbox_apis.models.search_api_config import SearchApiConfig

class SearchAPIResponse:
    def __init__(self, collection_name, answer):
        self.collection_name = collection_name
        self.answer = answer


class SearchAPI(BaseComponent):
    # TODO get faq order from db
    default_collections = OrderedDict({
        'smalltalk': 'botsmalltalk',
        'badword': 'botbadword',
        'faq': 'botfaq'
    })
    solr_base_url = None

    def __init__(self):
        super().__init__()

    def process(self, trace, params):
        search_api_obj = SearchApiConfig()
        for collection in search_api_obj.get_search_api_config(params['profile_id']):
            # TODO get intent id, org id for all collections
            response = SearchAPI.get_answer(self.get_request(collection.search_api_url, collection.collection_name, \
                trace.current_text, collection.bot_id, collection.intent_id, collection.org_id))
            if response is not None:
                return  { "collection_name" : collection.collection_name, "answer" : response}

    @staticmethod
    def get_answer(json_response):
        if json_response is None:
            return None
        if json_response['response']['numFound'] > 0 and len(json_response['response']['docs']) > 0:
            return json_response['response']['docs'][0]['answer']
        else:
            return None

    @staticmethod
    def get_request(search_api_url, collection_name, question, bot_id, intent_id, org_id):
        params = {
            'fq': f'botId:{bot_id} AND intentId:{intent_id} AND orgId:{org_id}',
            'q': f'{question}'
        }
        request_url = f'{search_api_url}/{collection_name}/select'
        try:
            response = requests.get(request_url, params=params)
            if response is not None and response.status_code == 200:
                return response.json()
            else:
                logging.error("something went wrong, solr status code = " + str(response.status_code))
        except requests.exceptions.ConnectionError as e:
            logging.error(e)
        return None
