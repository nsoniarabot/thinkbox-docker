from components.base_component import BaseComponent
import hunspell
from trace import Trace
from config import thinkbox_config
import logging
from sklearn.externals import joblib


class SentimentAnalysis(BaseComponent):

    '''
    The "SentimentAnalysis" class returns pos or neg string
    returning method: process
    returning type: string
    '''

    # Initlizing the pkl file as "classifier" named clf
    try:
        pkl_files = thinkbox_config['pkl_files']
        sent_path = pkl_files['sentiment_path']
        clf = joblib.load(
            sent_path
        )
    except Exception as e:
        logging.error(e)


    def __init__(self):       

        super().__init__()

      
    def process(self, trace: Trace, params: dict):
     
        text = [trace.current_text]
        try:
            sent_proba = SentimentAnalysis.clf.predict_proba(text)[0]
            sent_result = {"pos": sent_proba[2], "neg": sent_proba[0], "neu": sent_proba[1]}
        except Exception as e:
            logging.error(e)
            return None
        return sent_result
    
    def result(self, trace: Trace, params: dict):

        text = [trace.current_text]
        sent_proba = SentimentAnalysis.clf.predict_proba(text)
        values_list = sent_proba[0].tolist()
        max_value = values_list.index(sent_proba.max())

        if max_value == 0:
            return "neg"
        elif max_value == 1:
            return "neu"
        else:
            return "pos"

   