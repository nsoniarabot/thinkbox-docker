from components.base_component import BaseComponent
import hunspell
from trace import Trace
from config import thinkbox_config
import logging
import components.spellcheck_component.spellcheck_manager as spell_manager
import utils
from components.spellcheck_component.spellcheck_utils import AccuracyMetric, SpellcheckNormalizer, SpecialCharacters


class Spellcheck(BaseComponent):
    '''
    The "Spellcheck" class returns a dictionary
    with corrected word/sentence
    returning method: process
    '''

    def __init__(self):
        super().__init__()  
        self.spell_obj_en = spell_manager.spellchecker_en
        self.spell_obj_ar = spell_manager.spellchecker_ar
        self.protected_words = spell_manager.protected_words


    def process(self, trace: Trace, params: dict):
        '''The only method that would be called from this class

        :param params: Dictionary param that contains the
            user text and the profile ID
        :param trace: not used parameter in this class
        :type params: dict
        :type trace: dict
        :returns: Corrected user text
        :rtype: String
        '''
        spellcheck_result = self.corrected_output(trace.current_text)
        return spellcheck_result

    def corrected_output(self, text: str = None):
        '''This is the core method in the spellchecker module

        :param utterance: in case we use this function externally, deafult None
        :returns: Corrected string, confidence ratio to each token and confidence average
        :rtype: Dictionary with the following structure,
                {
                    "corrected_output": "<uttr>",
                    "confidence": {"token#1": conf, "token#2": conf, ...etc.},
                    "confidence_avg": float_vag}
                }
        '''
        if not len(text):
            return None
        
        try:
            min_confidence = thinkbox_config['spellcheck_options']['min_confidence']
            max_confidence = thinkbox_config['spellcheck_options']['max_confidence']
            confidence_cutoff = thinkbox_config['spellcheck_options']['confidence_cutoff']
        except Exception as e:
            logging.error(e)

        corrected_list = []
        result = {}
        confidence = {}
        source = {}
        for token in text.split():
            if SpecialCharacters.check_special_char(token):
                temp_token = token
            else:
                temp_token = SpecialCharacters.remove_special_chars(token)
                temp_token = SpellcheckNormalizer.duplicate_normalizer(temp_token)
            # To Change
            # if SpecialCharacters.ends_with_special_chars(token) is False:
            #     token = temp_token
            language = utils.detect_language(token) 
            if not self.check_spelling(temp_token, language):
                correct_token = self.get_suggestions(temp_token, language)
                if isinstance(correct_token, list):
                    confidence_ = correct_token[1]
                    correct_token = correct_token[0]
                    source_ = "protected"
                elif correct_token[-8:] in {
                    '@a_fixed',
                    '@t_fixed',
                    '@y_fixed'
                }:
                    confidence_ = max_confidence
                    correct_token = correct_token.replace(correct_token[-8:], '')
                    if correct_token.startswith("أل"):
                        correct_token = correct_token.replace("أل", "ال")
                    source_ = "normalizer"
                else:
                    confidence_ = AccuracyMetric.token_confidence(correct_token, token)
                    source_ = "hunspell"
                if confidence_ < confidence_cutoff:
                    confidence_ = min_confidence
                    corrected_list.append(token)
                else:
                    corrected_list.append(correct_token)
                confidence[token] = confidence_
                source[token] = source_
            else:
                source[token] = "dictionary"
                corrected_list.append(token)
                confidence[token] = max_confidence
            result["corrected_output"] = " ".join(corrected_list)
            result["confidence"] = confidence
            logging.debug(source)
            result["confidence_avg"] = AccuracyMetric.confidence_average(list(result["confidence"].values()), len(result["confidence"]))
        return result

    def check_protected_word(self, word):
        '''Check if exists as protected word"
        :param words: word
        :type params: string
        :returns: True or false
        :rtype: boolean
        '''

        if word in self.protected_words:
            return True
        return False

    def check_spelling(self, token: str, language):
        
        '''Check if string spelled correctly (exists in dictionary)"
        :param words: token
        :type params: string
        :returns: True or false
        :rtype: boolean
        '''
        if token.isdigit():
            return True
        if language == 'ar':
            return self.spell_obj_ar.spell(token)
        else:
            return self.spell_obj_en.spell(token)

    def add_to_speelcheck_temp(self, word: str, language):
        if language == 'ar':
            self.spell_obj_ar.add(word)
        else:
            self.spell_obj_en.add(word)

    def get_suggestions(self, token: str, language):
        
        '''Get the most similar word to wrong token"
        :param words: token
        :type params: string
        :returns: Suggested string
        :rtype: str
        '''

        suggestions_list = None
        if token in self.protected_words:
            return [self.protected_words[token], 1.0]

        if language == 'ar':
            suggestions_list = self.spell_obj_ar.suggest(token)
        else: 
            suggestions_list = self.spell_obj_en.suggest(token)

        for item in suggestions_list:
            try:
                alef_fixed = SpellcheckNormalizer.check_alef(token, item)
                taa_fixed =  SpellcheckNormalizer.check_taa(token, item)
                yaa_fixed =  SpellcheckNormalizer.check_yaa(token, item)
                '''
                    NOTE: RETURNING taa_fixed should be before retuning alef_fixed,
                    If you have to switch please make sure that the edit distance
                    is only one
                '''
                if taa_fixed is not None:
                    return taa_fixed + "@t_fixed"

                if alef_fixed is not None:
                    return alef_fixed + "@a_fixed"

                if yaa_fixed is not None:
                    return yaa_fixed + "@y_fixed"

            except Exception as e:
                logging.error(e)
                continue
            
        if not suggestions_list:
            return token
        return suggestions_list[0]
