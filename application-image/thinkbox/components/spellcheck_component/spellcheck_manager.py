from thinkbox_apis.models.protected_word import ProtectedWord
from thinkbox_apis.models.dictionary_words import DictionaryWord
import hunspell
import logging

spellchecker_ar = None
protected_words = None
spellchecker_en = None

def reload_dictionaries(dictionary_path, aff_file_path):
    global protected_words
    global spellchecker_ar
    global spellchecker_en

    for language in ['ar', 'en']:
        prepare_dictionary(dictionary_path, language)
    spellchecker_ar = initialize_hunspell(dictionary_path, aff_file_path, 'ar')
    spellchecker_en = initialize_hunspell(dictionary_path, aff_file_path, 'en')
    pwd_object = ProtectedWord()
    protected_words = pwd_object.get_p_words_dict()
    logging.info("Protected words ready to be used and Spellcheck ready to be used")


def prepare_dictionary(dictionary_path, language):
    try:
        w = open("{0}{1}.dic".format(dictionary_path, language), "w")
        word = DictionaryWord()
        dictionary_words = word.get_words(language)
        dictionary_words_str = []

        for record in dictionary_words:
            if record.affixation_rule == None:
                dictionary_words_str.append(record.word)
            else:
                dictionary_words_str.append('/'.join([record.word, record.affixation_rule]))

        w.write(str(len(dictionary_words_str)) + '\n')

        chunks = [dictionary_words_str[x:x+10000] for x in range(0, len(dictionary_words_str), 10000)]
        for chunk in chunks:
            w.write('\n'.join(chunk))
        logging.info("Finished preparing .dic file")
    except Exception as e:
        logging.error("Error in dictionary creation" + str(e))


def initialize_hunspell(dictionary_path, aff_file_path, language):
    spellchecker = None
    try:
        spellchecker = hunspell.HunSpell(
            "{0}{1}.dic".format(dictionary_path, language),
            "{0}{1}.aff".format(aff_file_path, language),
        )
        logging.info("Finished initializing hunspell")
    except IOError as e:
        logging.error("Error initializing hunspell" + str(e))
    return spellchecker


def add_dictionary(dictionary_path, language, spellchecker):
# Be careful, the pyhunspell add_dic function does not load a new affixation file. 
# It will use the affixation file from the current hunspell object, and therefore it is not suitable
# for adding a dictionary for a new language. It is suitable for adding a new domain to the current dictionary. 
# For example adding an english medical terms dictionary to the english hunspell object.
    try:
        spellchecker.add_dic("{0}{1}.dic".format(dictionary_path, language))
    except IOError as e:
        logging.error("Error adding hunspell dictionary" + str(e))