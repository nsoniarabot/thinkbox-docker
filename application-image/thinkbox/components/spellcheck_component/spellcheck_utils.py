from difflib import SequenceMatcher
import re
from nltk.metrics import edit_distance
from difflib import SequenceMatcher
from collections import Counter


class AccuracyMetric():

    @staticmethod
    def token_edit_distance(corrected_token, original_token):
        return edit_distance(corrected_token, original_token)

    @staticmethod
    def token_confidence(corrected_token, original_token):
        return float('{:.2f}'.format(SequenceMatcher(None, corrected_token.lower(), original_token.lower()).ratio()))

    @staticmethod
    def confidence_average(confidence_list, token_len):
        return float('{:.2f}'.format(sum(confidence_list) / token_len))
    
    @staticmethod
    def suggestions_confidence(suggestions_list, original_token):        
        tokens_dict = {}
        for token in suggestions_list:
            tokens_dict[token] = AccuracyMetric.token_confidence(token.lower(),original_token)
            tokens_dict = {k: v for k, v in sorted(tokens_dict.items(), key=lambda item: item[1],reverse=True)}
        return tokens_dict


# TODO
# class ProtectedWords():
#     @staticmethod
#     def protected_dictionary():
#         pass

#     @staticmethod
#     def add_to_protected(mistake, correct):
#         pass

class SpellcheckNormalizer():

    @staticmethod
    def check_taa(token, suggestion):
        if len(token) != len(suggestion):
            return None

        check_list = ["ة", "ه"]
        for i, j in enumerate(token):
            if (j != suggestion[i] and suggestion[i] in check_list and j in check_list and i == len(token) - 1
                and AccuracyMetric.token_edit_distance(token, suggestion) == 1
            ):
                return suggestion
        return None
    
    @staticmethod
    def check_alef(token, suggestion):
        if len(token) != len(suggestion):
            return None
        check_list = ["أ", "إ", "ا", "آ"]
        for i, j in enumerate(token):
            if (j != suggestion[i] and suggestion[i] in check_list and j in check_list
                and AccuracyMetric.token_edit_distance(token, suggestion) == 1
            ):
                return suggestion
        return None

    @staticmethod
    def check_yaa(token, suggestion):
        if len(token) != len(suggestion):
            return None
        check_list = ["ي", "ى"]
        for i, j in enumerate(token):
            if (j != suggestion[i] and suggestion[i] in check_list and j in check_list and i == len(token) - 1
                and AccuracyMetric.token_edit_distance(token, suggestion) == 1
            ):
                return suggestion
        return None

    @staticmethod
    def duplicate_normalizer(token):
        clean_token = ""
        char_frequency = dict(Counter(token))
        for key, val in char_frequency.items():
            if val >= 3:
                clean_token = token.replace(key*val, key*2)
        if len(clean_token):
            return clean_token
        return token

class SpecialCharacters():

    @staticmethod
    def check_special_char(token):
        return True if re.sub('[^A-Za-z0-9أ-ي]+', '', token) == '' else False

    @staticmethod
    def remove_special_chars(token):
        return re.sub('[^A-Za-z0-9أ-ي]+', '', token)

    @staticmethod
    def ends_with_special_chars(token):
        return True if token[-1] in ['?','!'] else False


