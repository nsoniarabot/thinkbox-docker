from gensim.parsing.preprocessing import split_alphanum
from pyarabic.araby import strip_tashkeel, strip_tatweel, normalize_hamza
from collections import OrderedDict
from string import punctuation
from trace import Trace
from components.base_component import BaseComponent

punctuation += "؟،'"
trans_table = str.maketrans('', '', punctuation)


def strip_punctuation(text):
    return text.translate(trans_table)


def lower(text):
    return text.lower()


class TextPreprcossing(BaseComponent):
    def __init__(self):
        super().__init__()
        return

    def process(self, trace: Trace, params):
        return TextPreprocessingPipelines.process(trace.current_text)


class TextPreprocessingPipelines:
    preprocessors = OrderedDict({
        'lower': lower,
        'split_alphanum': split_alphanum,
        'strip_punctuation': strip_punctuation,
        'strip_tashkeel': strip_tashkeel,
        'strip_tatweel': strip_tatweel,
        'normalize_hamza': normalize_hamza,
    })

    default_preprocessors = [
        'lower',
        'split_alphanum',
        'strip_punctuation',
        'strip_tashkeel',
        'strip_tatweel',
        'normalize_hamza'
    ]

    @staticmethod
    def process(text, pipeline=default_preprocessors):
        for preprosessor_name in pipeline:
            if preprosessor_name in TextPreprocessingPipelines.preprocessors:
                text = TextPreprocessingPipelines.preprocessors[preprosessor_name](text)
            else:
                raise Exception('processor \'{}\' was not found'.format(preprosessor_name))
        return text
