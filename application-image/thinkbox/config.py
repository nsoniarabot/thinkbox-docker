import yaml
import os

if os.path.isfile('local.yml'):
    with open("local.yml", 'r') as ymlfile:
        thinkbox_config = yaml.safe_load(ymlfile)
else:
    with open("config.yml", 'r') as ymlfile:
        thinkbox_config = yaml.safe_load(ymlfile)


def db_uri():
    db_config = thinkbox_config['database']['thinkbox']
    user = db_config['user']
    password = db_config['password']
    host = db_config['host']
    port = db_config['port']
    db = db_config['db']
    db_info = f'postgresql://{user}:{password}@{host}:{port}/{db}'
    return db_info


class FlaskConfig:
    SECRET_KEY = thinkbox_config['flask_app']['secret_key']
    SQLALCHEMY_DATABASE_URI = db_uri()
    PRESERVE_CONTEXT_ON_EXCEPTION = thinkbox_config['flask_app']['preserve_context_on_exception']
    SQLALCHEMY_TRACK_MODIFICATIONS = thinkbox_config['flask_app']['sqlalchemy_track_modifications']
    SQLALCHEMY_COMMIT_ON_TEARDOWN = thinkbox_config['flask_app']['sqlalchemy_commit_on_teardown']
    SQLALCHEMY_BINDS = {thinkbox_config['database']['thinkbox']['schema']: db_uri()}
    DEBUG =  thinkbox_config['flask_app']['debug']
    TESTING =  thinkbox_config['flask_app']['testing']