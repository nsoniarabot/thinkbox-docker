from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension
from config import FlaskConfig

db = SQLAlchemy()
toolbar = DebugToolbarExtension()


def create_app():
    app = Flask(__name__)
    app.config.from_object(FlaskConfig)
    db.init_app(app)
    extentions(app)

    return app

def extentions(app):
    toolbar.init_app(app)
    return None

app = create_app()