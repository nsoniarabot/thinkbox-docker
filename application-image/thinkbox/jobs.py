from apscheduler.schedulers.background import BackgroundScheduler
from classifier.intent_classifier_preprocessing.intent_classifier_preprocessing import *
from classifier.classifier_manager import ClassifierManager
from sklearn.feature_extraction.text import TfidfVectorizer
from classifier.classifier import SVM
import logging

from thinkbox_apis.models.label import Label
from thinkbox_apis.models.training_sample import TrainingSample

class JobUtils():
    running_jobs = None
    scheduler = None

    @staticmethod
    def initialize_jobs():
        JobUtils.running_jobs = set([])
        JobUtils.scheduler = BackgroundScheduler()
        JobUtils.scheduler.start()

    @staticmethod
    def validate_training_data_format(training_data_headers, records, profile_id):
        expected_columns = ['text', 'category_id']
        training_data_headers.sort()
        expected_columns.sort()
        if training_data_headers == expected_columns:
            mapping_ids = records['category_id'].unique()
            label_mappings = Label.get_label_mappings(profile_id)
            for id in mapping_ids:
                if id not in label_mappings.keys():
                    return False
        else:
            return False
        return True


    @staticmethod
    def start_train_model_job(profile_id, models_path):
        ts_object = TrainingSample()
        if profile_id.isdigit() \
                and ts_object.check_training_samples_exists(profile_id) > 0:
            profile_id = int(profile_id)
        else:
            return "error"
        if profile_id in JobUtils.running_jobs:
            return "already training"
        JobUtils.scheduler.add_job(JobUtils.train_model, coalesce=True, id=str(profile_id), args=[profile_id, models_path])
        return "success"

    @staticmethod
    #TODO This function needs fixing - get correct label_ids and mapping_ids
    def insert_data(data_file, profile_id):
        try:
            ts = TrainingSample()
            response = ts.insert_training_data(data_file, profile_id)
            if JobUtils.validate_training_data_format(data_file.columns.tolist(), data_file, profile_id):
                if response:
                    return True
                else:
                    return 'err'
            else:
                return False
        except Exception as e:
            logging.error(e)

    @staticmethod
    def train_model(profile_id, path):
        JobUtils.running_jobs.add(profile_id)
        logging.info("starting training for profile_id = " + str(profile_id))
        data = ClassifierManager.get_data(profile_id)
        if data is None:
            logging.warn("no data found for profile_id = " + str(profile_id))
            return None

        tokn = IntentClassifierBasicPreprocessing(data)
        v2 = TfidfVectorizer()
        classifier = SVM(profile_id)

        classifier_manager = ClassifierManager(profile_id, tokn, classifier)
        logging.info("starting training, model id = " + str(profile_id))

        inserted_id = classifier_manager.train_model(v2, 0.2, True, True)
        logging.info("model training finished, version = " + str(inserted_id))

        if inserted_id is not None:
            classifier_manager.save_model(path, inserted_id)
            logging.info("model saved successfully version = " + str(inserted_id))
        else:
            logging.error("something went wrong, id = " + str(inserted_id))
        JobUtils.running_jobs.remove(profile_id)