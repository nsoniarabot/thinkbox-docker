from config import thinkbox_config
from flask_script import Manager
from thinkbox_apis import blueprint
import app_init
from create_app import app
from py4j.clientserver import (ClientServer,
                                JavaParameters,
                                PythonParameters
)
from thinkbox_apis.util.py4j_interface import UserIntent

app.register_blueprint(blueprint)
app.app_context().push()
app_init.initialize_all(thinkbox_config['logging']['flask_files_path'], thinkbox_config['dictionaries']['dict_path'])
understand_text = UserIntent()
ClientServer(
    java_parameters=JavaParameters(),
    python_parameters=PythonParameters(
        address=thinkbox_config['py4j']['python_host'],
        port=thinkbox_config['py4j']['python_port']
    ),
    python_server_entry_point=understand_text
)
manager = Manager(app)

# migrate = Migrate(app, db)
#
# manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run(use_reloader=False)

if __name__ == '__main__':
    manager.run()
