ALTER TABLE custom_entity ADD COLUMN enable_parsing boolean DEFAULT FALSE;
--------------------------------------------------------------------------
INSERT INTO custom_entity (profile_id, "name", enable_parsing) VALUES (2, 'loan_account_number', TRUE);
--------------------------------------------------------------------------
INSERT INTO thinkbox.custom_entity_values (custom_entity_id, value)	VALUES
(5, 'المديونية رقم'),
(5, 'مديونيه برقم '),
(5, 'مديونيه برقم المرجع'),
(5, 'رقم المطالبة'),
(5, 'مطالبة برقم'),
(5, 'مطالبة رقم'),
(5, 'حساب رقم'),
(5, 'رقم الهوية لحساب التويل التاجيرى'),
(5, 'حساب التمويل العقاري رقم'),
(5, 'حساب التمويلي رقم'),
(5, 'حساب التمويلي رقم:'),
(5, 'على حساب التمويل');