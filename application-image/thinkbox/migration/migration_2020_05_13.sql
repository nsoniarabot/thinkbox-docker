CREATE TYPE EXTRACTION_STRATEGY AS ENUM ('UNIQUE', 'AMBIGUOUS');
CREATE TYPE GEO_TYPE AS ENUM ('CITY', 'COUNTRY', 'REGION', 'CONTINENT', 'AIRSTATION');

CREATE TABLE thinkbox.geo_locations (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    arabic_city_name varchar NULL,
    arabic_country_name varchar NULL,
    arabic_name varchar NOT NULL,
    arabic_nationality varchar NULL,
    arabic_official varchar NULL,
    capital_name varchar NULL,
    english_city_name varchar NULL,
    english_country_name varchar NULL,
    english_name varchar NOT NULL,
    english_nationality varchar NULL,
    english_official varchar NULL,
    extract_strategy EXTRACTION_STRATEGY,
    geo_type GEO_TYPE,
    iata varchar,
    iso2 varchar NULL,
    iso3 varchar NOT NULL,
    longlat varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TRIGGER geo_locations_updated_at BEFORE UPDATE ON thinkbox.geo_locations FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.geo_keywords (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    keyword varchar NOT NULL,
    identifier varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER geo_keywords_updated_at BEFORE UPDATE ON thinkbox.geo_keywords FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.geo_pre_phrases (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    phrase varchar NOT NULL,
    replacement varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TRIGGER geo_pre_phrases_updated_at BEFORE UPDATE ON thinkbox.geo_pre_phrases FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();
CREATE TABLE thinkbox.geo_stopwords (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    word varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TRIGGER geo_stopwords_updated_at BEFORE UPDATE ON thinkbox.geo_stopwords FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();
CREATE TABLE thinkbox.geo_synonyms (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    word varchar NOT NULL,
    synonym varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER geo_synonyms_updated_at BEFORE UPDATE ON thinkbox.geo_synonyms FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.geo_protectedwords (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    word varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER geo_protectedwords_updated_at BEFORE UPDATE ON thinkbox.geo_protectedwords FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


CREATE TYPE NAME_TYPE AS ENUM ('LAST_NAME', 'MALE', 'FEMALE', 'NEUTRAL');


CREATE TABLE thinkbox.names (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    name varchar NOT NULL,
    name_en varchar NOT NULL,
    type NAME_TYPE,
    is_ambiguous bool,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER names_updated_at BEFORE UPDATE ON thinkbox.names FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


CREATE TABLE thinkbox.names_protectedwords (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    word varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER names_protectedwords_updated_at BEFORE UPDATE ON thinkbox.names_protectedwords FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.names_synonyms (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    word varchar NOT NULL,
    synonym varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER names_synonyms_updated_at BEFORE UPDATE ON thinkbox.names_synonyms FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.parsers (
    id SERIAL PRIMARY KEY,
    parser_name varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TRIGGER parsers_update BEFORE UPDATE ON thinkbox.parsers FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();



CREATE TABLE thinkbox.regex_parsers (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    regex varchar NOT NULL,
    mapped_value varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    parser_id integer
);

CREATE TRIGGER regex_parsers_updated_at BEFORE UPDATE ON thinkbox.regex_parsers FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


CREATE TABLE thinkbox.list_parsers (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    list_parsers varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    parser_id integer
);
CREATE TRIGGER list_parsers_updated_at BEFORE UPDATE ON thinkbox.list_parsers FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


alter table thinkbox.geo_locations add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.geo_keywords add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.geo_pre_phrases add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.geo_stopwords add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.geo_synonyms add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.geo_protectedwords add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.names add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.names_protectedwords add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.names_synonyms add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.regex_parsers add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.list_parsers add foreign key (profile_id) references thinkbox.profiles(id);



ALTER TABLE thinkbox.list_parsers ADD foreign key (parser_id) REFERENCES thinkbox.parsers(id);
ALTER TABLE thinkbox.regex_parsers ADD foreign key (parser_id) REFERENCES thinkbox.parsers(id);


CREATE TABLE thinkbox.name_pre_phrases (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    phrase varchar NOT NULL,
    replacement varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER name_pre_phrases_updated_at BEFORE UPDATE ON thinkbox.name_pre_phrases FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE TABLE thinkbox.name_keywords (
    id SERIAL PRIMARY KEY,
    profile_id int NULL,
    keyword varchar NOT NULL,
    identifier varchar NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now()
);
CREATE TRIGGER name_keywords_updated_at BEFORE UPDATE ON thinkbox.name_keywords FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();





alter table thinkbox.name_pre_phrases add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.name_keywords add foreign key (profile_id) references thinkbox.profiles(id);
alter table thinkbox.names add foreign key (profile_id) references thinkbox.profiles(id);
