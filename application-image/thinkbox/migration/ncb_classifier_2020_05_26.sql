insert into thinkbox.profiles (id, name) values (4, 'ncb_cards'), (5, 'ncb_financing'), (6, 'ncb_channels'), (7, 'ncb_payment');

delete from thinkbox.labels where profile_id = 2;

insert into thinkbox.labels (mapping_id, priority, profile_id, name) values 
(1, 0, 2, 'Financing&Leasing')
,(0, 0, 2, 'Alahli Cards')
,(2, 0, 2, 'Alahli Channels')
,(3, 0, 2, 'Payment Operations')
,(4, 0, 2, 'Corporate Services')
,(5, 0, 2, 'others');


insert into thinkbox.labels (mapping_id, priority, profile_id, name) values (0, 0, 7, 'Payment Operations;International;Cancellation;Reason of Cancellation')
,(1, 0, 7, 'Payment Operations;International;Cancellation;Request For Cancellation')
,(2, 0, 7, 'Payment Operations;International;Fate of Payment;Ben Claim Non Receipt of Funds')
,(3, 0, 7, 'Payment Operations;International;Fate of Payment;Inquiry of Payment')
,(4, 0, 7, 'Payment Operations;Quick Pay;Trans Cancelation;nan')
,(5, 0, 7, 'Payment Operations;Quick Pay;Trans Not Received;nan')
,(6, 0, 7, 'Payment Operations;Sarie;Fate of Payment;Ben Claim Non Receipt of Funds')
,(7, 0, 7, 'others');

insert into thinkbox.labels (mapping_id, priority, profile_id, name) values (0, 0, 5, 'Financing&Leasing;Auto Lease;Account Services;Add/ Cancel Direct Debit' )
,(1, 0, 5, 'Financing&Leasing;Auto Lease;After Sales Service;Delay in ballon Payment' )
,(2, 0, 5, 'Financing&Leasing;Auto Lease;Fees and Installments;Reverse additional amount' )
,(3, 0, 5, 'Financing&Leasing;Collections Services;Complaint after Reposessed Car;nan' )
,(4, 0, 5, 'Financing&Leasing;Collections Services;Complaint on Collection;nan' )
,(5, 0, 5, 'Financing&Leasing;Collections Services;Rescheduling Request;nan' )
,(6, 0, 5, 'Financing&Leasing;Personal Finance;Reverse Early Deduction;nan' )
,(7, 0, 5, 'Financing&Leasing;Real Estate Finance;Discrepancy Install Amount;nan')
,(8, 0, 5, 'Financing&Leasing;SIMAH;Rejection Reason;nan' )
,(9, 0, 5, 'others' );


insert into thinkbox.labels (mapping_id, priority, profile_id, name) values (0, 0, 4, 'Alahli Cards;ATM Cards;NCB ATM - Deposit;ATM Deposit' )
,(1, 0, 4, 'Alahli Cards;ATM Cards;NCB ATM - Withdrawal;ATM Cash Withdrawal - NCB' )
,(2, 0, 4, 'Alahli Cards;Credit Cards;Financial Transactions;objection on the annual fees' )
,(3, 0, 4, 'Alahli Cards;Credit Cards;Financial Transactions;Paid amounts not ref on card' )
,(4, 0, 4, 'Alahli Cards;Credit Cards;Financial Transactions;Reverse Duplicate Payment' )
,(5, 0, 4, 'Alahli Cards;Credit Cards;Financial Transactions;Tayseer Markup/Service Charges' )
,(6, 0, 4, 'Alahli Cards;Credit Cards;Financial Transactions;Transact. Disputes' )
,(7, 0, 4, 'Alahli Cards;Credit Cards;Programs;Objection on Programs' )
,(8, 0, 4, 'others' );

insert into thinkbox.labels (mapping_id, priority, profile_id, name) values (0, 0, 6, 'Alahli Channels;Alahli Online & Mobile;Alahli Online Service;Report Fraud')
,(1, 0, 6, 'Alahli Channels;Branches;Account Issues;reson for Stopped Account')
,(2, 0, 6, 'Alahli Channels;Branches;Account Issues;SAMA Instructions')
,(3, 0, 6, 'Alahli Channels;Branches;Account Issues;Unkown deduction')
,(4, 0, 6, 'Alahli Channels;Branches;Branch Services;bad behav from Branch employee')
,(5, 0, 6, 'others');



insert into thinkbox.classifier_models (hyperparameters, metric, metric_value, vectorizer, preprocessor, model_type, profile_id, confidence_cutoff, active_on_test, active_on_live) 
values ('', 'accuracy', 0.9,  'TfidfVectorizer' ,'IntentClassifierBasicPreprocessing','ThinkboxLogisticRegression', 2,  0.6, true, true),
('', 'accuracy', 0.9,  'TfidfVectorizer' ,'IntentClassifierBasicPreprocessing','ThinkboxLogisticRegression', 4,  0.6, true, true), 
('', 'accuracy', 0.9,  'TfidfVectorizer' ,'IntentClassifierBasicPreprocessing','ThinkboxLogisticRegression', 5,  0.6, true, true), 
('', 'accuracy', 0.9,  'TfidfVectorizer' ,'IntentClassifierBasicPreprocessing','ThinkboxLogisticRegression', 6,  0.6, true, true), 
('', 'accuracy', 0.9,  'TfidfVectorizer' ,'IntentClassifierBasicPreprocessing','ThinkboxLogisticRegression', 7,  0.6, true, true);