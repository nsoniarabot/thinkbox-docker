USE thinkbox;

CREATE TABLE `dictionary_words` (
  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
  `language` varchar(100) NOT NULL, 
  `word` varchar(200) NOT NULL,
  `affixation_rule` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;