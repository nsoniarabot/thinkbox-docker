CREATE TYPE thinkbox.manual_classification_source AS ENUM (
    'default',
    'manual_classification_tool'
);

CREATE TYPE thinkbox.manual_classification_status AS ENUM (
    'pending',
    'correct',
    'incorrect',
    'reclassified'
);


CREATE TABLE thinkbox.manual_classification (
    id bigint NOT NULL,
    status thinkbox.manual_classification_status,
    label_id bigint NOT NULL,
    new_label_id bigint,
    text text NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    source thinkbox.manual_classification_source
);

ALTER TABLE ONLY thinkbox.manual_classification
    ADD CONSTRAINT manual_classification_label_id_fkey FOREIGN KEY (label_id) REFERENCES thinkbox.labels(id);

ALTER TABLE ONLY thinkbox.manual_classification
    ADD CONSTRAINT manual_classification_new_label_id_fkey FOREIGN KEY (new_label_id) REFERENCES thinkbox.labels(id);

ALTER TABLE ONLY thinkbox.manual_classification
    ADD CONSTRAINT manual_classification_pkey PRIMARY KEY (id);

CREATE TRIGGER manual_classification_updated_at BEFORE UPDATE ON thinkbox.manual_classification FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();

CREATE SEQUENCE thinkbox.manual_classification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE thinkbox.manual_classification_id_seq OWNED BY thinkbox.manual_classification.id;
ALTER TABLE ONLY thinkbox.manual_classification ALTER COLUMN id SET DEFAULT nextval('thinkbox.manual_classification_id_seq'::regclass);




-- insert into manual_classification (label_id, text, status, source) values (2174,'اود استلامها بفرعكم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'اوصيل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'اين استلم بضاعتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'اين استلم طلبي٠', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'اين مكان الاستلام', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'باكرصباحا', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'بتوصلي للبيت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'بجي اخذها', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'بدي اغير الموقع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'ترصيل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'تزصيل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'تصويل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'تغيير موعد الاستلام', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'تواصل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصبل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصلون للبيت؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصيلل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصيللللل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصيلها للبيت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصيييييلللل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توصیل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'توضيلجدة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'جدة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'جدولة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'جدوله', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'جدوله الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'راح توصلني البيت ولا انا اروح استلمها؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'شحتاتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'طريقة الاستلام', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'طريقة التسليم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'طريقة التسليم؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف استطيع ان استلم الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف استلم الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف استلم الطلب ؟.', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف استلم بضاعتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف استلمها', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف اقدر استلمها؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف بدك تعتلي اياها', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف توصل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف لي ان استلم الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف يمكننا ان نستلم الطلب؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيف يمكننني استيلام الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'كيفية التواصل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'مكان التسليم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'ممكن أغير الموقع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'نوصيل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'هل ستوصله إلى البيت؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'هل من الممكن استلمت اليوم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'هل يمكمنكم توصيلها للمنزل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'وين استلم اغراض', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'وين استلم الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'وين توصل الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (2174,'يعني استلمها من اركمس', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'أين شحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'اغراضي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipments history', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipments status', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipments statuses', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'All shipments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'وين شحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'History', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'هل لدي طلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'هل عندي طلبات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'هل عندي طلبيات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'طلبياتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'طرودي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'أين الشحنات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'اريد معرفه الطلبات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'طلباتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipmenta', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipmates', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'myshipments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shippments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shippment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipmemts', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'myshipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shopments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شخناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شاحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'سحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناني', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتس', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شجناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتك', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناته', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'where is my shipment?', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'where are my shipments?', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'أين شحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'طلبية', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'status', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my status', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'shipments list', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'وضع شحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'orders', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my orders', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Myshjpment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmenst', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shepment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmentd', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Myshipmenta', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ارجو تفصيل محتويات الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shiment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحانتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Mu shippment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'وصلتك', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My ahipments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmeants', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'parcel', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'parcels', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'اوردر', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'consignment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'consignments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'packages', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتی', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'اليوم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'رقم الطب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماهي الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'تم ابلاغي بوجود سحنه باسمي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحتني', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحواتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنتة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحاني', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحاتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنتاتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الشخنات الخاصه بي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحانتى', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شخنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الطلبية', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'انا كم طلبيه موجود لي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'حالة الشحن', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'هل لدي طرد لي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ابستفسر عن طلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماذا حدث لشحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنتكي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ابي رقم لشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'في طلبيه لي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ممكن رقم الاوردر', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماهو الطلب يالغالي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنلتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'وش الطلب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الطلب وصل ؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ممكن تعرفني ايش الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'استعلام', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ابغي تفاصيل الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'اريد استعلام عن طلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ما هي الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'انا لي طلب عندكم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماهي الطلبيه الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الطلبيه اللي طلبتها', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ششحناتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'موجود لي طلب بفرعكم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'طلبتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماهى الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'بضاعتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتبي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماي شبمنت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحناتتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ممكن اعرف عن الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ماذا حدث لطلبي؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'الاوردرات', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'سحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنتتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ايش هي الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'شحنانتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Update', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shiptments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmants', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shimpments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmintes', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipements', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My dhipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmemt', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shpments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My material', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipmenys', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My deleivery status', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shipmements', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shopment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmets', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'my shiments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'how much i have shippment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My ?', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmints', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipents', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipnent', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipme', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shioments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shimpents', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Shipnents', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My chipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Shipmens', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'How many items', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'Shippment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My Shipmens', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shipmrnts', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'My shepments', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ايش الشحنة؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ايش الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1860,'ايش الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'track', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'tracking', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'تتبع الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'تتبع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'بدي اتبع شحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'ارسالية', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متابعة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'الطرد', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين مندوبكم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'أين السائق', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين الدريويل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل ستتوصل الإرسالية للمنزل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'أي وقت بتوصل الرسالة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى أستلم البريد', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى تصل الأغراض', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى يوصلني السايق', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'طلعت أغراضي مع الدريويل', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل أغراضي مع المندوب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل ستصل الإرسالية اليوم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'باي مكان موجودة الاغراض', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل يوجد الطرد في المكتب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'package', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'where can i find the parcel', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'can i track my hawb', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'which office has my package', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'can i find the package in the office', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'is my package out for delivery', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'when will i receive the delivery', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'what time is the delivery', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'can you deliver my parcel', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'can i expect the delivery', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'where is the driver', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'where is the courier', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'do you have home delivery', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'اتتبع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'أتتبع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'شحنتي باي فرع', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'شحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'Where is my parcel', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'i need to locate shipment', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'is shipment with the driver', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'shipping', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين وصلت الشحنه ؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى تصل الشحنة ؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'لدي طلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى بيوصلني الغرض؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وصلت الطلبيه؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى بتوصل لعندي؟؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى توصل لعندي؟؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'اين اجد طلبيتي الان', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى استلام الطلب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى توصل الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'شحنة وينه وصلت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'شحنة وين وصلت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وصلت طلبيتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'حبيت اسال متى تجي الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل وصل هذه الطلبيه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل وصل هذه الطلبية', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وصلت الشحنه ام لا؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'استفسار عن الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'استفسر عن الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى استلمها ؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'الشحنة فين وصلت', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'عندي استفسار بخصوص شحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'عندي استفسار بخصوص شحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'رقم الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'ابي استفسر عن شحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'ابي استفسر عن شحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'ابي استفسر عن شحنة رقم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'حابه اعرف متى الطلب بوصل بالضبط', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'الشحنة رقم', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'When will you drop my parcel?', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'Tracks', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'تعقب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'تعقب الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'تعقب الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'هل يمكن الاستعلام عن الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'الاستعلام عن الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'بحث عن شحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'بحث عن شحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'البحث عن شحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'my order', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'كيف اتتبع شحنتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'اين الشحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'اريد اعرف حال طلبي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى ستصل الشحنة', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى وصول شحنه', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى يوصل الطلب', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين وصلت الشحنه؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'طلبيتي وصلت؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'متى بتوصل الطلبيه؟', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وين طلبيتي', 'pending', 'manual_classification_tool');
-- insert into manual_classification (label_id, text, status, source) values (1771,'وصلت الشحنة', 'pending', 'manual_classification_tool');

