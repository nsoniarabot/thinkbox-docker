--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Ubuntu 11.7-2.pgdg19.10+1)
-- Dumped by pg_dump version 11.7 (Ubuntu 11.7-2.pgdg19.10+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: thinkbox; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA thinkbox;


--
-- Name: dictionary_words_source; Type: TYPE; Schema: thinkbox; Owner: -
--

CREATE TYPE thinkbox.dictionary_words_source AS ENUM (
    'default',
    'editor'
);


--
-- Name: rules_rule_type; Type: TYPE; Schema: thinkbox; Owner: -
--

CREATE TYPE thinkbox.rules_rule_type AS ENUM (
    'exact',
    'keyword',
    'regex',
    'parser'
);


--
-- Name: training_sample_source; Type: TYPE; Schema: thinkbox; Owner: -
--

CREATE TYPE thinkbox.training_sample_source AS ENUM (
    'default',
    'classification_tool'
);


--
-- Name: current_timestamp_on_update(); Type: FUNCTION; Schema: thinkbox; Owner: -
--

CREATE FUNCTION thinkbox.current_timestamp_on_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN
      NEW.updated_at = now(); 
      RETURN NEW;
   ELSE
      RETURN OLD;
   END IF;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: classifier_models; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.classifier_models (
    id bigint NOT NULL,
    hyperparameters text NOT NULL,
    metric character varying(45) DEFAULT NULL::character varying,
    metric_value double precision,
    vectorizer character varying(45) DEFAULT NULL::character varying,
    preprocessor character varying(45) DEFAULT NULL::character varying,
    model_type character varying(45) DEFAULT NULL::character varying,
    profile_id bigint,
    confidence_cutoff double precision,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    active_on_test boolean,
    active_on_live boolean
);


--
-- Name: classifier_models_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.classifier_models_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: classifier_models_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.classifier_models_id_seq OWNED BY thinkbox.classifier_models.id;


--
-- Name: dictionary_words; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.dictionary_words (
    id bigint NOT NULL,
    language character varying(100) NOT NULL,
    word character varying(200) NOT NULL,
    affixation_rule character varying(200) DEFAULT NULL::character varying,
    dialect character varying(255) DEFAULT NULL::character varying,
    source thinkbox.dictionary_words_source,
    profile_id bigint,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    active_on_test boolean DEFAULT true NOT NULL,
    active_on_live boolean DEFAULT false NOT NULL
);


--
-- Name: dictionary_words_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.dictionary_words_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dictionary_words_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.dictionary_words_id_seq OWNED BY thinkbox.dictionary_words.id;


--
-- Name: labels; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.labels (
    id bigint NOT NULL,
    mapping_id bigint,
    priority integer,
    profile_id bigint NOT NULL,
    name character varying(255),
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: intents_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.intents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: intents_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.intents_id_seq OWNED BY thinkbox.labels.id;


--
-- Name: profiles; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.profiles (
    id bigint NOT NULL,
    name character varying(255),
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.profiles_id_seq OWNED BY thinkbox.profiles.id;


--
-- Name: protected_words; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.protected_words (
    id bigint NOT NULL,
    language character varying(100) NOT NULL,
    word character varying(200) NOT NULL,
    correct_word character varying(200) NOT NULL,
    profile_id bigint,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    active_on_test boolean DEFAULT true NOT NULL,
    active_on_live boolean DEFAULT false NOT NULL
);


--
-- Name: protected_words_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.protected_words_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: protected_words_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.protected_words_id_seq OWNED BY thinkbox.protected_words.id;


--
-- Name: rules; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.rules (
    id bigint NOT NULL,
    label_id bigint NOT NULL,
    rule_type thinkbox.rules_rule_type NOT NULL,
    keyword character varying(255) DEFAULT NULL::character varying,
    regex character varying(255) DEFAULT NULL::character varying,
    parser_name character varying(255),
    active_on_test boolean DEFAULT true,
    active_on_live boolean DEFAULT false,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: rules_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rules_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.rules_id_seq OWNED BY thinkbox.rules.id;


--
-- Name: search_api_config; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.search_api_config (
    bot_id integer NOT NULL,
    collection_name character varying(255) NOT NULL,
    intent_id integer NOT NULL,
    priority integer NOT NULL,
    search_api_url character varying(500) NOT NULL,
    org_id integer NOT NULL,
    profile_id bigint,
    id bigint NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: search_api_config_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.search_api_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_api_config_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.search_api_config_id_seq OWNED BY thinkbox.search_api_config.id;


--
-- Name: training_samples; Type: TABLE; Schema: thinkbox; Owner: -
--

CREATE TABLE thinkbox.training_samples (
    id bigint NOT NULL,
    label_id bigint NOT NULL,
    parent_id bigint,
    text text NOT NULL,
    active_on_test boolean DEFAULT true,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    source thinkbox.training_sample_source
);


--
-- Name: training_samples_id_seq; Type: SEQUENCE; Schema: thinkbox; Owner: -
--

CREATE SEQUENCE thinkbox.training_samples_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: training_samples_id_seq; Type: SEQUENCE OWNED BY; Schema: thinkbox; Owner: -
--

ALTER SEQUENCE thinkbox.training_samples_id_seq OWNED BY thinkbox.training_samples.id;


--
-- Name: classifier_models id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.classifier_models ALTER COLUMN id SET DEFAULT nextval('thinkbox.classifier_models_id_seq'::regclass);


--
-- Name: dictionary_words id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.dictionary_words ALTER COLUMN id SET DEFAULT nextval('thinkbox.dictionary_words_id_seq'::regclass);


--
-- Name: labels id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.labels ALTER COLUMN id SET DEFAULT nextval('thinkbox.intents_id_seq'::regclass);


--
-- Name: profiles id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.profiles ALTER COLUMN id SET DEFAULT nextval('thinkbox.profiles_id_seq'::regclass);


--
-- Name: protected_words id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.protected_words ALTER COLUMN id SET DEFAULT nextval('thinkbox.protected_words_id_seq'::regclass);


--
-- Name: rules id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.rules ALTER COLUMN id SET DEFAULT nextval('thinkbox.rules_id_seq'::regclass);


--
-- Name: search_api_config id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.search_api_config ALTER COLUMN id SET DEFAULT nextval('thinkbox.search_api_config_id_seq'::regclass);


--
-- Name: training_samples id; Type: DEFAULT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.training_samples ALTER COLUMN id SET DEFAULT nextval('thinkbox.training_samples_id_seq'::regclass);


--
-- Name: classifier_models idx_41201_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.classifier_models
    ADD CONSTRAINT idx_41201_primary PRIMARY KEY (id);


--
-- Name: dictionary_words idx_41215_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.dictionary_words
    ADD CONSTRAINT idx_41215_primary PRIMARY KEY (id);


--
-- Name: labels idx_41227_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.labels
    ADD CONSTRAINT idx_41227_primary PRIMARY KEY (id);


--
-- Name: protected_words idx_41233_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.protected_words
    ADD CONSTRAINT idx_41233_primary PRIMARY KEY (id);


--
-- Name: rules idx_41244_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.rules
    ADD CONSTRAINT idx_41244_primary PRIMARY KEY (id);


--
-- Name: training_samples idx_41256_primary; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.training_samples
    ADD CONSTRAINT idx_41256_primary PRIMARY KEY (id);


--
-- Name: profiles profiles_pkey; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: search_api_config search_api_config_pkey; Type: CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.search_api_config
    ADD CONSTRAINT search_api_config_pkey PRIMARY KEY (id);


--
-- Name: labels modify_classes_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_classes_updated_at BEFORE UPDATE ON thinkbox.labels FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: classifier_models modify_classifier_models_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_classifier_models_updated_at BEFORE UPDATE ON thinkbox.classifier_models FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: dictionary_words modify_dictionary_words_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_dictionary_words_updated_at BEFORE UPDATE ON thinkbox.dictionary_words FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: profiles modify_profiles_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_profiles_updated_at BEFORE UPDATE ON thinkbox.profiles FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: protected_words modify_protected_words_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_protected_words_updated_at BEFORE UPDATE ON thinkbox.protected_words FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: rules modify_rules_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_rules_updated_at BEFORE UPDATE ON thinkbox.rules FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: search_api_config modify_search_api_config_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_search_api_config_updated_at BEFORE UPDATE ON thinkbox.search_api_config FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: training_samples modify_training_samples_updated_at; Type: TRIGGER; Schema: thinkbox; Owner: -
--

CREATE TRIGGER modify_training_samples_updated_at BEFORE UPDATE ON thinkbox.training_samples FOR EACH ROW EXECUTE PROCEDURE thinkbox.current_timestamp_on_update();


--
-- Name: labels classes_profile_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.labels
    ADD CONSTRAINT classes_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES thinkbox.profiles(id);


--
-- Name: classifier_models classifier_models_profile_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.classifier_models
    ADD CONSTRAINT classifier_models_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES thinkbox.profiles(id);


--
-- Name: dictionary_words dictionary_words_profile_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.dictionary_words
    ADD CONSTRAINT dictionary_words_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES thinkbox.profiles(id);


--
-- Name: protected_words protected_words_profile_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.protected_words
    ADD CONSTRAINT protected_words_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES thinkbox.profiles(id);


--
-- Name: rules rules_class_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.rules
    ADD CONSTRAINT rules_class_id_fkey FOREIGN KEY (label_id) REFERENCES thinkbox.labels(id);


--
-- Name: search_api_config search_api_config_profile_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.search_api_config
    ADD CONSTRAINT search_api_config_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES thinkbox.profiles(id);


--
-- Name: training_samples training_samples_class_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.training_samples
    ADD CONSTRAINT training_samples_class_id_fkey FOREIGN KEY (label_id) REFERENCES thinkbox.labels(id);


--
-- Name: training_samples training_samples_parent_id_fkey; Type: FK CONSTRAINT; Schema: thinkbox; Owner: -
--

ALTER TABLE ONLY thinkbox.training_samples
    ADD CONSTRAINT training_samples_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES thinkbox.training_samples(id);


--
-- PostgreSQL database dump complete
--

