#WARNING: code is outdated - used as a reference for parserv2 queries only

import sqlalchemy
from sqlalchemy.sql import text
from config import thinkbox_config
import logging

bot_id = thinkbox_config['migration_script']['bot_id']
username = thinkbox_config['migration_script']['database_user']
password = thinkbox_config['migration_script']['database_password']

thinkbox_user =  thinkbox_config['database']['thinkbox']['user']
thinkbox_password =  thinkbox_config['database']['thinkbox']['password']

db_uri = f'mysql+pymysql://{username}:{password}@localhost/parserv2?charset=utf8mb4'
db_uri2 = f'mysql+pymysql://{thinkbox_user}:{thinkbox_password}@localhost/thinkbox?charset=utf8mb4'

try:
	engine = sqlalchemy.create_engine(db_uri)
except:
	logging.error("Failed to connect with sqlalchemy")

try:
	engine2 = sqlalchemy.create_engine(db_uri2)
except:
	logging.error("Failed to connect with sqlalchemy")

query = f'select intent.default_payload as `name`, intent.id , ( select GROUP_CONCAT(synonym) from ' \
        f'entity_values_synonyms where value_id in ( select id from entity_values where ' \
        f'entity_id=ir.entity_id)) synonyms from intent intent,intent_entity_relation ir where ' \
        f'intent.bot_id={bot_id} and intent.id=ir.intent_id and ir.`level` = 1 and ir.entity_id in ( ' \
        f'select id from entity where bot_id={bot_id} and `source`=2)  and intent.is_follow_up=0 and ' \
        f'intent.multi_entity_keyword=0 and intent.qa_intent=0 and intent.id not in (select intent_id ' \
        f'from bot_services where bot_id={bot_id}) ORDER BY  intent.intent_order,  intent.id; '
statement = text(query)
try:
	conn = engine.connect()
except:
	logging.error("Failed to connect with sqlalchemy")
result = conn.execute(statement)
conn.close()

values = []

for intent in result:
  for keyword in intent[2].split(','):
    intent_id = intent[1]
    rule_type = 'keyword'
    keyword_ = keyword.replace("'", "''")
    entry = f"({intent_id}, now(), now(), '{rule_type}', '{keyword_}')"
    values.append(entry)

query2 = f'''
    INSERT INTO rules (intent_id, created_at, updated_at, rule_type, keyword)
    VALUES {','.join(values)}
'''.replace('\n', ' ')
statement = text(query2)
conn = engine2.connect()
result = conn.execute(statement)
conn.close()

#print(query2)
