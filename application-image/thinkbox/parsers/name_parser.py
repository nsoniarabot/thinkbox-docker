import logging

class NameParser():
    name_parser = None
    jString = None

    def __init__(self):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        self.jString = pyjnius_init.jString
        NameParser = pyjnius_init.NameParser
        self.name_parser = NameParser()

    def update_file(self, file_name, data):
        self.name_parser.updateFile(self.jString(file_name), self.jString(data))
        return

    def get_name_tokens(self, text):
        jText = self.jString(text)
        #convert to python object
        matches = self.name_parser.getNameTokens(jText)
        
        return self.convert_to_python_objects(matches)

    def convert_to_python_objects(self, matches):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)

        p_matches = []
        if matches is not None:
            for a in range(matches.size()):
                p_matches.append({
                    'tokenString': str(matches.get(a).getTokenString()),
                    'startOffset': str(matches.get(a).getStartOffset()),
                    'endOffset': str(matches.get(a).getEndOffset()),
                    'name': JNameDTO(matches.get(a).getName()).__dict__,
                })
        return p_matches

class JNameDTO:
    def __init__(self, name_parser_match):
        self.name = str(name_parser_match.getName())
        self.type = str(name_parser_match.getType())
        self.nameEn = str(name_parser_match.getNameEn())
        self.isAmbiguous = name_parser_match.getIsAmbiguous()

    def __str__():
        return f'''
            self.name : {self.name},
            self.type : {self.type},
            self.nameEn : {self.nameEn},
            self.isAmbiguous : {self.isAmbiguous},
        '''