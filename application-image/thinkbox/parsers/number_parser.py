import logging


class NumberParser():

    def parse_numbers(self, input, limit):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        NumberParser = pyjnius_init.NumberParserInterface
        jInput = pyjnius_init.jString(input)
        extraction_result = NumberParser.parseNumbers(jInput, limit)
        parserMatchedNumbers = extraction_result.getNumbers()
        return self.convert_to_python_objects(parserMatchedNumbers)

    def convert_to_python_objects(self, matches):
        try:
            import pyjnius_init
        except Exception as e:
            logging.error(e)
        jSet = pyjnius_init.jSet
        variables = {}
        j_keys = jSet()
        j_keys.addAll(matches.getVariables().keySet())
        for variable in j_keys:
            variables[variable] = matches.get(variable)
        p_matches = []
        if matches is not None:
            for a in range(matches.size()):
                p_matches.append({
                    'matchedValue': str(matches.get(a).getMatchedValue()),
                    'replacedValue': str(matches.get(a).getReplacedValue()),
                    'locationFrom': str(matches.get(a).getLocationFrom()),
                    'locationTo': str(matches.get(a).getLocationTo()),
                    'variables': variables,
                })
        return p_matches

