'''
This file is to run the initialization of pyjnius
** For any usage of pyjnius such as importing classes from arabot core,
** you should import it only here, and import this file in a method level
** For more information and example, please see arabic_analyzer.py file
'''


import jnius_config
from config import thinkbox_config


'''
Here you must load the jar file to JVM firstly
'''
_JAVA_OPTS = thinkbox_config['pyjnuis_config']['JAVA_OPTS']
_CLASS_PATH = thinkbox_config['pyjnuis_config']['CLASS_PATH']
jnius_config.add_options(*_JAVA_OPTS)
jnius_config.set_classpath('.', _CLASS_PATH)

from jnius import autoclass

'''
Importing classes must be after importing the autoclass file
i.e: import io.arabot.interfaces.ParserExtraction
'''
ParserExtraction = autoclass('io.arabot.interfaces.ParserExtraction')
jString = autoclass('java.lang.String')
jInteger = autoclass('java.lang.Integer')
jHashMap = autoclass('java.util.HashMap')
jSet = autoclass('java.util.ArrayList')
ArabotProfile = autoclass('io.arabot.analyzer.ArabotProfile')
ParserMatch = autoclass('io.arabot.beans.ParserMatch')
ArabotNames = autoclass('io.arabot.parsers.nameparser.ArabotNames')
NameParser = autoclass('io.arabot.parsers.nameparser.NameParser')
NameTokenDTO = autoclass('io.arabot.beans.NameTokenDTO')

ParserExtractionResult = autoclass('io.arabot.parsercore.ParserExtractionResult')
NumberParserInterface = autoclass('io.arabot.parsers.NumberParserInterface')

ArabotArAdvanced = autoclass('io.arabot.analyzer.impl.ArabotArAdvanced')
ArabotFr = autoclass('io.arabot.analyzer.impl.ArabotFr')
ArabotArRegular = autoclass('io.arabot.analyzer.impl.ArabotArRegular')
ArabotMixedRegularSP = autoclass('io.arabot.analyzer.impl.ArabotMixedRegularSP')
ArabotMixedAdvancedSP = autoclass('io.arabot.analyzer.impl.ArabotMixedAdvancedSP')
