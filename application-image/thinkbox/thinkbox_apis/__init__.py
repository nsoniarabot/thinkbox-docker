from flask_restplus import Api
from flask import Blueprint
from thinkbox_apis.controllers.protected_words_controller import api as pwc_ns
from thinkbox_apis.controllers.training_samples_controller import api as ts_ns
from thinkbox_apis.controllers.classifier_models_controller import api as cm_ns
from thinkbox_apis.controllers.dictionary_words_controller import api as dw_ns
from thinkbox_apis.controllers.nlu_controller import api as nlu_ns
from thinkbox_apis.controllers.names_controller import api as nm_ns
from thinkbox_apis.controllers.geo_location_controller import api as gl_ns
from thinkbox_apis.controllers.label_controller import api as lbl_ns
from thinkbox_apis.controllers.manual_classification_controller import api as mc_ns
from thinkbox_apis.controllers.custom_controller import api as custom_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Thinkbox REST APIs',
          version='1.0',
          description='Thinkbox APIs for platform and SONOF'
          )

api.add_namespace(custom_ns, path='/v1/customnlu')
api.add_namespace(pwc_ns, path='/v1/protectedword')
api.add_namespace(ts_ns, path='/v1/trainingsample')
api.add_namespace(cm_ns, path='/v1/classifiermodel')
api.add_namespace(dw_ns, path='/v1/dictionaryword')
api.add_namespace(nlu_ns, path='/v1/nlu')
api.add_namespace(nm_ns, path='/v1/names')
api.add_namespace(gl_ns, path='/v1/geolocation')
api.add_namespace(lbl_ns, path='/v1/label')
api.add_namespace(mc_ns, path='/v1/manualclassification')
