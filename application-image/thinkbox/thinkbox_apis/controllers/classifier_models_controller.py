from flask_restplus import Resource
from thinkbox_apis.util.dto import ClasifierModelsDto
from thinkbox_apis.models.classifier_models import ClassifierModel

api = ClasifierModelsDto.api
_classifierModelsDto =  ClasifierModelsDto.classifiermodels


@api.route('/listing/<int:profile_id>')
class ClassifierModelList(Resource):
    def get(self,profile_id):
        """Listing Classifier Models """
        cm_object = ClassifierModel()
        classifier_models = cm_object.list_classifier_models(profile_id)
        if classifier_models:
            response_object = {
                'status': 'success',
                'result': classifier_models
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'no cassification models for this profile_id',
            }
            return response_object, 409


@api.route('/changeActivation/<int:id>/<int:profile_id>')
class ClassifierModelActivation(Resource):
    def put(self,id, profile_id):
        """Changing Active on Live Attribute"""
        cm_object = ClassifierModel()
        classifier_model = cm_object.update_activation(id, profile_id)
        if classifier_model==True:
            response_object = {
                'status': 'success',
                'result': 'the active_on_live attribute is successfully updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Id or Profile_id Not Exists',
            }
            return response_object, 409

@api.route('/changeActivationTest/<int:id>/<int:profile_id>')
class ClassifierModelActivationTest(Resource):
    def put(self,id, profile_id):
        """Changing Active on Test Attribute"""
        cm_object = ClassifierModel()
        classifier_model = cm_object.update_activation_test(id, profile_id)
        if classifier_model==True:
            response_object = {
                'status': 'success',
                'result': 'the active_on_test attribute is successfully updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Id or Profile_id Not Exists',
            }
            return response_object, 409

