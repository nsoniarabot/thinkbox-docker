from flask import request
from flask_restplus import Resource, reqparse

from thinkbox_apis.util.dto import CustomClassifiersDto
from thinkbox_apis.models.manual_classification import ManualClassification
from thinkbox_apis.services.nlu_components import Thinkbox
from thinkbox_apis.services.ncb_entities import NcbEntity

from config import thinkbox_config
import logging
from classifier.classifier_manager import ClassifierManagerMap

from string import punctuation
import requests
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import joblib

api = CustomClassifiersDto.api
_service_classifier = CustomClassifiersDto.customnlu

pattern = re.compile(r'(\d*|\s*)')
punctuation += "؟،'"
trans_table = str.maketrans('', '', punctuation)
def cleanup_text(text):
    text = text.translate(trans_table)
    text = pattern.sub(r"", text)
    return text


def synynoms_unification(text):
    text = text.replace('أ', 'ا')
    text = text.replace('إ', 'ا')
    text = text.replace('اونلاين', '')

    credit_card_syns = [
        'بطاقة ائتمانية',
        'بطاقة ائتمانية',
        'credit card',
        'master card',
        'كريدت كارد',
        'visa card',
        'visa',
        'مستر كارد',
        'ماستر كارد',
        'فيزا',
        'فيزا كارد',
        'بطاقه مسبقة الدفع',
    ]
    for i in credit_card_syns:
        text = text.replace(i, 'بطاقة_ايتمانية')

    renew_syns = [
        'تحديث',
        'اصدار',
        'تجديد',
        'استخراج',
        'اطلع',
        'اجدد',
        'احدث',
        'اصدر',
        'استخرج',
    ]

    for j in renew_syns:
        text = text.replace(j, 'تجديد')

    text = text.replace('البطاقة', 'بطاقة')
    text = text.replace('بطاقة', 'بطاقه')

    return text

def ncb_twitter_preprocessing(text):
    text = text.lower()
    text = synynoms_unification(text)
    text = re.sub(r'#(\w+)', ' ', text)
    text = re.sub(r'@(\w+)', ' ', text)
    text = re.sub('[^A-Za-zأ-ي]+', ' ', text)
    stop_words = set(stopwords.words('arabic'))
    word_tokens = word_tokenize(text)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    return ' '.join(filtered_sentence)

pkl_files = thinkbox_config['pkl_files']
ncb_twitter_path = pkl_files['ncb_twitter_path']
ncb_tweet_clf = joblib.load(ncb_twitter_path)

@api.route('/ncb_classify/')
class ClassifierPredict(Resource):
    ncb_main_categories = {
        'Financing&Leasing': 5,
        'Alahli Cards': 4,
        'Alahli Channels': 6,
        'Payment Operations': 7,
        'Corporate Services': None,
        'others': None
    }
    search_url = thinkbox_config['search_api']['ncb_rules_url']

    def get_request(self, collection_name, text, ncb_type):
        params = {
            'q' : f'{text}'
        }
        if ncb_type != 'others':
            params['fq'] = f'type:"{ncb_type}"'

        request_url = f'http://{self.search_url}/api/{collection_name}/select'
        logging.info('request  url =  ' + str(request_url))
        try:
            response = requests.get(request_url, params=params)
            if response is not None and response.status_code == 200:
                return response.json()
            else:
                logging.error("something went wrong, solr status code = " + str(response.status_code))
        except requests.exceptions.ConnectionError as e:
            logging.error(e)
        return None

    def parse_rule_based_response(self, response):
        logging.info(response)
        if response is None:
            return None
        if response['response']['numFound'] > 0 and len(response['response']['docs']) > 0:
            return response['response']['docs'][0]['combined'].replace('\t', ';')
        else:
            return None

    def insert_to_manual_classification(user_text):
        mc_object = ManualClassification()
        data = {
            'text' : user_text,
            'label_id' : 62, #id for others for type classifier in labels table
            'status' : 'pending',
            'source' : 'default'
        }
        mc_object.add_manual_classification_text(data)

    def get_response_obj(self, result):
        service = result[3]
        if result[3] == 'nan':
            service = ''
        return {
            "type": result[0],
            "area": result[1],
            "subarea": result[2],
            "service": service
        }

    @api.response(200, 'classification result')
    @api.expect(_service_classifier, validate=True)
    def post(self):
        data = request.json
        user_text = ''
        if data['title'] is not None:
            user_text += str(data['title'])
        if data['description'] is not None:
            user_text += str(data['description'])
        cleaned_text = cleanup_text(user_text)
        if len(cleaned_text) == 0:
            ClassifierPredict.insert_to_manual_classification(user_text)
            return {
                    "type" : "unclassified",
                    "area" : "unclassified",
                    "subarea" : "unclassified",
                    "service" : "unclassified"
                }

        classifier_manager = ClassifierManagerMap.get_classifier_manager(2)  # hard coded ncb profile id (2)
        if (classifier_manager is None):
            ClassifierPredict.insert_to_manual_classification(user_text)
            return {
                    "type" : "unclassified",
                    "area" : "unclassified",
                    "subarea" : "unclassified",
                    "service" : "unclassified"
                }
        
        parent_classification = classifier_manager.classify_get_name(user_text)

        #heirarichal classification
        logging.info('type classifier result = ' + str(parent_classification))
        sub_classifier = ClassifierPredict.ncb_main_categories[parent_classification]

        if sub_classifier is not None:
            sub_classifier_manager = ClassifierManagerMap.get_classifier_manager(sub_classifier)
            classification = sub_classifier_manager.classify_get_name(user_text)
            logging.info('sub classifier result = ' + str(classification))
        else:
            classification = parent_classification

        result = classification.split(";")
        response_obj = {
            "type": "unclassified",
            "area": "unclassified",
            "subarea": "unclassified",
            "service": "unclassified"
        }

        if result[0] == 'others' or parent_classification == 'others' or parent_classification == 'Corporate Services':
            logging.info('inside reslt' + str(parent_classification))
            # TODO: add other solr collection for the other types
            result = self.parse_rule_based_response(self.get_request('ncbrules', user_text, parent_classification))
            if result is not None:
                result = result.split(';')
                logging.info('search api result = ' + str(result))
                response_obj = self.get_response_obj(result)
            else:
                ClassifierPredict.insert_to_manual_classification(user_text)
        else:
            response_obj = self.get_response_obj(result)
        
        # get entities
        if response_obj is not None:
            logging.info('extracting entities')
            ncb_entity = NcbEntity()
            entities = ncb_entity.extract_ncb_entities(response_obj["type"], response_obj["area"], response_obj["subarea"], response_obj["service"], user_text)
            if entities is not None:
                logging.info('entities  = ' + str(entities))
                response_obj['entities'] = entities
        return response_obj

@api.route('/ncb_twitter/')
class ClassifyNewCards(Resource):
    @api.response(200, 'Classified')
    @api.param('text', 'Tweet to classify')
    def get(self):
        http_request = request.args.to_dict()
        text = http_request['text']
        if len(text) < 10:
            response_object = {
                'status': 'fail',
                'message': 'Invalid input'
            }
            return response_object, 200
        try:
            processed_text = ncb_twitter_preprocessing(text)
            label = ncb_tweet_clf.predict([processed_text])[0]
            if processed_text:
                response_object = {
                    'status': 'success',
                    'class': label
                }
                return response_object, 200
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'Invalid input'
                }
                return response_object, 200
        except Exception as e:
            logging.error(e)
            response_object = {
                'status': 'fail',
                'message': 'Internal error'
            }
            return response_object, 500