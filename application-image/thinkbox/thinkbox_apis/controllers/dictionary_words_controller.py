from flask import request
from flask_restplus import Resource
from utils import detect_language
from thinkbox_apis.services.nlu_components import Thinkbox

from thinkbox_apis.util.dto import DictionaryWordsDto
from thinkbox_apis.models.dictionary_words import DictionaryWord

api = DictionaryWordsDto.api
_dictionarywords = DictionaryWordsDto.dictionarywords
_delete_list = DictionaryWordsDto.delete_list


@api.route('/')
class Words(Resource):

    @api.doc('add a new word')
    @api.expect(_dictionarywords, validate=True)
    def post(self):
        """Add a new Word """
        dw_object = DictionaryWord()
        data = request.json
        thinkbox = Thinkbox(data['profile_id'])
        lang = detect_language(data['word'])
        check_word_flag = thinkbox.check_word_hunspel(data['word'], lang)
        result = dw_object.add_word(data, check_word_flag)
        if not check_word_flag:
            thinkbox.temp_spellcheck_add(data['word'], lang)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Word Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Word already exists.',
            }
            return response_object, 409

@api.route('/checkword/')
@api.param('profile_id', 'The profile id')
@api.param('word', 'The word to be tested')
class GetWord(Resource):
    @api.doc('word_exists')
    def get(self):
        """Check if Word Exists"""
        http_request = request.args.to_dict()
        thinkbox = Thinkbox(http_request['profile_id'])
        lang = detect_language(http_request['word'])
        check_word_flag = thinkbox.check_word_hunspel(http_request['word'], lang)
        source = 'hunspell'
        if not check_word_flag:
            check_word_flag = thinkbox.check_pwd(http_request['word'])
            source = 'protected word'

        if check_word_flag:
            response_object = {
                'status': 'success',
                'message': '1',
                'source': source
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': '0'
            }
            return response_object, 200

@api.route('/delete_ids')
class DictionaryWordsDeleteList(Resource):
    @api.expect(_delete_list)
    def delete(self):
        """Delete List of dictionary words by IDs """
        dw_object = DictionaryWord()
        data = request.json
        count_of_deleted = dw_object.delete_ids_list(data['ids_list'])
        if count_of_deleted > 0:
            response_object = {
                'status': 'success',
                'message': '{0} IDs Deleted'.format(count_of_deleted)
            }
            return response_object, 200
        elif count_of_deleted == 0:
            response_object = {
                'status': 'success',
                'message': 'No IDs Deleted'
            }
            return response_object, 409
        elif count_of_deleted == "error":
            response_object = {
                'status': 'fail',
                'message': 'Error while deleting',
            }
            return response_object, 500

@api.route('/listing')
class DictionaryWordsListing(Resource):
    """Listing Dictionary Words """

    @api.param('limit', 'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    def get(self):
        data = request.args.to_dict()
        dw_object = DictionaryWord()
        data = dw_object.list_dictionary_word(data)
        if data:
            response_object = {
                'status': 'success',
                'result': data
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Table is empty',
            }
            return response_object, 409