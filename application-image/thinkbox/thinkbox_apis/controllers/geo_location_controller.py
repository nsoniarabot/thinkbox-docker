from thinkbox_apis.util.dto import GeoLocationDto
from thinkbox_apis.models.geo_location import GeoLocation

from flask import request
from flask_restplus import Resource

api = GeoLocationDto.api
_geoDto = GeoLocationDto.geo_location


@api.route('/')
class GeoPost(Resource):
    @api.doc('add a new city')
    @api.expect(_geoDto, validate=True)
    def post(self):
        """Add a New Geo"""
        data = request.json
        geo_object = GeoLocation()
        result = geo_object.add_geo(data, data['profile_id'])
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB Error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Location Has Successfully Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'the location already exists.',
            }
            return response_object, 409


@api.route('/update/<int:id>')
class GeoUpdate(Resource):
    @api.doc('update exist location by id')
    @api.expect(_geoDto, validate=True)
    def put(self, id):
        """Updating exist location """
        data = request.json
        geo_object = GeoLocation()
        result = geo_object.update_geo(data, id)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Location Updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists',
            }
            return response_object, 409


@api.route('/listing')
class GeoListing(Resource):
    """Listing Locations For One Profile_id"""

    @api.param('limit', 'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    @api.param('profile_id', 'Profile_id')
    @api.doc('get locations for a given profile_id')
    def get(self):
        data = request.args.to_dict()
        geo_object = GeoLocation()
        geos = geo_object.list_geos(data)
        if geos:
            response_object = {
                'status': 'success',
                'result': geos
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'profile id not exist',
            }
            return response_object, 409

@api.route('/<int:id>')
class GeoDelete(Resource):
    @api.doc('delete a location')
    def delete(self, id):
        """Delete a location by ID """
        geo_object = GeoLocation()
        result = geo_object.delete_geo_by_id(id)
        if result:
            response_object = {
                'status': 'success',
                'message': 'Location Deleted'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists.',
            }
            return response_object, 409