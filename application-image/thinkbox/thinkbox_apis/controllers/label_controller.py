from flask import request
from flask_restplus import Resource

from thinkbox_apis.util.dto import LabelDto
from thinkbox_apis.models.label import Label

api = LabelDto.api
_label = LabelDto.label

@api.route('/')
class Labels(Resource):

    @api.doc('add a new label')
    @api.expect(_label, validate=True)
    def post(self):
        """Add a new Label """
        lbl_object = Label()
        data = request.json
        result = lbl_object.add_label(data)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Label Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Label already exists.',
            }
            return response_object, 409

@api.route('/<int:id>')
class LabelDelete(Resource):
    def delete(self,id):
        """Delete Label """
        lbl_object = Label()
        result = lbl_object.delete_label_by_id(id)
        if result:
            response_object = {
                'status': 'success',
                'message': 'Label Deleted'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists.',
            }
            return response_object, 409

@api.route('/listing')
class LabelsListing(Resource):
    @api.param('profile_id', 'Labels profile id')
    @api.param('limit', 'Listing limit')
    @api.param('page_number', 'Pagination page id')
    def get(self):
        """Listing Labels """
        lbl_object = Label()
        data = request.args.to_dict()
        result = lbl_object.list_labels(data)
        if result:
            response_object = {
                'status': 'success',
                'result': result
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Table is empty',
            }
            return response_object, 409