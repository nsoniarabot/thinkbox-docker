from flask import request
from flask_restplus import Resource

from thinkbox_apis.util.dto import ManualClassificationDto
from thinkbox_apis.models.manual_classification import ManualClassification

api = ManualClassificationDto.api
_manualclassification = ManualClassificationDto.manualclassification
_manualclassification_status = ManualClassificationDto.manualclassification_status

@api.route('/listing')
class ManualClassificationListing(Resource):
    @api.param('limit',  'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    @api.param('status', 'Status')
    def get(self):
        """Listing Manual Classification """
        data = request.args.to_dict()
        mc_object = ManualClassification()
        result = mc_object.list_manual_classification(data)
        if result:
            response_object = {
                'status': 'success',
                'result': result
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Table is empty',
            }
            return response_object, 409

@api.route('/')
class ManualClassificationPostPut(Resource):

    @api.doc('add new miss-classified Text')
    @api.expect(_manualclassification, validate=True)
    def post(self):
        """ Adding new miss-classified Text """
        data = request.json
        mc_object = ManualClassification()
        result = mc_object.add_manual_classification_text(data)
        if  result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Text Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Text already exists.',
            }
            return response_object, 409

    @api.doc('update miss-classified text')
    @api.expect(_manualclassification_status, validate=True)
    def put(self):
        """Update miss-classified text Status"""
        data = request.json
        mc_object = ManualClassification()
        result = mc_object.update_text_status(data)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Status Updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists',
            }
            return response_object, 409