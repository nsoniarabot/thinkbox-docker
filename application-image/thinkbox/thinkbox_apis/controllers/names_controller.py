from thinkbox_apis.models.names import Name
from thinkbox_apis.util.dto import NamesDto

from flask import request
from flask_restplus import Resource

api = NamesDto.api
_namesDto = NamesDto.names


@api.route('/')
class NamePost(Resource):
    @api.doc('add a new name')
    @api.expect(_namesDto, validate=True)
    def post(self):
        """Add a New Name"""
        data = request.json
        name_object = Name()
        result = name_object.add_name(data, data['profile_id'])
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB Error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Name Has Successfully Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'the name already exists.',
            }
            return response_object, 409

@api.route('/listing')
class NamesListing(Resource):
    """Listing Names For a Specific Limit"""

    @api.param('limit', 'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    @api.param('profile_id', 'Profile id')
    def get(self):
        data = request.args.to_dict()
        name_object = Name()
        names = name_object.list_names(data)
        if names:
            response_object = {
                'status': 'success',
                'result': names
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'profile id not exist',
            }
            return response_object, 409


@api.route('/<int:id>')
class NameDelete(Resource):
    @api.doc('delete a name')
    def delete(self, id):
        """Delete a Name by ID """
        name_object = Name()
        result = name_object.delete_name_by_id(id)
        if result:
            response_object = {
                'status': 'success',
                'message': 'Name Deleted'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists.',
            }
            return response_object, 409
