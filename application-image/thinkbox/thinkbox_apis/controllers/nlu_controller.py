from flask import request
from flask_restplus import Resource, reqparse

from thinkbox_apis.services.ncb_entities import NcbEntity

from classifier.classifier_manager import ClassifierManagerMap
from thinkbox_apis.util.dto import NLUDto
from thinkbox_apis.models.manual_classification import ManualClassification
from components.parsers import PParserMatch
from thinkbox_apis.services.nlu_components import Thinkbox
from config import thinkbox_config
from jobs import JobUtils
import logging

api = NLUDto.api

@api.route('/spellcheck/')
class Spellcheck(Resource):
    @api.response(200, 'Corrected output')
    @api.param('user_text', 'The user input')
    @api.param('profile_id', 'The profile id')
    def get(self):
        http_request = request.args.to_dict()
        thinkbox = Thinkbox(http_request['profile_id'])
        response = thinkbox.spellcheck(http_request)
        return response

@api.route('/understand_text/')
class UnderstandText(Resource):
    @api.response(200, 'Understand text output')
    @api.param('user_text', 'The user input')
    @api.param('profile_id', 'The profile id (optional)', )
    @api.param('parsers_list', 'list of parsers e.g. "GEO, DATE", the default will retrieve all parsers')
    def get(self):
        http_request = request.args.to_dict()
        if 'profile_id' not in http_request:
            http_request['profile_id'] = 1

        if 'parsers_list' not in http_request:
            http_request['parsers_list'] = 'all'

        thinkbox = Thinkbox(http_request['profile_id'], search_api_url=thinkbox_config['search_api']['url'])
        response = thinkbox.understand_text(http_request)
        return response.__dict__

@api.route('/parsers_names/')
class Parsers(Resource):
    def get(self):
        names = list(PParserMatch.get_parser_map_string().keys())
        return names


@api.route('/train_model/')
class TrainModel(Resource):
    @api.response(200, 'Training started')
    @api.param('profile_id', 'The profile id')
    def get(self):
        http_request = request.args.to_dict()
        try:
            result =  JobUtils.start_train_model_job(http_request['profile_id'], thinkbox_config['classifier']['model_path'])
        except Exception as e:
            logging.error(e)
            response_object = {
                'status': 'fail',
                'message': 'Internal server error'
            }
            return response_object, 500
        if result == 'success':
            response_object = {
                'status': 'success',
                'message': 'Training for profile ID {0} , started successfully'.format(http_request['profile_id'])
            }
            return response_object, 200
        elif result == "already training":
            response_object = {
                'status': 'exists',
                'message': 'Training samples for profile ID {0} , is already in training status'.format(http_request['profile_id'])
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Training failed'
            }
            return response_object, 409

@api.route('/classify/')
class Classify(Resource):
    @api.response(200, 'Classified')
    @api.param('profile_id', 'The profile id')
    @api.param('text', 'Text to classify')
    def get(self):
        http_request = request.args.to_dict()
        classifier_manager = ClassifierManagerMap.get_classifier_manager(int(http_request['profile_id']))
        if (classifier_manager is not None):
            return classifier_manager.classify_get_name(http_request['text'])
        else:
            return None


@api.route('/extract_entity/')
class ExtractEntity(Resource):
    @api.response(200, 'Entities Extracted')
    @api.doc('extract ncb entities')
    @api.param('type', 'ticket type')
    @api.param('area', 'ticket area')
    @api.param('subarea', 'ticket subarea')
    @api.param('service', 'ticket service')
    @api.param('text', 'text to be processed')
    def post(self):
        http_request = request.args.to_dict()
        try:
            if request.args.get("service") is None:
                http_request['service'] ='none'
            ncb_entity_obj = NcbEntity()
            result =  ncb_entity_obj.extract_ncb_entities(http_request['type'], http_request['area'], http_request['subarea'], http_request['service'], http_request['text'])
        except Exception as e:
            logging.error(e)
            response_object = {
                'status': 'fail',
                'message': 'Internal error'
            }
            return response_object, 500
        if result:
            response_object = {
                'status': 'success',
                'entities': result
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'success',
                'message': 'call types are unfound'
            }
            return response_object, 200

