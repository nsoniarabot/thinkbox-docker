from flask import request
from flask_restplus import Resource

from thinkbox_apis.util.dto import ProtectedWordsDto
from thinkbox_apis.models.protected_word import ProtectedWord

api = ProtectedWordsDto.api
_protectedwordsDto = ProtectedWordsDto.protectedword

@api.route('/')
class ProtectedWordsPostPut(Resource):

    @api.doc('add a new protected word')
    @api.expect(_protectedwordsDto, validate=True)

    def post(self):
        """Add a new protected Word """

        data = request.json
        pw_object = ProtectedWord()
        result = pw_object.add_p_word(data)
        if  result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Word Added'
            }
            return response_object, 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'Word already exists.',
            }
            return response_object, 409

    @api.doc('update existing protected word by id')
    @api.expect(_protectedwordsDto, validate=True)
    def put(self):
        """Updating existed protected Word """
        data = request.json
        pw_object = ProtectedWord()
        result = pw_object.update_p_word(data)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Word Updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists',
            }
            return response_object, 409

@api.route('/<int:id>')
class ProtectedWordsGetDelete(Resource):
    def delete(self,id):
        """Delete protected Word """
        pw_object = ProtectedWord()
        result = pw_object.delete_p_word_by_id(id)
        if result:
            response_object = {
                'status': 'success',
                'message': 'Word Deleted'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists.',
            }
            return response_object, 409

    def get(self, id):
        """Get protected Word by ID"""
        pw_object = ProtectedWord()
        data = pw_object.get_p_word(id)
        if data:
            response_object = {
                'status': 'success',
                'result': data
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists',
            }
            return response_object, 409

@api.route('/listing')
class ProtectedWordsListing(Resource):
    """Listing protected Word """

    @api.param('limit', 'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    def get(self):
        data = request.args.to_dict()
        pw_object = ProtectedWord()
        data = pw_object.list_p_word(data)
        if data:
            response_object = {
                'status': 'success',
                'result': data
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Table is empty',
            }
            return response_object, 409