import logging

from flask import request
from flask_restplus import Resource
import pandas as pd

from jobs import JobUtils
from thinkbox_apis.util import parsers
from thinkbox_apis.util.dto import TrainingSamplesDto
from thinkbox_apis.models.training_sample import TrainingSample


api = TrainingSamplesDto.api
_trainingSamplesDto = TrainingSamplesDto.trainingsample
_trainingSamplesBulkDeleteDto = TrainingSamplesDto.bulksamples
_trainingSamplesBulkDeleteDto = TrainingSamplesDto.trainingsample
_trainingsamplePUT = TrainingSamplesDto.trainingsample_edit

@api.route('/<int:profile_id>')
class TrainingSamplePost(Resource):
    @api.doc('add a new training sample')
    @api.expect(_trainingSamplesDto, validate=True)
    def post(self,profile_id):
        """Add a New Training Sample """
        data = request.json
        ts_object = TrainingSample()
        result = ts_object.add_training_sample(data,profile_id)
        if result=='err':
            response_object = {
                'status': 'fail',
                'message': 'DB Error'
            }
            return response_object, 500
        elif result == True:
            response_object = {
                'status': 'success',
                'message': 'Training Sample Has Successfully Added'
            }
            return response_object, 201
        elif result == 'prof_err':
            response_object = {
                'status': 'fail',
                'message': 'make sure of profile_id and label_id'
            }
            return response_object, 409
        else:
            response_object = {
                'status': 'fail',
                'message': 'Training Sample already exists.',
            }
            return response_object, 409

@api.route('/')
class TrainingSamplePut(Resource):
    @api.doc('update existing training sample by id')
    @api.expect(_trainingsamplePUT, validate=True)
    def put(self):
        """Updating existed training Sample """
        data = request.json
        ts_object = TrainingSample()
        result = ts_object.update_training_sample(data)
        if result == 'err':
            response_object = {
                'status': 'fail',
                'message': 'DB error'
            }
            return response_object, 500
        elif result:
            response_object = {
                'status': 'success',
                'message': 'Sample Updated'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists',
            }
            return response_object, 409

@api.route('/insert_bulk/<int:profile_id>')
class TrainingSampleBulkPost(Resource):
    @api.doc('add a bulk of training samples from an xlsx or a tab separated tsv file')
    @api.expect(parsers.file_upload)
    def post(self, profile_id):
        """Add a New bulk of Training Sample From XLSX or TSV File"""
        try:
            args = parsers.file_upload.parse_args()
            file = request.files['file']
            if '.xls' in str(file):
                data = pd.read_excel(file)
            elif '.tsv' in str(file):
                data = pd.read_csv(file, sep='\t')
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'the file should be tab separated tsv, or xlsx file'
                }
                return response_object, 500
            result = JobUtils.insert_data(data, profile_id)
            if result:
                response_object = {
                    'status': 'Success, Finished Importing Data',
                    'data_size': len(data)
                }
                return response_object, 200
            elif result == 'err':
                response_object = {
                    'status': 'fail',
                    'message': 'An internal error occurred',
                }
                return response_object, 500
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'invalid data. Expected columns "text", "category_id" and all labels should already be pre-defined',
                }
                return response_object, 400

        except Exception as e:
            logging.error(e)


@api.route('/<int:id>')
class TrainingSamplesDelete(Resource):
    def delete(self,id):
        """Delete Trainign sample by ID """
        ts_object = TrainingSample()
        result = ts_object.delete_sample_by_id(id)
        if result:
            response_object = {
                'status': 'success',
                'message': 'Sample Deleted'
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'ID not exists.',
            }
            return response_object, 409


@api.route('/delete_bulk')
class TrainingSamplesDeleteBulk(Resource):
    @api.expect(_trainingSamplesBulkDeleteDto)
    def delete(self):
        """Delete Trainign sample by list of IDs """
        ts_object = TrainingSample()
        data = request.json
        count_of_deleted = ts_object.delete_samples_bulk(data['ids_list'])
        if count_of_deleted > 0:
            response_object = {
                'status': 'success',
                'message': '{0} IDs Deleted'.format(count_of_deleted)
            }
            return response_object, 200
        elif count_of_deleted == 0:
            response_object = {
                'status': 'success',
                'message': 'No IDs Deleted'
            }
            return response_object, 409
        elif count_of_deleted == "error":
            response_object = {
                'status': 'fail',
                'message': 'Error while deleting',
            }
            return response_object, 500

@api.route('/listing')
class TrainingSamplesListing(Resource):
    """Listing Training Samples """

    @api.param('profile_id', 'Profile ID samples')
    @api.param('limit', 'Listing items limit')
    @api.param('page_number', 'Pagination page id')
    def get(self):
        data = request.args.to_dict()
        dw_object = ts_object = TrainingSample()
        data = dw_object.list_training_samples(data)
        if data:
            response_object = {
                'status': 'success',
                'result': data
            }
            return response_object, 200
        else:
            response_object = {
                'status': 'fail',
                'message': 'Table is empty',
            }
            return response_object, 409