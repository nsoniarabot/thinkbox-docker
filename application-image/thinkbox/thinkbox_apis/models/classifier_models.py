from create_app import db
from config import thinkbox_config
from thinkbox_apis.models.profile import Profile
from sqlalchemy import desc, distinct

import logging


class ClassifierModel(db.Model):
    """ Classifier Models Model"""
    __tablename__ = 'classifier_models'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    hyperparameters = db.Column(db.Text, nullable=False)
    metric = db.Column(db.String(45), server_default=db.FetchedValue())
    metric_value = db.Column(db.Float(53))
    vectorizer = db.Column(db.String(45), server_default=db.FetchedValue())
    preprocessor = db.Column(db.String(45), server_default=db.FetchedValue())
    model_type = db.Column(db.String(45), server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'))
    confidence_cutoff = db.Column(db.Float(53))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    active_on_test = db.Column(db.Boolean)
    active_on_live = db.Column(db.Boolean)

    profile = db.relationship('Profile', primaryjoin='ClassifierModel.profile_id == Profile.id',
                              backref='classifier_models')

    def list_classifier_models(self, profile_id):
        try:
            data = self.query.filter_by(profile_id=profile_id).all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    row[column.name] = str(getattr(item, column.name))
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def update_activation(self, id, profile_id):
        try:
            data = self.query.filter_by(profile_id=profile_id).all()
            item = self.get_classifier_model_id(id)
            if item in data:
                if len(data) > 1:
                    for d in data:
                        d.active_on_live = False
                    if item is not None:
                        item.active_on_live = True
                        db.session.flush()
                        db.session.commit()
                        return True
                    else:
                        return False
                elif len(data)==1 and item.active_on_live == True:
                    return True
                elif len(data) == 1 and item.active_on_live == False:
                    item.active_on_live = True
                    db.session.flush()
                    db.session.commit()
                    return True
                else:
                    return False
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()

    def get_models(self):
        try:
            model_profile_ids = self.query.with_entities(ClassifierModel.profile_id).filter_by(active_on_live = True).distinct().all()
            if model_profile_ids is not None:
                return model_profile_ids
            else:
                return None
        except Exception as e:
            logging.error(e)

    def get_classifier_model_config(self, profile_id):
        try:
            classifier_config = self.query.filter_by(profile_id = profile_id, active_on_live = True)\
                .order_by(desc(ClassifierModel.id)).first()
            return classifier_config
        except Exception as e:
            logging.error(e)

    def insert_classifier_model_config(self, cm):
        try:
            classifier_model = self.query.filter_by(hyperparameters=cm.hyperparameters, metric=cm.metric, metric_value=cm.metric_value,
                                                    vectorizer=cm.vectorizer, preprocessor = cm.preprocessor,
                                                    model_type=cm.model_type).first()
            if not classifier_model:
                db.session.flush()
                db.session.add(cm)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def get_confidence_cutoff(self):
        try:
            model_ids_with_cutoff = db.session.query(ClassifierModel.profile_id, ClassifierModel.confidence_cutoff).filter(ClassifierModel.active_on_live==True).all()
            if model_ids_with_cutoff is not None:
                cutoff = {}
                for item in model_ids_with_cutoff:
                    cutoff[item.profile_id] = item.confidence_cutoff
                return cutoff
            else:
                return None
        except Exception as e:
            logging.error(e)

    def get_classifier_model_id(self, id):
        try:
            return self.query.filter_by(id=id).first()
        except Exception as e:
            logging.error(e)

    def update_activation_test(self, id, profile_id):
        try:
            data = self.query.filter_by(profile_id=profile_id).all()
            item = self.get_classifier_model_id(id)
            if item in data:
                if len(data) > 1:
                    for d in data:
                        d.active_on_test = False
                    if item is not None:
                        item.active_on_test = True
                        db.session.flush()
                        db.session.commit()
                        return True
                    else:
                        return False
                elif len(data)==1 and item.active_on_test == True:
                    return True
                elif len(data) == 1 and item.active_on_test == False:
                    item.active_on_test = True
                    db.session.flush()
                    db.session.commit()
                    return True
                elif len(data) == 1 and item.active_on_test == None:
                    item.active_on_test = True
                    db.session.flush()
                    db.session.commit()
                    return True
                else:
                    return False
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()