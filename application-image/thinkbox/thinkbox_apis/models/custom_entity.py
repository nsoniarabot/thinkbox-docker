from create_app import db
from config import thinkbox_config
from thinkbox_apis.models.profile import Profile
import logging
from collections import defaultdict

class CustomEntity(db.Model):
    __tablename__ = 'custom_entity'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=False)
    name = db.Column(db.String(255))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    enable_parsing = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())

    def get_entities_names(self, profile_id):
        try:
            names_result = {}
            results = CustomEntity.query.filter_by(profile_id=profile_id).all()
            if results is not None:
                for result in results:
                    names_result[result.id] = result.name
                db.session.flush()
                db.session.close()
                return names_result
            return None
        except Exception as e:
            logging.error(e)
            return None

    def get_parsing_entites_names(self, profile_id):
        try:
            names_result = {}
            results = CustomEntity.query.filter_by(profile_id=profile_id).all()
            if results is not None:
                for result in results:
                    names_result[result.name] = result.enable_parsing
                db.session.flush()
                db.session.close()
                return names_result
            return None
        except Exception as e:
            logging.error(e)
            return None

    def get_entities_structure(self, profile_id):
        """ entities_structure is combined entities names,
            values and synonyms as list of dictionaries

        :param profile_id:
        :return: list
        """
        entities = self.get_entities_names(profile_id)
        entities_dict = {}
        entities_list = []
        entity_values_obj = CustomEntityValues()
        for key, val in entities.items():
            values_with_synonyms = entity_values_obj.get_values_with_synonyms(key)
            entities_dict [val] = values_with_synonyms
        for k, v in entities_dict.items():
            entities_list.append({k: v})
        return entities_list

class CustomEntityValues(db.Model):
    __tablename__ = 'custom_entity_values'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    custom_entity_id = db.Column(db.ForeignKey('thinkbox.custom_entity.id'), nullable=False)
    value = db.Column(db.String(255))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    def get_values_with_synonyms(self, custom_entity_id):
        values_result = []
        results = db.session.query(CustomEntityValues, CustomEntitySynonyms)\
            .outerjoin(CustomEntitySynonyms, CustomEntitySynonyms.custom_entity_value_id == CustomEntityValues.id)\
            .filter( CustomEntityValues.custom_entity_id == custom_entity_id)\
            .all()

        for r in results:
            if r[1]:
                values_result.append([r[0].value, r[1].synonym])
            else:
                values_result.append([r[0].value, None])
        temp = defaultdict(list)
        for key, val in values_result:
            temp[key].append(val)
        join_result = dict((key, list(val)) for key, val in temp.items())
        return join_result

class CustomEntitySynonyms(db.Model):
    __tablename__ = 'custom_entity_synonyms'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    custom_entity_value_id = db.Column(db.ForeignKey('thinkbox.custom_entity_values.id'), nullable=False)
    synonym = db.Column(db.String(255))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
