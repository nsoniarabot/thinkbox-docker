from create_app import db
from thinkbox_apis.models.profile import Profile
from sqlalchemy.dialects.postgresql.base import ENUM
import logging
from config import thinkbox_config
from sqlalchemy import desc, asc

class DictionaryWord(db.Model):
    """ Dictioanry Words Model to store hunspell dictionary """
    __tablename__ = 'dictionary_words'
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    language = db.Column(db.String(100), nullable=False)
    word = db.Column(db.String(200), nullable=False)
    affixation_rule = db.Column(db.String(200), server_default=db.FetchedValue())
    dialect = db.Column(db.String(255), server_default=db.FetchedValue())
    source = db.Column(ENUM('default', 'editor', name='dictionary_words_source'))
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    active_on_test = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    active_on_live = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())

    profile = db.relationship('Profile', primaryjoin='DictionaryWord.profile_id == Profile.id',
                              backref='dictionary_words')

    def add_word(self, data, check_word_flag):
        try:
            if not check_word_flag:
                new_word = DictionaryWord(
                    word=data['word'],
                    language=data['language'],
                    affixation_rule=data['affixation_rule'],
                    dialect=data['dialect'],
                    profile_id=data['profile_id'],
                    source=data['source']
                )
                db.session.add(new_word)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def get_word_id(self, id):
        return self.query.filter_by(id=id).first()

    def delete_word_by_id(self, id):
        if self.get_word_id(id) is not None:
            self.query.filter_by(id=id).delete()
            return True
        return False

    def get_max_test_id(self):
        test_profile_id = thinkbox_config['tests']['test_profile_id']
        return self.query.filter_by(profile_id=test_profile_id).order_by(desc("id")).first().id


    def get_words(self, language):
        dictionary_words = self.query.filter_by(language=language).all()
        return dictionary_words

    def delete_ids_list(self, ids_list):
        try:
            count_of_deleted = self.query.filter(DictionaryWord.id.in_((ids_list))).delete(synchronize_session='fetch')
            db.session.commit()
            return count_of_deleted
        except Exception as e:
            logging.error(e)
            return 'error'

    def list_dictionary_word(self, data):
       id_start = int(data['page_number']) * int(data['limit'])
       data = self.query.order_by(asc(DictionaryWord.id)).offset(id_start).limit(data['limit']).all()

       response = []
       for item in data:
           row = {}
           for column in item.__table__.columns:
               if str(column) in ["dictionary_words.updated_at", "dictionary_words.created_at"]:
                   row[column.name] = str(getattr(item, column.name))
               else:
                   row[column.name] = getattr(item, column.name)
           response.append(row)
       return response