import logging

from sqlalchemy import desc
from sqlalchemy.dialects.postgresql.base import ENUM

from config import thinkbox_config
from sqlalchemy import desc, asc
from thinkbox_apis.models.profile import Profile
from create_app import db



class GeoLocation(db.Model):
    """ GeoLocation Model"""
    __tablename__ = 'geo_locations'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=True)
    arabic_city_name = db.Column(db.Text, nullable=True)
    arabic_country_name = db.Column(db.Text, nullable=True)
    arabic_name = db.Column(db.Text, nullable=False)
    arabic_nationality = db.Column(db.Text, nullable=True)
    arabic_official = db.Column(db.Text, nullable=True)
    capital_name = db.Column(db.Text, nullable=True)
    english_city_name = db.Column(db.Text, nullable=True)
    english_country_name = db.Column(db.Text, nullable=True)
    english_name = db.Column(db.Text, nullable=False)
    english_nationality = db.Column(db.Text, nullable=True)
    english_official = db.Column(db.Text, nullable=True)
    extract_strategy = db.Column(ENUM('UNIQUE', 'AMBIGUOUS', name='EXTRACTION_STRATEGY'))
    geo_type = db.Column(ENUM('CITY', 'COUNTRY', 'REGION', 'CONTINENT', 'AIRSTATION', name='GEO_TYPE'))
    iata = db.Column(db.Text)
    iso2 = db.Column(db.Text, nullable=True)
    iso3 = db.Column(db.Text, nullable=False)
    longlat = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    profile = db.relationship('Profile', primaryjoin='GeoLocation.profile_id == Profile.id',
                              backref='geo_location')

    def list_geos(self, data):
        try:
            id_start = int(data['page_number']) * int(data['limit'])
            data = self.query.filter_by(profile_id=data['profile_id']).order_by(asc(GeoLocation.id)).offset(id_start).limit(data['limit']).all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    if str(column) in ["geo_locations.updated_at", "geo_locations.created_at"]:
                        row[column.name] = str(getattr(item, column.name))
                    else:
                        row[column.name] = getattr(item, column.name)
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)
            db.session.rollback()

    def list_geos_limit(self, limit=10):
        try:
            data = self.query.limit(limit).all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    row[column.name] = str(getattr(item, column.name))
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def get_geo_id(self, id):
        return self.query.filter_by(id=id).first()

    def delete_geo_by_id(self, id):
        if self.get_geo_id(id) is not None:
            self.query.filter_by(id=id).delete()
            db.session.commit()
            return True
        return False

    def add_geo(self, data, profile_id):
        try:
            arabic_name = data['arabic_name']
            english_name = data['english_name']
            if not self.check_geo_in_profile(arabic_name, english_name, profile_id):
                new_geo = GeoLocation(
                    profile_id=data['profile_id'],
                    arabic_city_name=data['arabic_city_name'],
                    arabic_country_name=data['arabic_country_name'],
                    arabic_name=data['arabic_name'],
                    arabic_nationality=data['arabic_nationality'],
                    arabic_official=data['arabic_official'],
                    capital_name=data['capital_name'],
                    english_city_name=data['english_city_name'],
                    english_country_name=data['english_country_name'],
                    english_name=data['english_name'],
                    english_nationality=data['english_nationality'],
                    english_official=data['english_official'],
                    extract_strategy=data['extract_strategy'].upper(),
                    geo_type=data['geo_type'].upper(),
                    iata=data['iata'],
                    iso2=data['iso2'],
                    iso3=data['iso3'],
                    longlat=data['longlat'],
                )
                db.session.add(new_geo)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def check_geo_in_profile(self, arabic_name, english_name, profile_id):
        try:
            data = self.query.filter_by(profile_id=profile_id, arabic_name=arabic_name, english_name=english_name).all()
            if data:
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            return None

    def update_geo(self, data, id):
        item = self.get_geo_id(id)
        try:
            if item is not None:
                item.profile_id = data['profile_id']
                item.arabic_city_name = data['arabic_city_name']
                item.arabic_country_name = data['arabic_country_name']
                item.arabic_name = data['arabic_name']
                item.arabic_nationality = data['arabic_nationality']
                item.arabic_official = data['arabic_official']
                item.capital_name = data['capital_name']
                item.english_city_name = data['english_city_name']
                item.english_country_name = data['english_country_name']
                item.english_name = data['english_name']
                item.english_nationality = data['english_nationality']
                item.english_official = data['english_official']
                item.extract_strategy = data['extract_strategy'].upper()
                item.geo_type = data['geo_type'].upper()
                item.iata = data['iata']
                item.iso2 = data['iso2']
                item.iso3 = data['iso3']
                item.longlat = data['longlat']
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def list_all_geos_nlp_core_format(self):
        try:
            data = self.query.all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    if not column.name == 'profile_id':
                        row[column.name] = str(getattr(item, column.name))
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def get_max_test_id(self):
        test_profile_id = thinkbox_config['tests']['test_profile_id']
        return self.query.filter_by(profile_id=test_profile_id).order_by(desc("id")).first().id
