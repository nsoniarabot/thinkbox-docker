from create_app import db
import logging
from config import thinkbox_config
from sqlalchemy import desc, asc
from thinkbox_apis.models.profile import Profile

class Label(db.Model):
    __tablename__ = 'labels'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    mapping_id = db.Column(db.BigInteger)
    priority = db.Column(db.Integer)
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=False)
    name = db.Column(db.String(255))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    profile = db.relationship('Profile', primaryjoin='Label.profile_id == Profile.id', backref='labels')

    def get_label_mappings(profile_id):
        label_mappings = {}
        results = Label.query.filter_by(profile_id=profile_id).all()
        if results is not None:
            for result in results:
                label_mappings[result.mapping_id] = result.id
            db.session.flush()
            db.session.close()
            return label_mappings

    def filter_by_name(self, data):
        return self.query.filter_by(name=data['name'], profile_id=data['profile_id']).first()

    def get_label_name(profile_id, label_id):
        label_name = ''
        result = Label.query.filter_by(profile_id=profile_id).filter_by(mapping_id=int(label_id)).first()
        if result is not None:
            label_name = result.name
            db.session.flush()
            db.session.close()
            return label_name

    def get_label_id(self, id):
        return self.query.filter_by(id=id).first()

    def add_label(self, data):
        try:
            label = self.filter_by_name(data)
            if not label:
                new_label = Label(
                    name=data['name'],
                    mapping_id=data['mapping_id'],
                    priority=data['priority'],
                    profile_id=data['profile_id']
                )
                db.session.add(new_label)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def delete_label_by_id(self, id):
        try:
            if self.get_label_id(id) is not None:
                self.query.filter_by(id=id).delete()
                db.session.commit()
                return True
            return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()

    def get_max_test_id(self):
        test_profile_id = thinkbox_config['tests']['test_profile_id']
        return self.query.filter_by(profile_id=test_profile_id).order_by(desc("id")).first().id

    def list_labels(self, data):
       id_start = int(data['page_number']) * int(data['limit'])
       data = self.query.filter_by(profile_id=data['profile_id']).order_by(asc(Label.id)).offset(id_start).limit(data['limit']).all()
       response = []
       for item in data:
           row = {}
           for column in item.__table__.columns:
               if str(column) in ["labels.updated_at", "labels.created_at"]:
                   row[column.name] = str(getattr(item, column.name))
               else:
                   row[column.name] = getattr(item, column.name)
           response.append(row)
       return response
