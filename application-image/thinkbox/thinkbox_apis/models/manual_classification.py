from create_app import db
from config import thinkbox_config
from sqlalchemy.dialects.postgresql.base import ENUM
from sqlalchemy import asc
import logging

class ManualClassification(db.Model):
    """ Manual classification model"""
    __tablename__ = 'manual_classification'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    status = db.Column(ENUM('pending', 'correct', 'incorrect', 'reclassified', name='manual_classification_status'))
    label_id = db.Column(db.ForeignKey('thinkbox.labels.id'), nullable=False)
    new_label_id = db.Column(db.ForeignKey('thinkbox.labels.id'))
    text = db.Column(db.Text, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    source = db.Column(ENUM('default', 'manual_classification_tool', name='manual_classification_source'))
    label = db.relationship('Label', primaryjoin='ManualClassification.label_id == Label.id')
    new_label = db.relationship('Label', primaryjoin='ManualClassification.new_label_id == Label.id')

    def list_manual_classification(self, data):
        try:
            id_start = int(data['page_number']) * int(data['limit'])
            status = data['status']
            if status == 'all':
                data = self.query.order_by(asc(ManualClassification.id)).offset(id_start).limit(data['limit']).all()
            else:
                data = self.query.filter_by(status=status).order_by(asc(ManualClassification.id)).offset(id_start).limit(data['limit']).all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    if str(column) in ["manual_classification.updated_at", "manual_classification.created_at"]:
                        row[column.name] = str(getattr(item, column.name))
                    else:
                        row[column.name] = getattr(item, column.name)
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)
            db.session.rollback()


    def add_manual_classification_text(self, data):
        try:
            new_text = ManualClassification(
                text=data['text'],
                label_id=data['label_id'],
                status=data['status'],
                source=data['source']
            )
            db.session.add(new_text)
            db.session.flush()
            db.session.commit()
            return True

        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def get_text_id(self, id):
        return self.query.filter_by(id=id).first()

    def update_text_status(self, data):
        item = self.get_text_id(data['id'])
        try:
            if item is not None:
                if 'new_label_id' in data:
                    item.status = data['status']
                    item.new_label_id = data['new_label_id']
                else:
                    item.status = data['status']
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'