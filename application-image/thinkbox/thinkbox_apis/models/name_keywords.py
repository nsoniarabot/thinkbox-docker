import logging
import utils
from thinkbox_apis.models.profile import Profile
from create_app import db
from config import thinkbox_config
from thinkbox_apis.util.parsers_utils import ParsersUtils


class NameKeydWords(db.Model):
    """ NameKeydWords Model"""
    __tablename__ = 'name_keywords'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=True)
    keyword = db.Column(db.Text, nullable=False)
    identifier = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    profile = db.relationship('Profile', primaryjoin='NameKeydWords.profile_id == Profile.id',
                              backref='name_keywords')

    def list_all_names_keywords(self):
        try:
            data = self.query.with_entities(NameKeydWords.keyword, NameKeydWords.identifier).all()
            parser_util = ParsersUtils()
            response =  parser_util.list_synonyms_key_words(data)
            return response
        except Exception as e:
            logging.error(e)