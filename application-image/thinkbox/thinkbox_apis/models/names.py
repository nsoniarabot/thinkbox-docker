import logging

from sqlalchemy import desc
from sqlalchemy.dialects.postgresql.base import ENUM

from config import thinkbox_config
from sqlalchemy import desc, asc
from thinkbox_apis.models.profile import Profile
from create_app import db


class Name(db.Model):
    """ Names Model"""
    __tablename__ = 'names'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=False)
    name = db.Column(db.Text, nullable=False)
    name_en = db.Column(db.Text, nullable=False)
    type = db.Column(ENUM('MALE', 'FEMALE', 'LAST_NAME', 'NEUTRAL', name='name_type'))
    is_ambiguous = db.Column(db.Boolean)
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    profile = db.relationship('Profile', primaryjoin='Name.profile_id == Profile.id',
                              backref='names')

    def list_names(self, data):
        try:
            id_start = int(data['page_number']) * int(data['limit'])
            data = self.query.filter_by(profile_id=data['profile_id']).order_by(asc(Name.id)).offset(id_start).limit(data['limit']).all()
            response = []
            for item in data:
                row={}
                for column in item.__table__.columns:
                    if str(column) in ["names.updated_at", "names.created_at"]:
                        row[column.name] = str(getattr(item, column.name))
                    else:
                        row[column.name] = getattr(item, column.name)
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def list_names_limit(self, limit=10):
        try:
            data = self.query.limit(limit).all()
            response = []
            for item in data:
                row = {}
                for column in item.__table__.columns:
                    if str(column) in ["names.updated_at", "names.created_at"]:
                        row[column.name] = str(getattr(item, column.name))
                    else:
                        row[column.name] = getattr(item, column.name)
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def list_all_names_nlp_core_format(self,limit):
        try:
            data = self.query.with_entities(Name.name, Name.name_en, Name.type, Name.is_ambiguous).limit(limit).all()
            response = []
            for item in data:
                row = {}
                row['name'] = item[0]
                row['nameEn'] = item[1]
                row['type'] = item[2].lower()
                row['isAmbiguous'] = str(item[3]).lower()
                response.append(row)
            return response
        except Exception as e:
            logging.error(e)

    def add_name(self, data, profile_id):
        try:
            name = data['name']
            name_en = data['name_en']
            if not self.check_name_in_profile(name, name_en, profile_id):
                new_name = Name(
                    profile_id=data['profile_id'],
                    name=data['name'],
                    name_en=data['name_en'],
                    type=data['type'].upper(),
                    is_ambiguous=data['is_ambiguous'],
                )
                db.session.add(new_name)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def check_name_in_profile(self, name, name_en, profile_id):
        try:
            data = self.query.filter_by(profile_id = profile_id, name = name, name_en = name_en).all()
            if data:
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            return None

    def get_name_id(self, id):
        return self.query.filter_by(id=id).first()

    def delete_name_by_id(self, id):
        if self.get_name_id(id) is not None:
            self.query.filter_by(id=id).delete()
            db.session.commit()
            return True
        return False

    def get_max_test_id(self):
        test_profile_id = thinkbox_config['tests']['test_profile_id']
        return self.query.filter_by(profile_id=test_profile_id).order_by(desc("id")).first().id