import logging

from thinkbox_apis.models.profile import Profile
from create_app import db
from config import thinkbox_config
from thinkbox_apis.util.parsers_utils import ParsersUtils


class NameSynonyms(db.Model):
    """ NameSynonyms Model"""
    __tablename__ = 'names_synonyms'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'), nullable=True)
    word = db.Column(db.Text, nullable=False)
    synonym = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    profile = db.relationship('Profile', primaryjoin='NameSynonyms.profile_id == Profile.id',
                              backref='names_synonyms')

    def list_all_names_synonyms(self):
        try:
            data = self.query.all()
            parser_util = ParsersUtils()
            response = parser_util.list_synonyms_key_words(data)
            return response
        except Exception as e:
            logging.error(e)