from create_app import db
from config import thinkbox_config

class Profile(db.Model):
    """ Profiles model"""
    __tablename__ = 'profiles'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    name = db.Column(db.String(255))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
