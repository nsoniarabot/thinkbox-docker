import re

from create_app import db
import logging
from config import thinkbox_config
from sqlalchemy import desc, asc
from thinkbox_apis.models.profile import Profile


class ProtectedWord(db.Model):
    """ Protected Words Model"""
    __tablename__ = "protected_words"
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {"schema": thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    language = db.Column(db.String(100), nullable=False)
    word = db.Column(db.String(200), nullable=False)
    correct_word = db.Column(db.String(200), nullable=False)
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'))
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    active_on_test = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    active_on_live = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    profile = db.relationship('Profile', primaryjoin='ProtectedWord.profile_id == Profile.id',
                              backref='protected_words')

    def filter_by_word(self, data):
        return self.query.filter_by(word=data['word'], profile_id=data['profile_id']).first()

    def get_p_word_id(self, id):
        return self.query.filter_by(id=id).first()

    def add_p_word(self, data):
        try:
            word = self.filter_by_word(data)
            user_word = data['word']
            corrected_word = data['correct_word']
            if not word and not self.check_empty(user_word) and not self.check_empty(corrected_word):
                new_word = ProtectedWord(
                    word=data['word'],
                    language=data['language'],
                    correct_word=data['correct_word'],
                    profile_id=data['profile_id']
                )
                db.session.add(new_word)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def delete_p_word_by_id(self, id):
        if self.get_p_word_id(id) is not None:
            self.query.filter_by(id=id).delete()
            db.session.commit()
            return True
        return False

    def update_p_word(self, data):
        item = self.get_p_word_id(data['id'])
        try:
            if item is not None:
                item.word = data['word']
                item.language = data['language']
                item.active_on_test = data['active_on_test']
                item.correct_word = data['correct_word']
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def list_p_word(self, data):
       id_start = int(data['page_number']) * int(data['limit'])
       data = self.query.order_by(asc(ProtectedWord.id)).offset(id_start).limit(data['limit']).all()
       response = []
       for item in data:
           row = {}
           for column in item.__table__.columns:
               if str(column) in ["protected_words.updated_at", "protected_words.created_at"]:
                   row[column.name] = str(getattr(item, column.name))
               else:
                   row[column.name] = getattr(item, column.name)
           response.append(row)
       return response

    def get_p_word(self, id):
        db_object = self.get_p_word_id(id)
        if db_object is not None:
            response = {}
            for column in db_object.__table__.columns:
                if str(column) in ["protected_words.updated_at", "protected_words.created_at"]:
                    response[column.name] = str(getattr(db_object, column.name))
                else:
                    response[column.name] = getattr(db_object, column.name)
            return response
        else:
            return False

    def get_p_words_dict(self):
        _records = self.query.all()
        p_words_dict = {}
        for item in _records:
            p_words_dict[item.word] = item.correct_word
        return p_words_dict

    def get_max_test_id(self):
        test_profile_id = thinkbox_config['tests']['test_profile_id']
        return self.query.filter_by(profile_id=test_profile_id).order_by(desc("id")).first().id

    def check_empty(self,word):
        pattern = re.compile("^\\s*$")
        matchObj = re.match(pattern, word)
        if matchObj:
            return True
        return False