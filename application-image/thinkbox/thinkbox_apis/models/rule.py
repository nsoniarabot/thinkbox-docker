import enum
from create_app import db
from thinkbox_apis.models.label import Label
import logging
from sqlalchemy.dialects.postgresql.base import ENUM
from config import thinkbox_config

class Rule(db.Model):
    """ Rules Model"""
    __tablename__ = "rules"
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {"schema": thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    label_id = db.Column(db.ForeignKey('thinkbox.labels.id'), nullable=False)
    rule_type = db.Column(ENUM('exact', 'keyword', 'regex', 'parser', name='rules_rule_type'), nullable=False)
    keyword = db.Column(db.String(255), server_default=db.FetchedValue())
    regex = db.Column(db.String(255), server_default=db.FetchedValue())
    parser_name = db.Column(db.String(255))
    active_on_test = db.Column(db.Boolean, server_default=db.FetchedValue())
    active_on_live = db.Column(db.Boolean, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    def get_rules(self, profile_id):
        try:
            data = self.query.join(Label).filter(Label.profile_id == profile_id).all()
            return data
        except Exception as e:
            logging.error(e)
            return None

class RulesRuleType(enum.Enum):
    exact = 'exact'
    keyword = 'keyword'
    regex = 'regex'
    parser = 'parser'