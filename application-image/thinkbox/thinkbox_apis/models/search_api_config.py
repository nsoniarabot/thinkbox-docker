from create_app import db
import logging
from config import thinkbox_config
from thinkbox_apis.models.profile import Profile

class SearchApiConfig(db.Model):
    """ Search Api Config Model"""
    __tablename__ = 'search_api_config'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {"schema": thinkbox_config['database']['thinkbox']['schema']}

    bot_id = db.Column(db.Integer, nullable=False)
    collection_name = db.Column(db.String(255), nullable=False)
    intent_id = db.Column(db.Integer, nullable=False)
    priority = db.Column(db.Integer, nullable=False)
    search_api_url = db.Column(db.String(500), nullable=False)
    org_id = db.Column(db.Integer, nullable=False)
    profile_id = db.Column(db.ForeignKey('thinkbox.profiles.id'))
    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    profile = db.relationship('Profile', primaryjoin='SearchApiConfig.profile_id == Profile.id', backref='search_api_configs')

    def get_search_api_config(self, profile_id):
        try:
            search_api_cofig = self.query.filter_by(profile_id=profile_id).all()
            return search_api_cofig
        except Exception as e:
            logging.error(e)
            return None
