import logging
from sqlalchemy.dialects.postgresql.base import ENUM
from config import thinkbox_config

from sqlalchemy import desc, asc, func

from thinkbox_apis.models.label import Label
from thinkbox_apis.models.profile import Profile
from create_app import db

class TrainingSample(db.Model):
    """ Training Samples Model"""
    __tablename__ = 'training_samples'
    __bind_key__ = thinkbox_config['database']['thinkbox']['schema']
    __table_args__ = {'schema': thinkbox_config['database']['thinkbox']['schema']}

    id = db.Column(db.BigInteger, primary_key=True, server_default=db.FetchedValue())
    label_id = db.Column(db.ForeignKey('thinkbox.labels.id'), nullable=False)
    parent_id = db.Column(db.ForeignKey('thinkbox.training_samples.id'), nullable=True)
    text = db.Column(db.Text, nullable=False)
    active_on_test = db.Column(db.Boolean, server_default=db.FetchedValue())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    source = db.Column(ENUM('default', 'classification_tool', name='training_sample_source'))

    label = db.relationship('Label', primaryjoin='TrainingSample.label_id == Label.id', backref='training_samples')
    parent = db.relationship('TrainingSample', remote_side=[id],
                             primaryjoin='TrainingSample.parent_id == TrainingSample.id', backref='training_samples')

    def add_training_sample(self, data, profile_id):
        try:
            training_sample = data['text']
            if self.check_training_samples_in_profile(training_sample=training_sample, profile_id=profile_id, label_id=data['label_id'])=='prof_err':
                return 'prof_err'
            elif not self.check_training_samples_in_profile(training_sample, profile_id, data['label_id']):
                new_training_sample = TrainingSample(
                    label_id=data['label_id'],
                    parent_id=data['parent_id'],
                    text=data['text'],
                    active_on_test=data['active_on_test'],
                    source=data['source'],
                )
                db.session.add(new_training_sample)
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def insert_training_data(self, records, profile_id, bulk_size=500):
        session = db.session
        label_mappings = Label.get_label_mappings(profile_id)
        try:
            if session is not None:
                for i, record in records.iterrows():
                    training_sample = TrainingSample(
                    label_id=label_mappings[record.category_id],
                    text=str(record.text),
                    source=TrainingSample.source.default,
                    )
                    db.session.add(training_sample)
                    db.session.flush()
                    db.session.commit()

                if i % bulk_size == 0:
                    db.session.flush()
                    db.session.commit()

                db.session.flush()
                db.session.commit()
                db.session.close()
            else:
                return False
            return True
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'

    def get_training_samples(self, profile_id):
        try:
            data = self.query.join(Label).filter(Label.profile_id == profile_id).all()
            response =[]
            if data:
                for item in data:
                    response.append([item.label_id, item.text])
                return response
            else:
                return None
        except Exception as e:
            logging.error(e)

    def list_training_samples(self, data):
       id_start = int(data['page_number']) * int(data['limit'])
       data = self.query.join(Label).filter(Label.profile_id == data['profile_id']).order_by(
           asc(TrainingSample.id)).offset(id_start).limit(data['limit']).all()
       response = []
       for item in data:
           row = {}
           for column in item.__table__.columns:
               if str(column) in ["training_samples.updated_at", "training_samples.created_at"]:
                   row[column.name] = str(getattr(item, column.name))
               else:
                   row[column.name] = getattr(item, column.name)
           response.append(row)
       return response

    def get_training_sample(self, text):
        try:
            return self.query.filter_by(text=text).first()
        except Exception as e:
            logging.error(e)

    def check_training_samples_exists(self, profile_id):
        try:
            count = self.query.join(Label).filter(Label.profile_id == profile_id).count()
            if count:
                return count
            return None
        except Exception as e:
            logging.error(e)
            return None

    def check_training_samples_in_profile(self, training_sample, profile_id, label_id):
        try:
            data = self.query.join(Label).filter(Label.profile_id == profile_id, TrainingSample.text == training_sample, TrainingSample.label_id == label_id).all()
            profile_ids = Profile.query.filter(Profile.id == profile_id).first()
            if profile_ids:
                if data:
                    return True
                else:
                    return False
            else:
                return 'prof_err'
        except Exception as e:
            logging.error(e)
            return None


    def get_max_test_id(self):
        return db.session.query(func.max(TrainingSample.id)).scalar()

    def get_sample_id(self, id):
        return self.query.filter_by(id=id).first()

    def delete_sample_by_id(self, id):
        if self.get_sample_id(id) is not None:
            self.query.filter_by(id=id).delete()
            db.session.commit()
            return True
        return False

    def delete_samples_bulk(self, ids_list):
        try:
            count_of_deleted = self.query.filter(TrainingSample.id.in_((ids_list))).delete(synchronize_session='fetch')
            db.session.commit()
            return count_of_deleted
        except Exception as e:
            logging.error(e)
            return 'error'

    def update_training_sample(self, data):
        item = self.get_sample_id(data['id'])
        try:
            if item is not None:
                item.label_id = data['label_id']
                item.text = data['text']
                item.active_on_test = data['active_on_test']
                db.session.flush()
                db.session.commit()
                return True
            else:
                return False
        except Exception as e:
            logging.error(e)
            db.session.rollback()
            return 'err'