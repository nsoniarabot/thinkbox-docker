
import logging

from parsers.number_parser import NumberParser
from trace import Trace

from components import parsers
from components.parsers import ParserNames
from components.custom_entities import CustomEntites

class NcbEntity:
    parser_name_map = [
        'atm_machine_number',
        'type_of_product',
        'loan_account_number',
        'disputes_type'
    ]


    def extract_ncb_entities(self, type, area, subarea, service, text):
        request_list = [type.lower().strip(), area.lower().strip(), subarea.lower().strip(), service.lower().strip()]
        parser = parsers.Parsers()
        named_entities = {}
        response = {}
        entities = []
        unimplemented_entities = self.get_unimplemented_list()
        unimplemented_entities = [item.strip().lower() for item in unimplemented_entities]
        unimplemented_entities_found = []
        call_type_dict = self.get_dict_list()



        for key in call_type_dict:
            if str(key).lower().strip() == str(request_list).lower().strip():
                entities = call_type_dict[key].split(',')
                entities = [item.strip().lower() for item in entities]
                for unimplemented_entity in unimplemented_entities:
                    if unimplemented_entity in entities:                               
                        entities.remove(unimplemented_entity)
                        unimplemented_entities_found.append(unimplemented_entity)
                parsers_list = self.map_entities_parsers(entities)
                named_entities = parser.process(Trace(text),{"parser_names": parsers_list})

        for ent in entities:
            response[ent] = []

        custom_entities = CustomEntites()
        trace = Trace(text)
        params = {'profile_id' : 2}
        trace = Trace()
        trace.current_text = text
        custom_entities_values = custom_entities.process(trace, params)
        response = self.extract_parser_entities(custom_entities_values, response)

        if named_entities is not None:
            for key_named in named_entities:
                values = named_entities[key_named]
                matched_values = []
                for val in values:
                    matched_values.append(val['replacedValue'])
                response[key_named] = matched_values

            if 'account_number' in entities:
                account_numbers = []
                # TODO use number parser to get the length of number
                for i in response['number']:
                    if len(i) == 14:
                        account_numbers.append(i)
                        response['account_number'] = account_numbers
                response.pop('number')

            if 'date' in response:
                if 'transaction_date' in entities:
                    response['transaction_date'] = response.pop('date')
                elif 'date_of_transfer' in entities:
                    response['date_of_transfer'] = response.pop('date')
                else:
                    response['date_of_visit'] = response.pop('date')

            if 'currency' in response:
                response['transaction_amount'] = response.pop('currency')

            for unimplemented_entity_found in unimplemented_entities_found:
                # TODO : implement the rest of remaining parsers
                response[unimplemented_entity_found] = []
        return response


    def extract_parser_entities(self, entities, parsed_entities):
        for key, value in entities.items():
            if key not in parsed_entities:
                continue
            if key in NcbEntity.parser_name_map:
                parsed_entities[key] = []
                for parsed_entity in value:
                    parsed_entities[key].append(parsed_entity['matchedValue'])
        return parsed_entities

    def get_dict_list(self):
        entities_dict = {
            "['Alahli Cards', 'ATM Cards', 'NCB ATM - Withdrawal', 'ATM Cash Withdrawal - NCB']" :'transaction_date, transaction_amount',
            "['Alahli Cards', 'ATM Cards', 'NCB ATM - Deposit', 'ATM Deposit']" :'account_number, transaction_time, branch, atm_machine_number, Amount',
            "['Alahli Cards', 'ATM Cards', 'International Trans Disputes', '']" :'transaction_date , transaction_amount , account_number ,disputes_type',
            "['Alahli Cards', 'Credit Cards', 'Financial Transactions', 'Reverse Duplicate Payment']" :'transaction_date, transaction_amount',
            "['Alahli Cards', 'Credit Cards', 'Financial Transactions', 'Tayseer Markup/Service Charges']" :'transaction_date, transaction_amount',
            "['Alahli Cards', 'Credit Cards', 'Financial Transactions', 'Paid amounts not ref on card']" :'transaction_date, credit_card_number,transaction_amount',
            "['Alahli Cards', 'Credit Cards', 'Programs', 'Objection on Programs']" :'transaction_date , credit_card_number , transaction_amount',
            "['Alahli Cards', 'Credit Cards', 'Financial Transactions', 'Transact. Disputes']" :'transaction_date , credit_card_number , transaction_amount',
            "['Alahli Cards', 'Credit Cards', 'Financial Transactions', 'objection on the annual fees']" :'transaction_date , credit_card_number , transaction_amount',
            "['Alahli Channels', 'Alahli Online & Mobile', 'Alahli Online Service', 'Report Fraud']" :'account_number, transaction_amount, transaction_date',
            "['Alahli Channels', 'Branches', 'Account Issues', 'Unkown deduction']" :'transaction_date, transaction_amount',
            "['Alahli Channels', 'Branches', 'Branch Services', 'bad behav from Branch employee']" :'branch_name , branch_code, date_of_visit , name_of_the_ncb_employee',
            "['Alahli Channels', 'Branches', 'Account Issues', 'reson for Stopped Account']" :'branch_number, reason, account_number',
            "['Corporate Services', 'POS', 'Purchase Operations', 'Purchasing Process']" :'transaction_date, transaction_amount',
            "['Financing&Leasing', 'Auto Lease', 'After Sales Service', 'Delay in ballon Payment']" :'loan_account_number',
            "['Financing&Leasing', 'Auto Lease', 'Account Services', 'Add/ Cancel Direct Debit']" :'loan_account_number',
            "['Financing&Leasing', 'Collections Services', 'Complaint on Collection', '']" :'type_of_product',
            "['Financing&Leasing', 'Collections Services', 'Complaint after Reposessed Car', '']" :'loan_account_number',
            "['Financing&Leasing', 'Personal Finance', 'Reverse Early Deduction', '']" :'deduction_date, amount',
            "['Payment Operations', 'International', 'Cancellation', 'Reason of Cancellation']" :'transaction_date, transaction_amount',
            "['Payment Operations', 'International', 'Fate of Payment', 'Ben Claim Non Receipt of Funds']" :'transaction_date, transaction_amount',
            "['Payment Operations', 'International', 'Fate of Payment', 'Inquiry of Payment']" :'transaction_date, transaction_amount',
            "['Payment Operations', 'International', 'Cancellation', 'Request For Cancellation']" :'account_number, date_of_transfer, Amount',
            "['Payment Operations', 'Quick Pay', 'Trans Not Received', '']" :'transaction_date, transaction_amount, transaction_number',
            "['Payment Operations', 'Quick Pay', 'Trans Cancelation', '']" :'transaction_date, transaction_amount, transaction_number',
            "['Payment Operations', 'Sarie', 'Fate of Payment', 'Ben Claim Non Receipt of Funds']" :'transaction_date, transaction_amount'
        }
        return entities_dict

    def map_entities_parsers(self, entities_list):
        parsers_list = []
        parsers_dict = {
            'credit_card_number': ParserNames.CREDIT_CARD_PARSER.value,
            'transaction_amount': ParserNames.CURRENCY_PARSER.value,
            'transaction_date': ParserNames.DATE_PARSER.value,
            'branch_name': ParserNames.NAME_PARSER.value,
            'transaction_number': ParserNames.NUMBERS_PARSER.value,
            'loan_account_number': ParserNames.NUMBERS_PARSER.value,
            'branch_number': ParserNames.NUMBERS_PARSER.value,
            'account_number': ParserNames.NUMBERS_PARSER.value,
            'date_of_transfer': ParserNames.DATE_PARSER.value,
            'deduction_date': ParserNames.DATE_PARSER.value,
            'amount': ParserNames.CURRENCY_PARSER.value,
            'name_of_the_ncb_employee': ParserNames.NAME_PARSER.value,
            'date_of_visit': ParserNames.DATE_PARSER.value,
            'transfer number': ParserNames.NUMBERS_PARSER.value
        }
        for entity in entities_list:
            if entity in parsers_dict:
                parsers_list.append(parsers_dict[entity])
        return parsers_list

    def get_unimplemented_list(self):
        unimplemented_list = [
        'transaction_time', 
        'branch', 
        'reason', 
        'branch_code']
        return unimplemented_list


    def get_account_number(self, text):
        try:
            number_parser = NumberParser()
            matches = number_parser.parse_numbers(text, 14)
            return matches
        except Exception as e:
            logging.error(e)