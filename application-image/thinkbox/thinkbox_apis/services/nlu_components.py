from components.rule_matcher import RuleBasedIntentParser
from components.intent_classifier import IntentClassifier
from components.spellcheck_component.spellcheck import Spellcheck
from components.custom_entities import CustomEntites
import components.spellcheck_component.spellcheck_manager as spell_manager
from components.text_preprocessing_pipelines import TextPreprcossing
from classifier.classifier_manager import *
from components.sentiment_analysis import SentimentAnalysis
from components.arabic_analyzer import Analyzer
from components.parsers import *
from components.parsers import Parsers
from components.searchapi import SearchAPI
from config import thinkbox_config
from trace import Trace
import sys
import logging


class Thinkbox:
    """
    Business logic of the thinkbox that does not relate to a specific component
    """
    def __init__(self, profile_id, model_path=None, search_api_url=None):
        self._profile_id = profile_id
        self._pipeline = thinkbox_config['default_pipeline']
        self._mapping = self.class_config_mapping()
        self.init_search_api(search_api_url)

    def reload_spellchecker(self):
        spell_manager.reload_dictionaries(thinkbox_config['dictionaries']['dict_path'], thinkbox_config['dictionaries']['affixation_file_path'])

    def init_search_api(self, search_api_url):
        SearchAPI.solr_base_url = search_api_url

    def understand_text(self, params):
        """
        This function calls all the components that are necessary for intent parsing. It manages the pipeline
        and returns the output of each component in the thinkbox.

        Args:
            params (dict): The input params from the platform (regarding user_text and state)
        Returns:
            trace (Trace): The output of each component in thinkbox
        """
        trace = Trace(params['user_text'])
        step = 1
        params['profile_id'] = self._profile_id
        text_processing_components = ["spellcheck", "text_processing"]
        try:
            for name in self._pipeline:
                classname = self._mapping[name]['classname']
                uninstantiated_class = getattr(sys.modules[__name__], classname)
                instance = uninstantiated_class()
                component_output = instance.process(trace, params)
                setattr(trace, self._mapping[name]['attr'], component_output)
                if name in text_processing_components:
                    trace.current_text = component_output
                if name == "spellcheck" and component_output is not None:
                    trace.current_text = component_output['corrected_output']
                if trace.label_id is not None or trace.search_api_response is not None:
                    return trace
                step = step + 1
        except Exception as e:
            logging.error(e)
        return trace

    def spellcheck(self, params):
        spellcheck = Spellcheck()
        trace = Trace(params['user_text'])
        return {"spellcheck_result": spellcheck.process(trace, params)}

    def custom_entities(self, params):
        custom_entities = CustomEntites()
        trace = Trace(params['user_text'])
        return {"custom_entities": custom_entities.process(trace, params)}

    def check_word_hunspel(self, user_text, lang):
        spellcheck = Spellcheck()
        return spellcheck.check_spelling(user_text, lang)

    def check_pwd(self, word):
        spellcheck = Spellcheck()
        return spellcheck.check_protected_word(word)

    def temp_spellcheck_add(self, user_text, lang):
        spellcheck = Spellcheck()
        spellcheck.add_to_speelcheck_temp(user_text, lang)

    def class_config_mapping(self):
        """
        This function maps each config parameter to the class it is related to in the the thinkbox, and
        the name that is used in the thinkbox to refer to it.
        """
        # name_in_config: {classname: name_of_class}
        mapping = {
            "analyzer": {'classname': "Analyzer", "attr": "analyzer_result"},
            "text_processing": {'classname': "TextPreprcossing", "attr": "processed_text"},
            "custom_entities": {'classname': "CustomEntites", "attr": "custom_entities"},
            "spellcheck": {'classname': "Spellcheck", "attr": "spellcheck_result"},
            "sentiment": {'classname': "SentimentAnalysis", "attr": "sentiment_result"},
            "rule_matcher": {'classname': "RuleBasedIntentParser", "attr": "label_id"},
            "parsers": {'classname': "Parsers", "attr": "named_entities"},
            "intent_classifier": {'classname': "IntentClassifier", "attr": "label_id"},
            "search_api": {'classname': "SearchAPI", "attr": "search_api_response"}
        }
        return mapping
