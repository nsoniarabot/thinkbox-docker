from flask_restplus import Namespace, fields


class ProtectedWordsDto:
    api = Namespace('protectedword', description='protected words operations')
    protectedword = api.model('protectedword', {
        'id': fields.Integer(description='protected word id'),
        'word': fields.String(required=True, description='wrong word'),
        'correct_word': fields.String(required=True, description='correct word'),
        'language': fields.String(required=True, description='protected word language'),
        'profile_id': fields.Integer(description='protected word profile id'),
        'active_on_test': fields.Boolean(description='Boolean value to choose if the word should be active on testing stage')
    })


class TrainingSamplesDto:
    api = Namespace('trainingsample', description='training samples operations')
    trainingsample = api.model('trainingsample', {
        'label_id': fields.Integer(required=True, description='label id'),
        'parent_id': fields.Integer(description='parent id'),
        'text': fields.String(required=True, description='text'),
        'active_on_test': fields.Boolean(description='active_on_test'),
        'source': fields.String(description='source'),

    })

    bulksamples = api.model('training_sample', {
        'ids_list': fields.List(fields.Integer, required=True, description='IDs list'),
    })
    trainingsample_edit = api.model('trainingsamples', {
        'id': fields.Integer(description='protected word id'),
        'label_id': fields.Integer(required=True, description='label id'),
        'text': fields.String(required=True, description='text'),
        'active_on_test': fields.Boolean(description='active_on_test'),
    })

class ClasifierModelsDto:
    api = Namespace('classifiermodels', description='classifier models operations')
    classifiermodels = api.model('classifiermodels', {
        'hyperparameters': fields.String(required = True, description='hyperparameters of a classifier model'),
        'metric': fields.String(required = True, description='metric'),
        'metric_value': fields.Float(required = True, description='metric_value'),
        'vectorizer': fields.String(required = True, description='vectorizer name'),
        'preprocessor': fields.String(required = True, description='preprocessor'),
        'model_type': fields.String(required = True, description='model_type'),
        'profile_id': fields.Integer(required = True, description='profile id'),
        'confidence_cutoff': fields.Float(description='confidence_cutoff'),
        'active_on_test': fields.Boolean(description='active_on_test'),
        'active_on_live': fields.Boolean(required = True, description='active_on_live')
    })


class DictionaryWordsDto:
    api = Namespace('dictionarywords', description='dictionary words operations')
    dictionarywords = api.model('dictionarywords', {
        'word': fields.String(required=True, description='spellcheck word'),
        'affixation_rule': fields.String(description='affixation_rule'),
        'language': fields.String(required=True, description='language'),
        'dialect': fields.String(description='dialect'),
        'source': fields.String(description='source'),
        'profile_id': fields.Integer(required=True, description='profile_id')
    })
    delete_list = api.model('dictionarywords', {
        'ids_list': fields.List(fields.Integer, required=True, description='IDs list')

    })

class NLUDto:
    api = Namespace('nlu', description='NLU components')

class CustomClassifiersDto:
    api = Namespace('customnlu', description='Custom NLU components')
    customnlu = api.model('customnlu', {
        'profile_id': fields.Integer(required=True, description='profile identifier'),
        'case_number': fields.Integer(required=True, description='SAMA Case Number'),
        'sama_category': fields.String(required=False, description='SAMA Category'),
        'sama_product': fields.String(required=False, description='SAMA Prorduct'),
        'title': fields.String(required=True, description='user input'),
        'description': fields.String(required=True, description='user input')
    })

class NamesDto:
    api = Namespace('name', description='Names operations')
    names = api.model('name', {
        'profile_id': fields.Integer(required=True, description='bot identifier'),
        'name': fields.String(required=True, description='name'),
        'name_en': fields.String(required=True, description='name in English'),
        'type': fields.String(required=True, description='male, female, last_name'),
        'is_ambiguous': fields.Boolean(required=True, description='ambiguouty identifier'),
    })


class GeoLocationDto:
    api = Namespace('geo_location', description='Geo_Location operations')
    geo_location = api.model('geo_location', {
        'profile_id': fields.Integer(required=False, description='bot identifier'),
        'arabic_city_name': fields.String(required=False, description='city name in Arabic'),
        'arabic_country_name': fields.String(required=False, description='country name in Arabic'),
        'arabic_name': fields.String(required=True, description='name in Arabic'),
        'arabic_nationality': fields.String(required=False, description='nationality in Arabic'),
        'arabic_official': fields.String(required=False, description='official name in Arabic'),
        'capital_name': fields.String(required=False, description='capital name in Arabic or English'),
        'english_city_name': fields.String(required=False, description='city name in English'),
        'english_country_name': fields.String(required=False, description='country name in English'),
        'english_name': fields.String(required=True, description='name in English'),
        'english_nationality': fields.String(required=False, description='nationality in English'),
        'english_official': fields.String(required=False, description='official name in English'),
        'extract_strategy': fields.String(description='unique, ambiguous'),
        'geo_type': fields.String(description='city, country, continent'),
        'iata': fields.String(description='iata'),
        'iso2': fields.String(required=False, description='ISO 2'),
        'iso3': fields.String(required=True, description='ISO 3'),
        'longlat': fields.String(required=True, description='longitude & latitude')
    })


class LabelDto:
    api = Namespace('label', description='labels')
    label = api.model('label', {
        'mapping_id': fields.Integer(description='label mapping id'),
        'priority': fields.Integer(description='label priority'),
        'profile_id': fields.Integer(description='label profile id'),
        'name': fields.String(required=True, description='label name')
    })

class ManualClassificationDto:
    api = Namespace('manualclassification', description='manual classification operations')
    manualclassification = api.model('manualclassification', {
        'text': fields.String(required=True, description='ticket text'),
        'label_id': fields.Integer(description='category id'),
        'source': fields.String(required=True, description='source'),
        'status': fields.String(required=True, description='status'),
    })
    manualclassification_status = api.model('manualclassification_status', {
        'id': fields.Integer(description='ticket text id'),
        'new_label_id': fields.Integer(required=False, description='new label id'),
        'status': fields.String(required=True, description='status'),
    })