
class ParsersUtils:
    def list_synonyms_key_words(self, data):
        response = ""
        for item in data:
            response += item[0] + " => " + item[1] + "\n"
        return response

    def list_stop_protected_words(self,data):
        response = ''
        for item in data:
            response += item[0] + '\n'
        return response
