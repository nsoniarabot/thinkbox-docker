from thinkbox_apis.services.nlu_components import Thinkbox
import logging
import json
from manage import app

class UserIntent(object):
    def understand_text(self, text=None, profile_id=None, parsers_list=None):
        '''Taking input parameters from JVM

        :param text: String, defined using JAVA interface
        :param profile_id: Integer, defined using JAVA interface
        :returns: json that contains the result of each NLP component
        :rtype: json
        '''
        think_box_obj = Thinkbox(profile_id)
        trace = think_box_obj.understand_text(
                {"user_text": text,
                 "profile_id": profile_id,
                 "parsers_list": parsers_list}
            )
        trace = trace.__dict__
        logging.debug("py4j tracing: {}".format(trace))
        return json.dumps(trace)
