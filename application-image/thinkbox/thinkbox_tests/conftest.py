from create_app import app
import pytest
from app_init import classifiers_init
from config import thinkbox_config
import components.spellcheck_component.spellcheck_manager as spell_manager
from thinkbox_apis import blueprint

app.register_blueprint(blueprint)
app.app_context().push()


@pytest.fixture()
def path():
    return thinkbox_config['classifier']['model_path']


@pytest.fixture()
def url():
    return thinkbox_config['search_api']['testing_url']


@pytest.fixture(scope="session", autouse=True)
def setup():
    dictionary_path = thinkbox_config['dictionaries']['test_dict_path']
    affixation_file_path = thinkbox_config['dictionaries']['affixation_file_path']
    spell_manager.reload_dictionaries(dictionary_path, affixation_file_path)
    classifiers_init()

@pytest.yield_fixture()
def client():
    """
    Setup an app client, this gets executed for each test function.

    :param app: Pytest fixture
    :return: Flask app client
    """

    with app.test_client() as client:
        yield client

