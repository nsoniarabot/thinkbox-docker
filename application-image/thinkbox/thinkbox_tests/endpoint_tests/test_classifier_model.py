import pytest
from thinkbox_apis.models.classifier_models import ClassifierModel
import json
from config import thinkbox_config

TEST_ID = thinkbox_config['tests']['test_id']
test_profile_id = thinkbox_config['tests']['test_profile_id']


def test_put_method(client):
    response = client.put('/v1/classifiermodel/changeActivation/' + str(TEST_ID) + '/' + str(test_profile_id))
    cm = ClassifierModel()
    models = cm.list_classifier_models(test_profile_id)
    active_ids = []
    for model in models:
        if model["active_on_live"] == 'True':
            active_ids.append(model["id"])
    if str(TEST_ID) in active_ids:
        assert b'"status": "success"' in response.data
        assert b'"result": "the active_on_live attribute is successfully updated"' in response.data
        assert response.status_code == 200
    else:
        assert b'"status": "fail"' in response.data
        assert b'"message": "Id or Profile_id Not Exists"' in response.data
        assert response.status_code == 409


def test_get_method(client):
    response = client.get('/v1/classifiermodel/listing/' + str(test_profile_id))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data
    assert b'"metric": "accuracy"' in response.data