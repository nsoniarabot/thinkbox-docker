import pytest
from thinkbox_apis.models.dictionary_words import DictionaryWord
import json
from config import thinkbox_config

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_post_method(client):
    response =  client.post('/v1/dictionaryword/', data=json.dumps(dict(
        word = "TESTINGWORD",
        affixation_rule = "",
        language = "en",
        profile_id = test_profile_id,
        dialect= "JOR",
        source="editor"
    )), content_type='application/json')
    global TEST_ID
    TEST_ID = DictionaryWord().get_max_test_id()
    assert response.status_code == 201

def test_get_method(client):
    response = client.get('/v1/dictionaryword/checkword/?profile_id={0}&word={1}'.format(test_profile_id, "TESTINGWORD"))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data
    assert b'"message": "1"' in response.data
    DictionaryWord().delete_word_by_id(TEST_ID)