import pytest

from config import thinkbox_config
from thinkbox_apis.models.geo_location import GeoLocation
import json

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']


def test_post_method(client):
    response =  client.post('/v1/geolocation/', data=json.dumps(dict(
        profile_id = test_profile_id,
        arabic_city_name = "Test_City",
        arabic_country_name = "Test_Country",
        arabic_name = "Test_API",
        arabic_nationality = "Test_Nationality",
        arabic_official = "Test_Official",
        capital_name = "Test_Capital",
        english_city_name = "English_City",
        english_country_name = "Country_En",
        english_name = "API_Test",
        english_nationality = "Nationality_En",
        english_official = "English_Official",
        extract_strategy = "UNIQUE",
        geo_type = "CITY",
        iata = 'iata',
        iso2 = "iso2",
        iso3 = "iso3",
        longlat = "ltlong"
    )), content_type='application/json')
    global TEST_ID
    TEST_ID = GeoLocation().get_max_test_id()
    assert response.status_code == 201
    assert b'"status": "success"' in response.data
    assert b'"message": "Location Has Successfully Added"' in response.data


def test_put_method(client):
    response =  client.put('/v1/geolocation/update/' + str(TEST_ID), data=json.dumps(dict(
        profile_id=test_profile_id,
        arabic_city_name="Test_City",
        arabic_country_name="Test_Country",
        arabic_name="Test_Put_API",
        arabic_nationality="Test_Nationality",
        arabic_official="Test_Official",
        capital_name="Test_Capital",
        english_city_name="English_City",
        english_country_name="Country_En",
        english_name="API_Test",
        english_nationality="Nationality_En",
        english_official="English_Official",
        extract_strategy="UNIQUE",
        geo_type="COUNTRY",
        iata='iata',
        iso2="iso2",
        iso3="iso3",
        longlat="ltlong"
    )), content_type='application/json')
    assert response.status_code == 200
    assert b'"message": "Location Updated"' in response.data


def test_get_method(client):
    response = client.get('/v1/geolocation/listing/'+str(test_profile_id))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data


def test_delete_method(client):
    response = client.delete('/v1/geolocation/'+ str(TEST_ID))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data
    assert b'"message": "Location Deleted"' in response.data