from thinkbox_apis.models.label import Label
import json
from config import thinkbox_config

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_post_method(client):
    response =  client.post('/v1/label/', data=json.dumps(dict(
        mapping_id = 111,
        priority = 111,
        profile_id = 1,
        name= "TESTING_LABLE"
    )), content_type='application/json')
    global TEST_ID
    TEST_ID = Label().get_max_test_id()
    assert response.status_code == 201

def test_delete_method(client):
    response = client.delete('/v1/label/{0}'.format(TEST_ID))
    assert response.status_code == 200