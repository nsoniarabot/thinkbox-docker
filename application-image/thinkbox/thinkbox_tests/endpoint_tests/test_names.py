import pytest

from thinkbox_apis.models.names import Name
import json
from config import thinkbox_config

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_post_method(client):
        response =  client.post('/v1/names/', data=json.dumps(dict(
            profile_id = test_profile_id,
            name = "يوسف",
            name_en = "yousof",
            type= "MALE",
            is_ambiguous= False
        )), content_type='application/json')
        global TEST_ID
        TEST_ID = Name().get_max_test_id()
        assert response.status_code == 201


def test_get_method(client):
        response = client.get('/v1/names/listing/'+str(test_profile_id))
        assert response.status_code == 200
        assert b'"status": "success"' in response.data


def test_get_method_limit(client):
    response = client.get('/v1/names/listingLimit/' + str(test_profile_id))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data


def test_delete_method(client):
    response = client.delete('/v1/names/'+ str(TEST_ID))
    assert response.status_code == 200
    assert b'"message": "Name Deleted"' in response.data