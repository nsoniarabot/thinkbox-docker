from config import thinkbox_config

test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_get_parse(client):
    response = client.get('/v1/nlu/parse/?profile_id={0}&user_text=where%20will%20you%20deliver%20my%20shipment'.format(test_profile_id))
    assert response.status_code == 200
    assert b'"original_text": "where will you deliver my shipment"' in response.data
    assert b'"current_text": "where will you deliver my shipment"' in response.data
    assert b'"label_id": 424' in response.data

def test_get_spellcheck(client):
    response = client.get('/v1/nlu/spellcheck/?profile_id={0}&user_text=where%20will%20you2%20deliver%20my%20shipmentt'.format(test_profile_id))
    assert response.status_code == 200
    assert b'"corrected_output": "where will you deliver my shipment"' in response.data

def test_get_train_model(client):
    response = client.get('/v1/nlu/train_model/?profile_id={0}'.format(test_profile_id))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data
    assert b'"message": "Training for profile ID %d , started successfully"' % test_profile_id in response.data
