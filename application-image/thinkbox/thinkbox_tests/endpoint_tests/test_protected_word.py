import pytest
from thinkbox_apis.models.protected_word import ProtectedWord
import json
from config import thinkbox_config

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_post_method(client):
    response =  client.post('/v1/protectedword/', data=json.dumps(dict(
        word = "POST_TEST",
        correct_word = "POST_TEST",
        language = "en",
        profile_id = test_profile_id,
        active_on_test= True
    )), content_type='application/json')
    global TEST_ID
    TEST_ID = ProtectedWord().get_max_test_id()
    assert response.status_code == 201

def test_put_method(client):
    response =  client.put('/v1/protectedword/', data=json.dumps(dict(
        id = TEST_ID,
        word = "PUT_TEST",
        correct_word = "PUT_TEST",
        language = "en",
        profile_id = test_profile_id,
        active_on_test= True
    )), content_type='application/json')
    assert response.status_code == 200

def test_get_method(client):
    response = client.get('/v1/protectedword/{0}'.format(TEST_ID))
    assert response.status_code == 200
    assert b'"status": "success"' in response.data
    assert b'"id": %d' % TEST_ID in response.data
    assert b'"language": "en"' in response.data
    assert b'"word": "PUT_TEST"' in response.data
    assert b'"correct_word": "PUT_TEST"' in response.data

def test_delete_method(client):
    response = client.delete('/v1/protectedword/{0}'.format(TEST_ID))
    assert response.status_code == 200