import pytest
from thinkbox_apis.models.training_sample import TrainingSample
import json
from config import thinkbox_config

TEST_ID = None
test_profile_id = thinkbox_config['tests']['test_profile_id']

def test_post_method(client):
        response =  client.post('/v1/trainingsample/'+str(test_profile_id), data=json.dumps(dict(
            label_id = 21,
            parent_id = 7621,
            text = "POST_TEST_FOR_TS_TEST",
            active_on_test= True,
            source="classification_tool"
        )), content_type='application/json')
        global TEST_ID
        TEST_ID = TrainingSample().get_max_test_id()
        assert response.status_code == 201


def test_get_method(client):
        response = client.get('/v1/trainingsample/listing/'+str(test_profile_id))
        assert response.status_code == 200
        assert b'"status": "success"' in response.data

def test_delete_method(client):
    response = client.delete('/v1/trainingsample/'+ str(TEST_ID))
    assert response.status_code == 200
