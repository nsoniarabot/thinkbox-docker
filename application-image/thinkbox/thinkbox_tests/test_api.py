from components.parsers import ParserNames
import components.parsers as parsers
from components.rule_matcher import RuleMatcher
from components.rule_matcher import RuleBasedIntentParser
from thinkbox_apis.models.rule import Rule
from trace import Trace
from config import thinkbox_config

test_profile_id = thinkbox_config['tests']['test_profile_id']
my_shipments_intent_id = thinkbox_config['tests']['my_shipments_intent_id']
schedule_shipment_intent_id = thinkbox_config['tests']['schedule_shipment_intent_id']

def test_keyword_matchers():
    params = {'profile_id': test_profile_id}
    rule_based_intent_parser = RuleBasedIntentParser()
    label_id = rule_based_intent_parser.process(Trace('delivery'), params)
    assert label_id == schedule_shipment_intent_id


def test_keyword_partial_matchers():
    params = {'profile_id': test_profile_id}
    rule_based_intent_parser = RuleBasedIntentParser()
    label_id = rule_based_intent_parser.process(Trace('اريد معرفه الطلبات لو سمحت'), params)
    assert label_id == my_shipments_intent_id


def test_regex_matcher():
    rule_matcher = RuleMatcher()
    params = {'rule': Rule()}
    params['rule'].regex = 'regex'
    params['rule'].label_id = my_shipments_intent_id
    params['rule'].rule_type = 'regex'
    label_id = rule_matcher.process(Trace('regex'), params)
    assert label_id == my_shipments_intent_id


def test_parser_matcher():
    rule_matcher = RuleMatcher()
    params = {'label_id': my_shipments_intent_id, 'rule': Rule()}
    params['rule'].parser_name = 'confirmation'
    params['rule'].rule_type = 'parser'
    params['rule'].label_id = my_shipments_intent_id
    label_id = rule_matcher.process(Trace('no'), params)
    assert label_id == my_shipments_intent_id


def test_parser():
    parser = parsers.Parsers()
    named_entities = parser.process(Trace("I live in syria "), {"parser_name": ParserNames.GEO_PARSER.value})
    assert named_entities[ParserNames.GEO_PARSER.value] is not None and len(named_entities[ParserNames.GEO_PARSER.value]) != 0

    for match in named_entities[ParserNames.GEO_PARSER.value]:
        assert match['userUtterance'] == 'syria'
        assert match['matchedValue'] == 'سوريا'
        assert match['replacedValue'] == 'syria'
        assert match['locationFrom'] == 10
        assert match['locationTo'] == 15


def test_parser_date():
    parser = parsers.Parsers()
    named_entities = parser.process(Trace("I bought them on 01-01-2020"), {"parser_name": ParserNames.DATE_PARSER.value})
    assert named_entities[ParserNames.DATE_PARSER.value] is not None and len(named_entities[ParserNames.DATE_PARSER.value]) != 0

    for match in named_entities[ParserNames.DATE_PARSER.value]:
        assert 'Wed Jan 01' in match['variables']['date'] 