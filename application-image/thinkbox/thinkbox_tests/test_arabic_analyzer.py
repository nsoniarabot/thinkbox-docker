from components.arabic_analyzer import Analyzer
import pytest
import logging
from trace import Trace
from config import thinkbox_config

test_profile_id = thinkbox_config['tests']['test_profile_id']
test_obj = Analyzer()


def test_analyzer():

    '''Test Arabic morphological analyzer

    :parama: none
    :returns: none
    :rtype: pytest assertion
    '''
    
    assert test_obj.process(
            Trace("والأردن"),
            {
                "profile_id": test_profile_id,
                "analyzer_pid": "NONE"
            }
        ) == "اردن"
    
    assert test_obj.process(
            Trace("salut comment ca va? puis-je vous aider?"),
            {
                "profile_id": test_profile_id,
                "analyzer_pid": "TEXT_ARABOT_FR"
            }
        ) == "salut coment ca va pui je vou aid"

    assert test_obj.process(
            Trace("كَلَّا لَا تُطِعْهُ وَاسْجُدْ وَاقْتَرِبْ"),
            {
                "profile_id": test_profile_id,
                "analyzer_pid": "TEXT_ARABOT_AR_REGULAR"
            }
        ) == "كلاء لا اطع سجد اقترب"
