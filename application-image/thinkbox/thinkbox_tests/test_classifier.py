from classifier.intent_classifier_preprocessing.intent_classifier_preprocessing import IntentClassifierBasicPreprocessing
from classifier.classifier_manager import ClassifierManager
from sklearn.feature_extraction.text import TfidfVectorizer
from classifier.classifier import SVM
from thinkbox_apis.services.nlu_components import Thinkbox
import os.path
from config import thinkbox_config

test_profile_id = thinkbox_config['tests']['test_profile_id']
my_shipments_intent_id = thinkbox_config['tests']['my_shipments_intent_id']

def test_train_model():
    data = ClassifierManager.get_data(test_profile_id)
    tokn = IntentClassifierBasicPreprocessing(data)
    v2 = TfidfVectorizer()
    classifier = SVM(test_profile_id)
    classifier_manager = ClassifierManager(test_profile_id, tokn, classifier)
    classifier_manager.train_model(v2, 0.2, True, True)
    assert classifier_manager.classify('shipments') == my_shipments_intent_id


def test_save_model(path):
    data = ClassifierManager.get_data(test_profile_id)
    tokn = IntentClassifierBasicPreprocessing(data)
    v2 = TfidfVectorizer()
    classifier = SVM(test_profile_id)

    classifier_manager = ClassifierManager(test_profile_id, tokn, classifier)
    inserted_id = classifier_manager.train_model(v2, 0.2, True, True)
    if inserted_id is not None:
        classifier_manager.save_model(path, inserted_id)
    else:
        raise Exception("something went wrong, id = " + str(inserted_id))
    assert os.path.exists(path + '/' + str(test_profile_id) + '_' + str(inserted_id))


def test_load_model(path):
    classifier_manager = ClassifierManager(test_profile_id)
    classifier_manager.load_model(test_profile_id, path)
    assert classifier_manager.classify('shipments') == my_shipments_intent_id


def test_thinkbox_integration(path):
    thinkbox = Thinkbox(test_profile_id, path)
    pipline = {
        "intent_classifier"
    }
    thinkbox._pipeline = pipline
    params = {'user_text': 'shipments', 'profile_id': test_profile_id}
    assert thinkbox.understand_text(params).label_id == my_shipments_intent_id
