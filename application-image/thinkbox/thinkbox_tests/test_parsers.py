import json

from parsers.name_parser import NameParser
import pytest
import logging

from thinkbox_apis.models.name_keywords import NameKeydWords
from thinkbox_apis.models.names import Name
from trace import Trace
from config import thinkbox_config

def test_name_keyword_parser():
    name_parser = NameParser()
    name_parser.update_file("name_keywords.txt", "المتميز => name_identifier")
    name_tokens = name_parser.get_name_tokens("المتميز سامي")
    for token in name_tokens:
        assert token["tokenString"] == "name_identifier_سامي"
        assert token["name"]["name"] == "سامي"


def test_name_parser():
    name_parser = NameParser()
    file_name = "names.json"
    static_data = [{"name" : "روبرت دينيرو" ,"type" : "male" ,"nameEn" : "null" ,"isAmbiguous" : "no"}]
    name_parser.update_file(file_name, json.dumps(static_data))
    name_tokens = name_parser.get_name_tokens("روبرت دينيرو")
    for token in name_tokens:
        assert token["name"]["name"] == "روبرت دينيرو"

def test_multi_name_parsers_change():
    name_parser = NameParser()
    file_name = "names.json"
    static_data = [{"name" : "روبرت دينيرو" ,"type" : "male" ,"nameEn" : "null" ,"isAmbiguous" : "no"}]
    name_parser.update_file(file_name, json.dumps(static_data))
    name_tokens = name_parser.get_name_tokens("روبرت دينيرو")
    for token in name_tokens:
        assert token["name"]["name"] == "روبرت دينيرو"

    name_parser2 = NameParser()
    name_tokens = name_parser2.get_name_tokens("احمد")
    for token in name_tokens:
        assert token["name"]["name"] == "احمد"
