from components.searchapi import SearchAPI
from trace import Trace
from thinkbox_apis.services.nlu_components import Thinkbox
from config import thinkbox_config

test_bot_id = thinkbox_config['tests_solr']['test_bot_id']
test_faq_id = thinkbox_config['tests_solr']['test_faq_id']
test_org_id = thinkbox_config['tests_solr']['test_org_id']
test_profile_id = thinkbox_config['tests_solr']['test_profile_id']

def test_solr_integration(url):
    SearchAPI.solr_base_url = url
    solr = SearchAPI()
    trace_ = Trace()
    trace_.current_text = 'What payment methods can I use to pay for the Aramex services'
    params_ = {
        'bot_id': test_bot_id,
        'intent_id': test_faq_id,
        'org_id': test_org_id,
        'profile_id': test_profile_id
    }
    processed_response = solr.process(trace_, params_)
    assert processed_response is not None
    assert processed_response['answer'] == 'We have three (3) convenient payment modes available. You can pay by cash for scheduled online pickups, or; You can visit any of our outlets and pay by credit card (Visa or MasterCard), or; Through our Monthly Invoices System for corporate customers with an Aramex account.'
    assert processed_response['collection_name'] == 'botfaq'


def test_no_answer(url):
    SearchAPI.solr_base_url = url
    solr = SearchAPI()
    trace_ = Trace()
    trace_.current_text = 'asdfasdf'
    params_ = {
        'bot_id': test_bot_id,
        'intent_id': test_faq_id,
        'org_id': test_org_id,
        'profile_id': test_profile_id
    }
    response = solr.process(trace_, params_)
    assert response is None


def test_thinkbox_integration(path, url):
    thinkbox = Thinkbox(test_profile_id, path, url)
    params = {
        'user_text': 'What payment methods can I use to pay for the Aramex services',
        'bot_id': test_bot_id,
        'intent_id': test_faq_id,
        'org_id': test_org_id,
        'profile_id': test_profile_id
    }
    pipline = {
        "text_processing",
        "search_api"
    }
    thinkbox._pipeline = pipline
    processed_response = thinkbox.understand_text(params)
    assert processed_response.search_api_response is not None
    assert processed_response.search_api_response['answer'] == 'We have three (3) convenient payment modes available. You can pay by cash for scheduled online pickups, or; You can visit any of our outlets and pay by credit card (Visa or MasterCard), or; Through our Monthly Invoices System for corporate customers with an Aramex account.'
    assert processed_response.search_api_response['collection_name'] == 'botfaq'


def test_search_api_thinkbox_integration(path, url):
    thinkbox = Thinkbox(test_profile_id, path, url)
    params = {
        'user_text': 'احسنت',
        'bot_id': test_bot_id,
        'intent_id': test_faq_id,
        'org_id': test_org_id,
        'profile_id': test_profile_id

    }
    pipline = {
        "text_processing",
        "search_api"
    }
    thinkbox._pipeline = pipline
    processed_response = thinkbox.understand_text(params)
    assert processed_response.search_api_response is not None
    assert processed_response.search_api_response['answer'] == 'انا بخدمتك دائما'
    assert processed_response.search_api_response['collection_name'] == 'botsmalltalk'


def test_search_api_bad_word_thinkbox_integration(path, url):
    thinkbox = Thinkbox(test_bot_id, path, url)
    params = {
        'user_text': 'كذاب',
        'bot_id': test_bot_id,
        'intent_id': test_faq_id,
        'org_id': test_org_id,
        'profile_id': test_profile_id

    }
    pipline = {
        "text_processing",
        "search_api"
    }
    thinkbox._pipeline = pipline
    processed_response = thinkbox.understand_text(params)
    assert processed_response.search_api_response is not None
    assert processed_response.search_api_response['answer'] == '، للشكاوى يرجى زيارة الرابط التالي:https://www.aramex.com/support/submit-complaint/Index/'
    assert processed_response.search_api_response['collection_name'] == 'botbadword'
