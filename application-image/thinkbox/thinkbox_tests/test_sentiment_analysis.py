from components.sentiment_analysis import SentimentAnalysis
from config import thinkbox_config
import pytest
import logging
from trace import Trace

test_profile_id = thinkbox_config['tests']['test_profile_id']
test_obj = SentimentAnalysis()
params = {'profile_id': test_profile_id}

def sentiment_final_result(text: str):

    result = test_obj.result(Trace(text), params)
    return result


def test_sentiment():

    '''Test if the sentiment analysis result is correct

    :parama: none
    :returns: none
    :rtype: pytest assertion
    '''
    assert sentiment_final_result("خدمة سيئة") == "neg"
    assert sentiment_final_result("خدمة ممتازة") == "pos"
    assert sentiment_final_result("حسبي الله فيكم") == "neg"
    assert sentiment_final_result("رائع") == "pos"
    assert sentiment_final_result("انتم غير مميزين") == "neg"
    assert sentiment_final_result("حسبي الله ونعم الوكل") == "neg"
    assert sentiment_final_result("مطعم ورد مطعم متوسط") == "neu"
