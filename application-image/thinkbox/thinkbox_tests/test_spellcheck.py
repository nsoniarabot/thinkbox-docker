from components.spellcheck_component.spellcheck import Spellcheck
from config import thinkbox_config
import pandas as pd
import pytest
import logging
import components.spellcheck_component.spellcheck_manager as spell_manager
import os 
import utils

def test_reload_dictionaries():
    '''
    Test if the reload_dictionaries method is creating the .dic and .aff files
    '''
    dictionary_path = thinkbox_config['dictionaries']['test_dict_path']
    affixation_file_path = thinkbox_config['dictionaries']['affixation_file_path']
    languages = ['ar', 'en']
    for language in languages:
        dict_file = dictionary_path + language + ".dic"
        if os.path.exists(dict_file):
            os.remove(dict_file)
        assert (os.path.exists(dict_file) == False)
    spell_manager.reload_dictionaries(dictionary_path, affixation_file_path)
    for language in languages:
        dict_file = dictionary_path + language + ".dic"
        assert os.path.exists(dict_file)
        assert os.stat(dict_file).st_size != 0
        os.remove(dict_file)


def test_check_spelling_type():

    '''Test if the 'check_spelling' method returning True or False
        beside the main functionality validation of this method

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert type(test_obj.check_spelling("what", 'en')) == bool
    assert type(test_obj.check_spelling(":)", utils.detect_language(":)"))) == bool
    assert type(test_obj.check_spelling("ماذا", 'ar')) == bool


def test_spelling_en():

    '''Test that common english words are actually in the dictionary

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert test_obj.check_spelling("what", utils.detect_language("what")) == True
    assert test_obj.check_spelling("receiving", utils.detect_language("receiving")) == True
    assert test_obj.check_spelling("amazing", utils.detect_language("amazing")) == True
    assert test_obj.check_spelling("track", utils.detect_language("track")) == True
    assert test_obj.check_spelling("schedule", utils.detect_language("schedule")) == True
    assert test_obj.check_spelling("shipment", utils.detect_language("shipment")) == True
    assert test_obj.check_spelling("shipments", utils.detect_language("shipments")) == True
    assert test_obj.check_spelling("I'll", utils.detect_language("I'll")) == True
    assert test_obj.check_spelling("scheduleeeee", utils.detect_language("scheduleeeee")) == False
    assert test_obj.check_spelling("3adi", utils.detect_language("3adi")) == False

def test_spelling_ar():

    '''Test that common arabic words are actually in the dictionary

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert test_obj.check_spelling("ماذا", utils.detect_language("ماذا")) == True
    assert test_obj.check_spelling("الشحنة", utils.detect_language("الشحنة")) == True
    assert test_obj.check_spelling("توصيل", utils.detect_language("توصيل")) == True
    assert test_obj.check_spelling("استلام", utils.detect_language("استلام")) == True
    assert test_obj.check_spelling("بنك", utils.detect_language("بنك")) == True
    assert test_obj.check_spelling("تذكرة", utils.detect_language("تذكرة")) == True



def test_get_suggestions():

    '''Test if the 'get_suggestions' method returning String type
        beside the main functionality validation of this method

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert type(test_obj.get_suggestions("what", utils.detect_language("what"))) == str
    assert type(test_obj.get_suggestions(":)", utils.detect_language(":)"))) == str
    assert type(test_obj.get_suggestions("ماذا", utils.detect_language("ماذا"))) == str  


def test_corrected_output():

    '''Test if the 'corrected_output' method returning String type
        beside the main functionality validation of this method

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert type(test_obj.corrected_output("what")['corrected_output']) == str
    assert type(test_obj.corrected_output(":)")['corrected_output']) == str
    assert type(test_obj.corrected_output("ماذا")['corrected_output']) == str  


def test_corrected_output_en():

    '''Test that common misspellings are corrected

    :parama: none
    :returns: none
    '''
    test_obj = Spellcheck()
    assert test_obj.corrected_output("scheduule")['corrected_output'] == "schedule"
    assert test_obj.corrected_output("shipmants")['corrected_output'] == "shipments"

# TODO fix this test
@pytest.mark.skip(reason="test should be fixed")
def test_dictionary_accuracy():

    '''Test if the 'dictionary_accuracy' method returning corrected
        spelling mistakes, and show the accurey of certain dictionary
        beside the main functionality validation of this method

    :parama: none
    :returns: none
    :rtype: pyetest assertion
    '''

    bot_id = 0  # Todo after reading the bot_id from config

    dict_setting = thinkbox_config['dictionaries']
    dict_path = dict_setting['dict_path']
    dict_name = dict_setting['test_dict']
    errors = []
    try:
        df = pd.read_csv('{0}{1}.csv'.format(
            dict_path,
            dict_name
        ))
    except IOError as e:
            logging.error(e)
    correct_list = list(df['Correct Spelling'].str.lower())
    missed_list = list(df['Misspelt word'].str.lower())
    c = 0
    for idx, word in enumerate(missed_list):
        suggestion = test_obj.process(
                    {},
                    {
                        "user_text": word,
                        "bot_id": bot_id
                    }
        )
        if suggestion != correct_list[idx]:
            errors.append(word)
        else:
            c+=1
    print("--- {0:.0f} % accuracy ---".format(c/len(missed_list) *100))
    # assert no error message has been registered, else print messages
    assert not errors, "errors occurred:\n{}".format("\n".join(errors))