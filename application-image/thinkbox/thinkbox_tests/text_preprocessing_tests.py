from components.text_preprocessing_pipelines import TextPreprocessingPipelines
from config import thinkbox_config
from thinkbox_apis.services.nlu_components import Thinkbox
test_profile_id = thinkbox_config['tests']['test_profile_id']
my_shipments_intent_id = thinkbox_config['tests']['my_shipments_intent_id']

def test_default_preprocessing():
    input = 'aASD12asd  ؟، أنتي جماعتْ الســــــــــــلام'
    expected = 'aasd 12 asd   ءنتي جماعت السلام'
    result = TextPreprocessingPipelines.process(input)
    assert result == expected


def test_custom_preprocessing():
    input = 'aASD12asd  ؟، أنتي جماعتْ الســــــــــــلام'
    expected = 'aasd12asd   أنتي جماعتْ الســــــــــــلام'
    result = TextPreprocessingPipelines.process(input, ['lower', 'strip_punctuation'])
    assert result == expected


def test_custom_preprocessing_not_found():
    input = 'aASD12asd  ؟، أنتي جماعتْ الســــــــــــلام'
    try:
        TextPreprocessingPipelines.process(input, ['lower', 'a nonexistent function'])
    except Exception as e:
        assert str(e) == 'processor \'a nonexistent function\' was not found'



def test_preprocessing_thinkbox_integration():
    input = 'شحنــــاتي'
    thinkbox = Thinkbox(test_profile_id)
    params = {'user_text': input, 'profile_id': test_profile_id}
    label_id = thinkbox.understand_text(params).label_id
    assert label_id == my_shipments_intent_id

