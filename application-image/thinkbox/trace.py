
class Trace():
    def __init__(self, text=None):
        self.original_text = text
        self.current_text = text
        self.spellcheck_result = None
        self.named_entities = None
        self.sentiment_result = None
        self.processed_text = None
        self.analyzer_result = None
        self.label_id = None
        self.search_api_response = None

    def __str__(self):
        return f'original_text = {self.original_text},\n' \
               f'current_text = {self.current_text},\n' \
               f'spellcheck_result = {self.spellcheck_result},\n' \
               f'sentiment_result = {self.sentiment_result},\n' \
               f'named_entities = {self.named_entities},\n' \
               f'processed_text = {self.processed_text},\n' \
               f'analyzer_result = {self.analyzer_result},\n' \
               f'label_id = {self.label_id},\n' \
               f'search_api_response = {self.search_api_response}'
