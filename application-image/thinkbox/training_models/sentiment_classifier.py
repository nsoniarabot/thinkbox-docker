import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
import joblib
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report


data_set = pd.read_csv('/home/mfayoumi/11_Tasks_Archive/sent_data/csv_data/sent_dataset.csv')

X = list(data_set['text'])
y = list(data_set['polarity'])

vectorizer = CountVectorizer()

X_train, X_test, y_train, y_test = train_test_split(
X, y, test_size=0.2, random_state=1000)

vectorizer = CountVectorizer()
model  = LogisticRegression(random_state=0, solver='sag')

pipeline = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('classifier', model),
])
pipline_fitter = pipeline.fit(X_train, y_train)

y_pred = pipline_fitter.predict(X_test)
print("Accuracy: {}".format(pipline_fitter.score(X_test, y_test)))
print(classification_report(y_test, y_pred))

joblib.dump(pipline_fitter, '/home/mfayoumi/repos/thinkbox/resources/pkl_files/pip_sent_classifier.pkl')