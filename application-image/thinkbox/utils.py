import langid

#TODO ADD UTILS CLASS
def detect_language(text, languages=['en','ar']):
    langid.set_languages(languages)
    (language, conf) = langid.classify(text)
    return language
