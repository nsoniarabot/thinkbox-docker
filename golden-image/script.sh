## java installation
mkdir -p /usr/java
cd /usr/java/
tar -xzf /home/thinkbox/jdk-8u231-linux-x64.tar.gz
update-alternatives --install "/usr/bin/java" "java" "/usr/java/jdk1.8.0_231/bin/java" 0
update-alternatives --install "/usr/bin/javac" "javac" "/usr/java/jdk1.8.0_231/bin/javac" 0
update-alternatives --set java /usr/java/jdk1.8.0_231/bin/java
update-alternatives --set javac /usr/java/jdk1.8.0_231/bin/javac
rm /home/thinkbox/jdk-8u231-linux-x64.tar.gz


## install prerequisits for python compilation
apt-get install make build-essential zlib1g-dev libffi-dev libssl-dev libbz2-dev libssl-dev zlib1g-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev -y
cd /usr/local/src/
cp /home/thinkbox/Python-3.7.0.tgz /usr/local/src/Python-3.7.0.tgz

## extract and install python
tar -xzf Python-3.7.0.tgz
cd Python-3.7.0
./configure 
make install

